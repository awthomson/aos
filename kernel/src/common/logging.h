#ifndef __logging_h__
#define __logging_h__

#include <common/stdarg.h>

#define LL_WARN		0
#define LL_INFO		1
#define LL_DEBUG	2
#define LL_ERROR	3
#define LL_FATAL	4

void klog(int ll, const char *line, ...); 

#endif
