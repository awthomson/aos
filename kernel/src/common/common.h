typedef short int           int16_t;
typedef unsigned char       uint8_t;
typedef unsigned short int  uint16_t;
typedef unsigned int        uint32_t;
typedef unsigned long int   uint64_t;

int strlen(const char *str);
void outb(uint16_t port, uint8_t val);
unsigned char inb(unsigned int port);
uint16_t inw(unsigned int port);
uint32_t ind(unsigned int port);
void itoa_64 (char *buf, int base, unsigned long d);
void itoa_32 (char *buf, int base, unsigned int d);
int strcmp(const char *s1, const char *s2);
char *strncpy(char *destination, const char* source, unsigned int num);
char *strcpy(char *destination, const char* source);
int strncmp(const char *s1, const char *s2, unsigned long int max);
char *strtok(char *s, const char delim);
