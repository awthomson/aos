#include <drivers/serial/serial.h>
#include <common/stdarg.h>
#include <common/common.h>
#include <sys/scheduler.h>

#include "logging.h"

/*
	30 Black	33 Orange
	31 Red		34 Blue
	32 Green	35 Purple
*/

void klog(int ll, const char *fmt, ...)
{
	com1_putchar('[');

    switch(ll) {
        case LL_INFO:
            com1_printf("\e[0;32mINFO \e[1;37m] ");
            break;
        case LL_DEBUG:
            com1_printf("\e[0;34mDEBUG\e[0;37m] ");
            break;
        case LL_WARN:
            com1_printf("\e[0;33mWARN \e[0;37m] ");
            break;
        case LL_ERROR:
            com1_printf("\e[0;31mERROR\e[1;37m] ");
            break;
        case LL_FATAL:
            com1_printf("\e[1;31mFATAL\e[1;37m] ");
            break;
    }
	com1_printf("[PID %d] ", current_process->info->pid);

    const char *p;
    int i;
    char *s;
    va_list argp;
    char buff[20];

    va_start(argp, fmt);

    for(p = fmt; *p != '\0'; p++) {
        if(*p != '%') {
			if (*p == '\n') {
            	com1_putchar('\r');
            	com1_putchar('\n');
			} else {
            	com1_putchar(*p);
			}
            continue;
        }

        switch(*++p) {
        case 'c':
            i = va_arg(argp, int);
            com1_putchar(i);
            break;
        case 'd':
            i = va_arg(argp, unsigned int);
            itoa_32(buff, 'd', i);
            com1_kprint_str(buff);
            break;
        case 's':
            s = va_arg(argp, char *);
            com1_kprint_str(s);
            break;
        case 'x':
            i = va_arg(argp, unsigned int);
            itoa_32(buff, 'x', i);
            com1_kprint_str(buff);
            break;
        case 'g':
            i = va_arg(argp, unsigned long);
            itoa_64(buff, 'x', i);
            com1_kprint_str(buff);
            break;
        case '%':
            com1_putchar('%');
            break;
        }
    }

    va_end(argp);

	com1_printf("\e[0;37m");

}
