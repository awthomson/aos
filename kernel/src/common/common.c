#include "common.h"
#include "logging.h"

int strlen(const char *str) 
{
	int l = 0;
	while (*str++ != 0)
		l++;
	return l;
}


char *strtok(char *s, const char delim)
{
    static char *pos = 0;

    if ((s == 0) && ((uint64_t)pos == -1))
        return 0;

    if (s == 0)
        s = pos;

    char *bu = s;
    while (*s != 0) {
        if (*s == delim) {
            *s = '\0';
            pos = s+1;
            return bu;
        }
        s++;
    }
    pos =(char *)-1;            // Indicate there's no more tokens after this one
    return bu;
}


int strncmp(const char *s1, const char *s2, unsigned long int max)
{
    unsigned long int i;
    for (i=0; i<max; i++) {
        if ((s1[i] == 0) && (s2[i] == 0))
            return 0;
        else if (s1[i] < s2[i])
            return 1;
        else if (s1[i] > s2[i])
            return -1;
    }
	
    return 0;
}


char *strncpy(char *destination, const char* source, unsigned int num)
{
    if (destination == 0)
        return 0;

    char* ptr = destination;

    while (*source && num--)
    {
        *destination = *source;
        destination++;
        source++;
    }

    *destination = '\0';

    return ptr;
}

char *strcpy(char *destination, const char* source)
{
    if (destination == 0)
        return 0;

    char* ptr = destination;

    while (*source)
    {
        *destination = *source;
        destination++;
        source++;
    }

    *destination = '\0';

    return ptr;
}



int strcmp(const char *s1, const char *s2)
{
    unsigned long int i = 0;
    while(1) {
        if ((s1[i] == 0) && (s2[i] == 0)) 
            return 0;
        else if (s1[i] < s2[i]) 
            return 1;
        else if (s1[i] > s2[i]) 
            return -1;
		i++;
    }
}

void outb(uint16_t port, uint8_t val)
{
    __asm__ volatile ( "outb %1, %0" : : "a"(val), "Nd"(port) );
    // __asm__ volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
}

unsigned char inb(unsigned int port)
{
   unsigned char ret;
   __asm__ volatile ("inb al,dx":"=a" (ret):"d" (port));
//    __asm__ volatile ("inb %%dx,%%al":"=a" (ret):"d" (port));
   return ret;
}

uint16_t inw(unsigned int port)
{
   uint16_t ret;
   __asm__ volatile ("inw ax,dx":"=a" (ret):"d" (port));
//    __asm__ volatile ("inw %%dx,%%ax":"=a" (ret):"d" (port));
   return ret;
}

uint32_t ind(unsigned int port)
{
   uint32_t ret;
   __asm__ volatile ("in eax, dx":"=a" (ret):"d" (port));
//    __asm__ volatile ("in %%dx,%%eax":"=a" (ret):"d" (port));
   return ret;
}

/*  Convert the integer D to a string and save the string in BUF. If
    BASE is equal to 'd', interpret that D is decimal, and if BASE is
    equal to 'x', interpret that D is hexadecimal. */
void itoa_32(char *buf, int base, unsigned int d)
{
    char *p = buf;
    char *p1, *p2;
    unsigned int ud = d;
    int divisor = 10;

    /*  If %d is specified and D is minus, put `-' in the head. */
    if (base == 'D' && d < 0) {
        *p++ = '-';
        buf++;
        ud = -d;
    } else if (base == 'x')
         divisor = 16;

    /*  Divide UD by DIVISOR until UD == 0. */
    do {
        int remainder = ud % divisor;
        *p++ = (remainder < 10) ? remainder + '0' : remainder + 'a' - 10;
    } while (ud /= divisor);

    /*  Terminate BUF. */
    *p = 0;

    /*  Reverse BUF. */
    p1 = buf;
    p2 = p - 1;
    while (p1 < p2) {
    char tmp = *p1;
        *p1 = *p2;
        *p2 = tmp;
        p1++;
        p2--;
    }
}


void itoa_64 (char *buf, int base, unsigned long d)
{
    char *p = buf;
    char *p1, *p2;
    unsigned long ud = d;
    int divisor = 10;

    /*  If %d is specified and D is minus, put `-' in the head. */
    if (base == 'd' && d < 0) {
        *p++ = '-';
        buf++;
        ud = -d;
    } else if (base == 'x')
         divisor = 16;

    /*  Divide UD by DIVISOR until UD == 0. */
    do {
        int remainder = ud % divisor;
        *p++ = (remainder < 10) ? remainder + '0' : remainder + 'a' - 10;
    } while (ud /= divisor);

    /*  Terminate BUF. */
    *p = 0;

    /*  Reverse BUF. */
    p1 = buf;
    p2 = p - 1;
    while (p1 < p2) {
    char tmp = *p1;
        *p1 = *p2;
        *p2 = tmp;
        p1++;
        p2--;
    }
}

