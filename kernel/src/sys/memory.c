/*
	PML4 > PDP > PD > PT
	0x900000 - start of free memory (in QEMU)
	0x900000 - page table stuff
	0xa00000 - available memory
*/

#include <common/bootboot.h>
#include <common/common.h>
#include <common/logging.h>
#include <drivers/serial/serial.h>
#include <sys/scheduler.h>

#include "memory.h"

extern BOOTBOOT bootboot;

void *phys_mem_start_ptr;				// Alocate kernel memory from here
void *phys_mem_curr_ptr;
void *phys_mem_max_ptr;

uint8_t mem_map[4000000000/4096];
/*
	Sets up paging so virtual address 0x00000080_00000000 (second entry in PML4) points
	to the physical address of the largest continuous block on memory (saving some stuff
	at the start for page tables)
*/
void init_mem(void) {

	klog(LL_INFO, "Initialising memory\n");
	kernel_pml4 = get_pml4_addr();
	klog(LL_DEBUG, "Kernel PML4:       0x%x\n", kernel_pml4);

	// Clear memory bitmap
	for (int i=0; i<4000000000/4096; i++) 
		mem_map[i] = 0;

	// Parse the memory-map to find the largest block of "free" memory	
	parse_mmap();

}

void get_mem(uint64_t *res)
{
	*res = (uint64_t)phys_mem_curr_ptr;
}

void load_pml4(uint64_t pml4) {
    asm volatile(
        "mov rax, %0;"
        "mov cr3, rax;"
        ::"r"((uint64_t)pml4) :
    );
}


void load_kernel_pml4(void) {
	asm volatile(
        "mov rax, %0;"
        "mov cr3, rax;"
		::"r"((uint64_t)kernel_pml4) :
	);
}

// Create a 512 qword array to use as a page table
uint64_t *allocate_pt(uint64_t val, uint64_t inc)
{

	uint64_t *pt_addr;
	int page = find_next_free_block(4096);
	mark_page_used(page);
	pt_addr =  phys_mem_start_ptr+(page*4096);

	for (int i=0; i<512; i++) {
		pt_addr[i]=val;
		val += inc;
	}

	return pt_addr;

}

/*
	Go through the mmap entries given to us from BOOTBOOT.  Find the largest
	continuous block of memory and use that as our main work area.

	The First n bytes will be reserved for storing page-tables, everything else
	is fair game
*/
void parse_mmap() {
	int num_map_entries = (bootboot.size-128) / 16;
	MMapEnt *mmap_ent = &bootboot.mmap;
	uint64_t largest_size = 0;
	uint64_t largest_ptr = 0;
	for (int i=0; i<num_map_entries; i++) {
		if ((MMapEnt_Size(mmap_ent)>largest_size) && (MMapEnt_Type(mmap_ent) == MMAP_FREE)) {
			largest_size = MMapEnt_Size(mmap_ent);
			largest_ptr = MMapEnt_Ptr(mmap_ent);
		}
		mmap_ent++;	
	}

	phys_mem_start_ptr = (void *)largest_ptr;
	phys_mem_curr_ptr  = phys_mem_start_ptr;
	phys_mem_max_ptr   = phys_mem_start_ptr+largest_size;

	klog(LL_DEBUG, "Phy Mem Addr.:     0x%x - 0x%x\n", phys_mem_start_ptr, phys_mem_start_ptr+largest_size);
	klog(LL_DEBUG, "Phy Mem Size:      %d MB\n", largest_size>>20);
	
}

// TODO: Unmap virtual memory address for that page too
void free(void *addr)
{
	int page = (addr-phys_mem_start_ptr)>>12;
	// klog(LL_DEBUG, "Freeing page %d\n", page);
	mark_page_free(page);
}

void *malloc(uint64_t size)
{
	// Page align the size (multiple of 4096)
	size = ((size >> 12)+1)<<12;
	int next_free = find_next_free_block(size);
	// klog(LL_INFO, "Allocated %d bytes at block %d\n", size, next_free);
	void *this_mem_ptr = phys_mem_start_ptr+(next_free*4096);
	for (int i=0; i<(size>>12); i++)
		mark_page_used(next_free+i);
	// klog(LL_INFO, "malloc(): Allocated %d bytes at address %x\n", size, this_mem_ptr);
	
	phys_mem_curr_ptr += size; 	
	int i=0;
	for (i=0; i<size/4096; i++) {
		map_phys_address_4k(this_mem_ptr+(4096*i), this_mem_ptr+(4096*i), (void *)curr_proc_info->pml_ptr);
	}
	return this_mem_ptr;
}

// The only difference between this and malloc is this lets you specify the PML4 address
void *kmalloc(uint64_t size, void *pml4) 
{
	size = ((size >> 12)+1)<<12;
	int next_free = find_next_free_block(size);
	void *this_mem_ptr = phys_mem_start_ptr+(next_free*4096);
	for (int i=0; i<(size>>12); i++)
		mark_page_used(next_free+i);
	klog(LL_INFO, "kmalloc(): Allocated %d bytes at address %x\n", size, this_mem_ptr);

	int i;
	for (i=0; i<(size>>12); i++) 
		map_phys_address_4k(this_mem_ptr+(i*4096), this_mem_ptr+(i*4096), pml4);

	return this_mem_ptr;
	
}

int find_next_free_block(int num_bytes)
{
	int num_pages = (num_bytes>>12); 
	// klog(LL_DEBUG, "Finding next free %d contiguous pages (%d bytes)\n", num_pages, num_bytes);
	int cnt = 0;
	while (cnt<(4000000000/4096)-num_pages) {
		int could_be_free = 1;
		for (int i=0; i<num_pages; i++)
			if (is_page_used(cnt+i)==1) {
				could_be_free = 0;
				i = num_pages;
			}
		if (could_be_free == 1) {
			// klog(LL_DEBUG, "Found at page %d\n", cnt);
			return cnt;
		}
		cnt++;
	}
	return -1;
}

int is_page_used(int page)
{
	int byte = page / 8;
	int bit = page % 8;
	if ((mem_map[byte] & 1<<bit) == 1<<bit) {
		// klog(LL_DEBUG, "Page %d is Used\n", page);
		return 1;
	} else {
		// klog(LL_DEBUG, "Page %d is Not used\n", page);
		return 0;
	}	
}


void mark_page_used(int page)
{
	// klog(LL_DEBUG, "Marking page %d used\n", page);
	int byte = page / 8;
	int bit = page % 8;
	mem_map[byte] |= 1<<bit;
}

void mark_page_free(int page)
{
	// klog(LL_DEBUG, "Marking page %d free\n", page);
	int byte = page / 8;
	int bit = page % 8;
	uint8_t t = mem_map[byte];
	t &= -(1<<bit);
	mem_map[byte] = t;
}


void memcpy(void *src, void *dest, uint64_t size) 
{
	uint8_t *s = (uint8_t *)src;
	uint8_t *d = (uint8_t *)dest;
	for (uint64_t i=0; i<size; i++) {
		d[i] = s[i];
	}
}

void map_phys_address_4k(uint64_t *virt, uint64_t *phys, void *pml4_ptr) {

//	klog(LL_DEBUG, "Mapping 4k page virtual address %x to physical address %x in PML4 table at %x\n", virt, phys, pml4_ptr);

    uint64_t *pml4 = pml4_ptr;

    unsigned int pml4i = (uint64_t)virt >> 39 & (0b111111111);
    unsigned int pdpi  = (uint64_t)virt >> 30 & (0b111111111);
    unsigned int pdi   = (uint64_t)virt >> 21 & (0b111111111);
    unsigned int pti   = (uint64_t)virt >> 12 & (0b111111111);

    uint64_t pml4_curr = pml4[pml4i];
    if (pml4_curr == 0) {
       	pml4[pml4i] = (uint64_t)allocate_pt(0, 0) | 0x23;
    	pml4_curr = pml4[pml4i];
		map_phys_address_4k((void *)(pml4_curr&0xffffffffffffff00), (void *)(pml4_curr&0xffffffffffffff00), pml4_ptr);
    } 
    pml4[pml4i] |= 0b100;           // Set user accessable

    // Get pointer to the right PDP table   
    uint64_t pdp_loc = pml4[pml4i] & 0xffffffffffffff00;
    uint64_t *pdp_loc_p = (uint64_t *)pdp_loc;
    uint64_t pdp_curr = pdp_loc_p[pdpi];
    if (pdp_curr == 0) {
        pdp_loc_p[pdpi] = (uint64_t)allocate_pt(0, 0) | 0x23;
    	pdp_curr = pdp_loc_p[pdpi];
		map_phys_address_4k((void *)(pdp_curr&0xffffffffffffff00), (void *)(pdp_curr&0xffffffffffffff00), pml4_ptr);
    }
    pdp_loc_p[pdpi] |= 0b100;           // Set user accessable

    // Get pointer to the right PD table   
    uint64_t pd_loc = pdp_curr &0xfffffffffffff00;
    uint64_t *pd_loc_p = (uint64_t *)pd_loc;
    uint64_t pd_curr = pd_loc_p[pdi];
    if (pd_curr == 0) {
        pd_loc_p[pdi] = (uint64_t)allocate_pt(0, 0) | 0x23;
    	pd_curr = pd_loc_p[pdi];
		map_phys_address_4k((void *)(pd_curr&0xffffffffffffff00), (void *)(pd_curr&0xffffffffffffff00), pml4_ptr);
    }

	if (pd_curr & 0b10000000) {
		uint64_t old_addr = pd_curr & 0xffffffffffffff00;
		uint64_t new_pt = (uint64_t)allocate_pt(old_addr | 0b111, (uint64_t)4096);
		pd_loc_p[pdi] = new_pt | 0b111;	// Set user accessable bit, present
		pd_curr = pd_loc_p[pdi];
	} else {
       	pd_loc_p[pdi] |= 0x27;           // Set user accessable

	}		

    // Get pointer to the right PD table
    uint64_t pt_loc = pd_curr & 0xfffffffffffff00;
    uint64_t *pt_loc_p = (uint64_t *)pt_loc;
    pt_loc_p[pti] = (uint64_t)phys | 0b111;

}

/*
	Find the current CR3 register
*/
uint64_t *get_pml4_addr(void)
{
	uint64_t x;
	asm volatile("mov %0, cr3;" :"=r"(x) ::);
	return (uint64_t *)x;
}
