#ifndef __IPC_H__
#define __IPC_H__

#include <common/stdint.h>

struct s_ipc_msg {
    uint64_t data;
    uint64_t id;
    uint64_t src_pid;
    uint64_t dest_pid;
	uint64_t type;
	struct ipc_msg *next;	
};

struct s_ipc_msg ipc_msg[100];

uint64_t ipc_get_64(uint64_t id);
void ipc_send_64(uint64_t dest_proc, uint64_t val, uint64_t type);
uint64_t ipc_check(uint64_t *type, uint64_t *from_pid);
void init_ipc(void);

#endif
