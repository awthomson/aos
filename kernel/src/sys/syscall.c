#include <common/logging.h>
#include <common/stdint.h>
#include <drivers/serial/serial.h>
#include <drivers/keyboard.h>
#include <drivers/mouse.h>
#include <drivers/ide/ide.h>
#include <drivers/graphics/graphics.h>
#include <drivers/ext2/ext2.h>
#include <sys/hpet.h>
#include <sys/pit.h>
#include <sys/lapic.h>
#include <sys/memory.h>
#include <sys/scheduler.h>
#include <sys/ipc.h>

#include "syscall.h"

#define SEGMENT_SELECTOR_KERNEL_CODE 0x08
#define SEGMENT_SELECTOR_USER_CODE 0x08

/*
    Rather than take arguments this accesses the registers directly:
        RAX: system call function
        RBX: 1st argument
        RCX: Reserved (address to return to - set by the "syscall" command)
        RDX: 2nd argument
        RSI: 3rd argument
        RDI: 4th argument

    After finishing syscall returns to the address in RCX
*/
__attribute__((naked, noreturn)) uint64_t handle_syscall(void) {
	asm volatile(
		"mov rax, %0;"
		"mov cr3, rax;"
		::"r"(kernel_pml4):"rbx"
	);

	// Save the return address
    uint64_t return_addr;
    asm volatile(
        "mov rcx,[rsp];"
		"mov %0, rcx;"
        :"=r"(return_addr)::"rbx"
    );

	// Get the supplied arguments - use static - using a "naked" function
	// seems to do odd things with the stack
    static uint64_t function;
    static uint64_t arg1;
    static uint64_t arg2;
    static uint64_t arg3;
    static uint64_t arg4;
    asm volatile(
		"mov %0, r8;"
		"mov %1, r9;"
		"mov %2, r10;"
		"mov %3, rbx;"
		"mov %4, r12;"
        :"=r"(function), "=r"(arg1), "=r"(arg2),"=r"(arg3),"=r"(arg4) ::
    );

	asm volatile(
		"mov ax ,0;"
		"mov ds, ax;"
		"mov es, ax;"
		"mov fs, ax;"
		"mov gs, ax;"
	);

	uint64_t result = 0;
	uint64_t tmpnum = 0;
	uint8_t scan_code = 0x00;

    switch (function) {

		case SYSCALL_MMAP:
			// Used for sharing memory pages between processes
			// Arguments:
			//  * arg1 = mem pointer
			// 	* arg2 = mem size
			//  * arg3 = pid to share with
//			klog(LL_DEBUG, "SYSCALL - mmap(0x%x, %d, %d)\n", arg1, arg2, arg3);
	
			if (arg3 == 0)
				arg3 = (uint64_t)current_process->info->parent->pid;
			struct process_info *shared_proc = get_proc_ptr(arg3);
			uint64_t ptr = shared_proc->pml_ptr;

			int num_blocks = (arg2>>12)+1;
			num_blocks++;
			num_blocks--;
			// klog(LL_DEBUG, "Blocks %d\n", num_blocks);

			for (int i=0; i<=num_blocks; i++) {
				uint64_t *p = (uint64_t *)arg1+(512*i);
				// klog(LL_DEBUG, "Mapping page %x at ptr %x\n", (uint64_t)p, (uint64_t)ptr);
				map_phys_address_4k(p, p, (void *)ptr);
			}

			break;

		case SYSCALL_GET_PID:
			*(uint64_t *)arg1 = current_process->info->pid;
			break;

		case SYSCALL_SET_WFB:
			klog(LL_DEBUG, "syscall_set_wfb(%x)\n", arg1);
			gfx_set_working_fb((uint32_t *)arg1);
			break;

		case SYSCALL_HIDE_MOUSE:
			gfx_draw_area(arg1, arg2, 12, 15);
			break;

		case SYSCALL_SHOW_MOUSE:
			gfx_draw_img((uint32_t *)arg1, arg2, arg3, 12, 15);
			break;

		case SYSCALL_DRAW_REGION:
			// Arguments:
			//  * arg1 = x
			// 	* arg2 = y
			//  * arg3 = w
			//  * arg4 = h
			gfx_draw_area(arg1, arg2, arg3, arg4);
			break;

        case SYSCALL_DRAW_CHAR:
			// Arg1 = Character to display
			// Arg2 = X
			// Arg3 = Y
			// Arg4 = Colour (RGB)

			// This is a workaround for the term program - allows during a cursor
			gfx_draw_filled_rect(arg2, arg3, 17, 22, 0);
            gfx_draw_char((uint16_t)arg2, (uint16_t)arg3, (uint32_t)arg4, (uint8_t)arg1);
            break;

		case SYSCALL_KLOG:
			// Arguments:	
			//  * arg1 = log level (LL_DEBUG, LL_INFO, etc.)
			//	* arg2 = string to output
			//  * arg3 = value to include
			// Note:
			//  * This only accepts 1 argument.  Although it works fine in kernel mode, specifying 
			//    more than one argument via. syscall corrupts something and crashes
			klog((int)arg1, (char *)arg2, (int)arg3);
			break;

		case SYSCALL_SEND_MSG_64:
			// Arguments:	
			//  * arg1 = process id to sent message to
			//	* arg2 = value to send
			//  * arg3 = message type identifier

			// To send the IPC message to the current proc's parent, just use '0'
			if (arg1 == 0)
				arg1 = (uint64_t)current_process->info->parent->pid;

			ipc_send_64(arg1, arg2, arg3);
			break;			

		case SYSCALL_CHECK_MSG:
			// Returns:
			//  * arg1 = Message ID
			//  * arg2 = Message type
			//  * arg3 = Source PID
			tmpnum = ipc_check((uint64_t *)arg2, (uint64_t *)arg3);
			*(uint64_t *)arg1 = tmpnum;
			break;

        case SYSCALL_GET_MSG_64:
			// Arguments:
			//  * arg2 = Message ID to receive
            // Returns:
            //  * arg1 = Pointer to buffer to store the qword
            // ipc_receive_data(arg1, (void *)arg2);
            *(uint64_t *)arg1 = ipc_get_64(arg2);
			// klog(LL_DEBUG, "SYSCALL_GET_MSG_64 %x\n", arg1);
            break;


		case SYSCALL_READ_MOUSE:
			// Returns:
			//  * arg1 = Mouse info 
			result = ((uint64_t)get_mouse_buttons()<<32) + (get_mouse_x()<<16) + get_mouse_y();
			*(uint64_t *)arg1 = result;
			break;

		case SYSCALL_DRAW_BUFFER:
			// Arguments:	
			//  * arg1 = buffer
			gfx_draw_buffer((uint32_t *)arg1);
			break;

        case SYSCALL_FOPEN:
			// Arguments:	
			//  * arg1 = filename
			//	* arg2 = mode (ignored for now)
			// Returns:
			//  * arg3 = pointer to file descriptor (or error code)
			klog(LL_INFO, "syscall_fopen()\n");
			tmpnum = get_inode((char *)arg1, curr_proc_info->cwd);
			if (tmpnum == ENOENT) {
				klog(LL_ERROR, "Could not find file\n");
				*(uint64_t *)arg3 = ENOENT;
				break;
			} 				
			struct FILE *fd = malloc(sizeof(struct FILE));
			struct inode_table *i = get_inode_info(tmpnum);
			fd->inode = tmpnum;
			fd->fsize  = i->i_size;
			fd->fpos = 0;
			memcpy(fd, (void *)arg3, sizeof(struct FILE));
            break;

		case SYSCALL_FREAD:
			// Arguments:	
			//  * arg1 = buffer
			//	* arg2 = # of bytes to read
			//	* arg3 = file descripter
			// Returns:
			//  * ?		
			klog(LL_INFO, "syscall_fread(%x, %d, %x)\n", arg1, arg2, arg3);
			struct FILE *f = (struct FILE *)arg3;
			uint32_t inode = f->inode;
			uint64_t size = arg2;
			klog(LL_DEBUG, "inode: %d\n", inode);
			klog(LL_DEBUG, "size: %d\n", size);
			read_file((void *)arg1, inode, f->fpos, size);
			// f->fpos += size;
			// if (f->fpos > f->fsize)
			// 	f->fpos = size;
			break;

        case SYSCALL_WAIT:
			// Arguments:	
			//  * arg1 = number of ms to wait
			tmpnum = get_uptime_ns();
			curr_proc_info->state = STATE_WAITING;
			curr_proc_info->wait_until = tmpnum+(arg1*1000);
            break;

		// TODO: Don't return a redsult like this!
        case SYSCALL_CHECK_KEYPRESS:
            scan_code = read_key_buffer();
			result = (uint64_t)scan_code;
            break;

        case SYSCALL_READ_SECTOR:
			// Arguments:	
			//  * arg1 = buffer address
			//  * arg2 = sector number
            ide_read_sectors((uint8_t *)arg1, arg2, 1);
            break;

		case SYSCALL_MALLOC:
			// Arguments:		
			//  * arg1 = Size of memory block to allocate
			// Returns:
			//  * arg2 = Address of allocated block
			*(uint64_t *)arg2 = (uint64_t)malloc(arg1);
			// klog(LL_DEBUG, "SYSCALL_MALLOC allocated memory %x - %x\n", arg2, arg2+arg1);
			
			break;

		case SYSCALL_FREE:
			// Arguments:		
			//  * arg1 = Location of the memory block to free
			free((void *)arg1);
			// *(uint64_t *)arg2 = (uint64_t)malloc(arg1);
			
			break;			

		case SYSCALL_GET_UPTIME_NS:
			result = get_uptime_ns();
			break;

		case SYSCALL_READ_DIR:
			// Arguments:		
			//  * arg1 = Pointer to buffer to write directory information to
			// Returns:
			//  * arg2 = Return code		
			klog(LL_INFO, "syscall_read_dir()\n");
			read_dir(current_process->info->parent->cwd);
			klog(LL_DEBUG, "Dir info src: %x\n", dir_data);
			klog(LL_DEBUG, "Dir info dest: %x\n", arg1);
			memcpy((void *)dir_data, (void *)arg1, 4096);	// TODO: Assumption here dir_data is only 1 block
			klog(LL_DEBUG, "Done mem copy\n");
			break;

		case SYSCALL_GET_CWD:
			// Arguments:		
			//  * None
			// Returns:
			//  * arg1 = Result code		
			klog(LL_INFO, "syscall_get_cwd(%x)\n", arg1);
			*(int *)arg1 = curr_proc_info->parent->cwd;	
			break;

		case SYSCALL_SET_CWD:
			// Arguments:		
			//  * arg1 = Pointer to string of directory name
			// Returns:
			//  * arg2 = Result code			
			klog(LL_INFO, "syscall_set_cwd(%x, %x)\n", arg1, arg2);
			result = get_inode((char *)arg1, curr_proc_info->cwd);
			*(int *)arg2 = result;
			if (result > 0) {
				struct inode_table *it = get_inode_info(result);
				klog(LL_DEBUG, "Mode: %x\n", it->i_mode);
				if (it->i_mode & 0x4000) {
					current_process->info->cwd = result;
					*(int *)arg2 = ENOERR;
				} else {
					*(int *)arg2 = ENOTDIR;
				}
			} else {
					*(int *)arg2 = ENOENT;
			}
			klog(LL_INFO, "syscall_set_cwd: Returning %x\n", *(int *)arg2);
			break;

		case SYSCALL_EXEC:
			// Arguments:
			//  * arg1 = inode of file to execute
			//  * arg2 = point to string containing arguments
			// Returns:
			//  * arg3 = new pid
			tmpnum = get_inode((char *)arg1, curr_proc_info->cwd);
			klog(LL_DEBUG, "syscall_exec: inode: %d\n", tmpnum);
			if (tmpnum != ENOENT) {
				exec(tmpnum, (char **)arg2, (uint64_t *)arg3);
			} else {
				// File not found (or can't execute)
				klog(LL_ERROR, "syscall_exec: File not found\n");
				*(uint64_t *)arg3 = ENOENT;
			}
			break;

		case SYSCALL_GET_MEM:
			// Arguments:		
			//  * None		
			// Returns:
			//  * arg1 = position of free pysical mem
			klog(LL_INFO, "syscall_get_mem()\n");	
			get_mem((uint64_t *)arg1);
			break;

		case SYSCALL_SCROLL_UP:
			// Aguments:
			//  * arg1 = number of scanlines to scroll up	
			gfx_scroll_up(arg1);
			break;	

		case SYSCALL_DRAW_CURS:
			gfx_draw_filled_rect(arg1, arg2, 22, 22, arg3);
			break;		

		case SYSCALL_WRITE_STDOUT:	
			write_stdout((const char *)arg1);
			break;

		case SYSCALL_READ_STDIN:	
			read_stdin((char *)arg1);
			break;	

		case SYSCALL_DEBUG_PS:
			debug_ps();
			break;	

		case SYSCALL_KILL:
			kill(arg1);
			break;

		case SYSCALL_PROC_INFO:
			result = (uint64_t)get_proc_info((struct process_info *)arg1, arg2);
			break;

		case SYSCALL_KILL_ME:
			result = kill(curr_proc_info->pid);
			break;
			
        default:
            klog(LL_ERROR, "Unknown syscall function called\n");
            break;
    }

	// Restore the processes PML4
	asm volatile(
		"mov rax,%0;"
		"mov cr3, rax;"
		::"r"(curr_proc_info->pml_ptr):
	);	

	// Return back to where we came from
	asm volatile(	
		"pop rax;"				// When does this get put on stack?  
		"mov rcx, %0;"
		"mov rax, %1;"
		"rex.w sysret;"
		::"r"(return_addr),"r"(result):		
	);

}



uint64_t read_stdin(char *dest)
{
	// klog(LL_INFO, "Reading stdin %s\n", current_process->info->stdin_buffer);
	// klog(LL_INFO, "STDIN buffer len  %x\n", strlen(current_process->info->stdin_buffer));
	strcpy(dest, current_process->info->stdin_buffer);
	for (int i=0; i<4096; i++)
		current_process->info->stdin_buffer[i] = '\0';
	return strlen(dest);
}

void yield(void) 
{
	asm volatile("int 0x69;");
}

void init_syscall(void)
{

	klog(LL_INFO, "Initialising syscall\n");

	// Enable syscalls
	uint64_t val = get_msr(0xC0000080);
	set_msr(0xC0000080, val|1);

	// Set syscall handler location
	set_msr(0xC0000082, (uint64_t)&handle_syscall);
	set_msr(0xC0000084, (uint64_t)0x200);
	uint64_t star = get_msr(0xC0000081);
	star &= 0x00000000ffffffff;
   	star |= (uint64_t)SEGMENT_SELECTOR_KERNEL_CODE << 32;
   	star |= (uint64_t)((SEGMENT_SELECTOR_USER_CODE - 16) | 3) << 48;
	//set_msr(0xC0000081, (uint64_t)star);
	set_msr(0xC0000081, (uint64_t)0x0010000800000000);
	set_msr(0xC0000102, (uint64_t)0x1234567887654321);
	
}

uint64_t get_msr(uint32_t msr)
{
	uint32_t lo;
	uint32_t hi;
   	asm volatile("rdmsr" : "=a"(lo), "=d"(hi) : "c"(msr));
	return ((uint64_t)lo) + ((uint64_t)hi<<32);
}
 
void set_msr(uint32_t msr, uint64_t val)
{
	uint32_t hi = val>>32;
	uint32_t lo = val & 0xffffffff;
   	asm volatile("wrmsr" : : "a"(lo), "d"(hi), "c"(msr));
}

void write_stdout(const char *x)
{	
	char *pos = current_process->info->stdout_buffer;
	char c = *pos;

	// Find the end of the buffer
	while (c != '\0')
		c = *++pos;

	// Append the string to the end of the buffer
	strcpy(pos, x);
}

