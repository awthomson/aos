#ifndef __gdt_h__
#define __gdt_h__

/*
	Access byte:
		bit   7: Present bit (must be 1
		bit 5-6: Priv level
		bit   4: Descriptor type (e.g. COde/Data or TSS)
		bit   3: Executable bit (i.e. Code selector)
		bit   2: Direction/Conforming bit
		bit   1: R/W bit
		bit   0: Accessed 
	
	CODE0:	1 00 1 1 0 1 0  = 9A
	DATA0:	1 00 1 0 0 1 0  = 92
	CODE3:	1 11 1 1 0 1 0  = FA
	DATA3:	1 11 1 0 0 1 0  = F2

*/

#include <common/stdint.h>

#define GDT_GRANULARITY             0x20
#define GDT_RING0_CODE              0x9A
// #define GDT_RING0_CODE              0x98
#define GDT_RING0_DATA              0x92
#define GDT_RING3_CODE              0xFA
#define GDT_RING3_DATA              0xF2
#define GDT_TSS_ENTRY               0xE9

struct gdt_entry {
  uint16_t limit_low;
  uint16_t base_low;
  uint8_t base_mid;
  uint8_t access;
  uint8_t flags;
  uint8_t base_high;
} __attribute__((packed));

struct gdt_tss_entry {
  uint16_t limit_low;
  uint16_t base_low;
  uint8_t base_mid;
  uint8_t access;
  uint8_t flags;
  uint8_t base_high;
  uint32_t base_xhigh;
  uint32_t reserved;
} __attribute__((packed));

// https://wiki.osdev.org/Task_State_Segment
struct tss_structure {
	uint32_t	reserved_1;
	uint64_t	rsp0;
	uint64_t	rsp1;
	uint64_t	rsp2;
	uint32_t	reserved_2;
	uint64_t	ist1;
	uint64_t	ist2;
	uint64_t	ist3;
	uint64_t	ist4;
	uint64_t	ist5;
	uint64_t	ist6;
	uint64_t	ist7;
	uint64_t	reserved_3;
	uint32_t	reserved_4;
	uint32_t	iopb_offset;
} __attribute__((packed));


void gdt_set_gate(struct gdt_entry *gdt_ptr, uint64_t base, uint64_t limit, uint8_t access, uint8_t gran);
//void gdt_set_tss_gate(struct gdt_tss_entry *tss_ptr, uint64_t base, uint64_t limit, uint8_t access, uint8_t gran);
void gdt_set_tss_gate(struct gdt_tss_entry *tss_ptr, struct tss_structure *base, uint64_t limit, uint8_t access, uint8_t gran);
void init_gdt(void);

void gdt_write(uint16_t cs, uint16_t ds, uint16_t tr);

#endif
