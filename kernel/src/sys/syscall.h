#ifndef __SYSCALL_H__
#define __SYSCALL_H__

#include <common/stdint.h>

//#define SYSCALL_PRINT_NUM   	0x0
//#define SYSCALL_PRINT_CHAR  	0x1
//#define SYSCALL_PRINT_STR   	0x8
#define SYSCALL_WAIT			0x2
#define SYSCALL_CHECK_KEYPRESS	0x3
#define SYSCALL_READ_SECTOR		0x4
#define SYSCALL_DRAW_CHAR       0x5
#define SYSCALL_MALLOC			0x6
#define SYSCALL_GET_UPTIME_NS	0x7
#define SYSCALL_READ_DIR		0x9
#define SYSCALL_GET_CWD			0xa
#define SYSCALL_GET_MEM			0xb
#define SYSCALL_SET_CWD			0xc
#define SYSCALL_EXEC			0xd
#define SYSCALL_SCROLL_UP		0xe
#define SYSCALL_DRAW_CURS		0xf
#define SYSCALL_WRITE_STDOUT	0x10
#define SYSCALL_READ_STDIN	    0x11
#define SYSCALL_DEBUG_PS        0x12
#define SYSCALL_KILL            0x13
#define SYSCALL_PROC_INFO       0x14
#define SYSCALL_KILL_ME         0x15
#define SYSCALL_FOPEN           0x16
#define SYSCALL_FREAD           0x17
#define SYSCALL_FEOF            0x18
#define SYSCALL_DRAW_BUFFER     0x19
#define SYSCALL_READ_MOUSE      0x1a
#define SYSCALL_SEND_MSG		0x1b
#define SYSCALL_KLOG			0x1e
#define SYSCALL_DRAW_REGION     0x1f
#define SYSCALL_SET_WFB			0x20
#define SYSCALL_HIDE_MOUSE      0x22
#define SYSCALL_SHOW_MOUSE      0x23
#define SYSCALL_FREE            0x24
#define SYSCALL_MMAP            0x25
#define SYSCALL_CHECK_MSG		0x1c
#define SYSCALL_SEND_MSG_64     0x26
#define SYSCALL_GET_MSG_64		0x21
#define SYSCALL_GET_PID         0x27



uint64_t get_msr(uint32_t msr);
void set_msr(uint32_t msr, uint64_t val);
void init_syscall(void);
uint64_t handle_syscall(void);
void yield(void);
void write_stdout(const char *x);
uint64_t read_stdin(char *dest);

#endif
