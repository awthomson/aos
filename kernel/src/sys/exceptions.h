#ifndef __exceptions_h__
#define __exceptions_h__

// #include <sys/idt.h>

struct exception_frame {
	uint64_t flags;
    uint32_t rip;
} __attribute__((packed));


__attribute__((interrupt)) void isr_unhandled(struct exception_frame *f);
__attribute__((interrupt)) void isr_gpf(struct exception_frame *f);
__attribute__((interrupt)) void isr_pagefault(struct exception_frame *f);
__attribute__((interrupt)) void isr_invalid_opcode(struct exception_frame *f);
void display_regs(void);
#endif
