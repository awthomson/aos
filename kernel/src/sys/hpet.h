#ifndef __hpet_h__
#define __hpet_h__

#define HPET_ADDR 0x0fed00000

#define GENERAL_CAPABILITIES 	0
#define GENERAL_CONFIG 			0x10
#define MAIN_COUNTER			0xf0
#define TIMER_1_CONFIG			0x100
#define TIMER_1_COMPARATOR		0x108

uint64_t ticks_per_sec;

uint64_t get_uptime_ns(void);
void init_hpet(void);
void hpet_set_time(int timer, uint64_t time);
uint64_t hpet_get_time(void);
void hpet_enable(void);
void hpet_disable(void);
uint32_t phy_mem_read32(uint64_t addr);
void phy_mem_write32(uint64_t addr, uint32_t val);
uint64_t get_range(uint64_t value, uint8_t highest, uint8_t lowest);
int is_bit_set(uint64_t value, uint8_t bit_num);
void disable_pic(void);

#endif
