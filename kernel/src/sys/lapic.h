#ifndef __lapic_h__
#define __lapic_h__

#define APIC_REG_ID 0x20
#define APIC_REG_VERSION 0x30
#define APIC_REG_EOI 0xB0
#define APIC_REG_SPURIOUS 0xF0
#define APIC_REG_LINT0 0x350
#define APIC_REG_LINT1 0x360
#define APIC_REG_ICR0 0x300
#define APIC_REG_ICR1 0x310
#define APIC_REG_TIMER_LVT 0x320
#define APIC_REG_TIMER_INITIAL 0x380
#define APIC_REG_TIMER_CURRENT 0x390
#define APIC_REG_TIMER_DIVIDE 0x3E0
#define APIC_REG_ISR_BASE 0x100

#define APIC_CPUID_BIT (1 << 9)
#define APIC_DISABLE 0x10000
#define APIC_TIMER_PERIODIC 0x20000
#define IRQ_APIC_SPURIOUS 0xFF

#define LAPIC_BASE 0xfee00000

uint64_t get_ticks(void);
void init_lapic(void);

static inline void lapic_write(uint64_t reg_offset, uint32_t data)
{
    *(volatile uint32_t *)(LAPIC_BASE + reg_offset) = data;
}

static inline uint32_t lapic_read(uint64_t reg_offset)
{
    return *(volatile uint32_t *)(LAPIC_BASE + reg_offset);
}

#endif
