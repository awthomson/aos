#include <common/logging.h>
#include <common/stdint.h>
#include <sys/memory.h>
#include <drivers/ext2/ext2.h>
#include "scheduler.h"

#include "../../userspace/term.inc"

uint64_t pid_counter;

extern uint8_t *inode_table_data;

/*
	Useful indexes:
		0x18	e_entry (memory address of the entry point)
		0x20	e_phoff (location of the program header table)
		0x28	e_shoff (location of the section header table)
*/

struct elf_header {
	uint32_t e_ident_mag;
	uint8_t	e_ident_class;
	uint8_t	e_ident_data;
	uint8_t	e_ident_version;
	uint8_t	e_ident_os_abi;
	uint8_t	e_ident_abi_ver;
	uint8_t	e_ident_pad_1;
	uint8_t	e_ident_pad_2;
	uint8_t	e_ident_pad_3;
	uint8_t	e_ident_pad_4;
	uint8_t	e_ident_pad_5;
	uint8_t	e_ident_pad_6;
	uint8_t	e_ident_pad_7;
	uint16_t e_type;
	uint16_t e_machine;
	uint32_t e_ver;
	uint64_t e_entry;
	uint64_t e_phoff;
	uint64_t e_shoff;
	uint32_t e_flags;
	uint16_t e_ehsize;
	uint16_t e_phentsize;
	uint16_t e_phnum;
	uint16_t e_shentsize;
	uint16_t e_shnum;
	uint16_t e_shstrndx;
};

struct elf_prog_header {
	uint32_t p_type;
	uint32_t p_flags;
	uint64_t p_offset;
	uint64_t p_vaddr;
	uint64_t p_paddr;
	uint64_t p_file_size;
	uint64_t p_mem_size;
	uint64_t p_align;
};

struct elf_section_header {
	uint32_t s_name;
	uint32_t s_type;
	uint64_t s_flags;
	uint64_t s_addr;
	uint64_t s_offset;
	uint64_t s_size;
	uint32_t s_link;
	uint32_t s_info;
	uint64_t s_addr_align;
	uint64_t s_ent_size;
};


/*
	Setup the scheduler.  This involves loading an example ELF files as
	a process, and jumping into it
*/
void init_scheduler(void)
{

	klog(LL_INFO, "Initialising scheduler\n");

	// Init process list
	pid_counter = 1;
	first_process = (struct process_list_node *)0;


	// Load two example programs as a test
 	 load_elf((uint8_t *)&term, term_len, 0, 0);
	// exec(13);

	// Insert the process at the start of the list
	current_process = first_process;
	curr_proc_info = first_process->info;

	// Start  executing the first in the list
	klog(LL_DEBUG, "About to jump to %x.  SP=%x,  PML4=%x\n", first_process->info->rip, first_process->info->rsp, first_process->info->pml_ptr);

	jump_usermode( (void*)first_process->info->rip, (void*)first_process->info->rsp, (void*)first_process->info->pml_ptr);

	// Should never reach this
	while(1);

}

/*
	Spawn a new process.  This creates an entry in the process list and allocates some
	memory for a stack.
*/
uint64_t new_process(void *entry_ptr, void *pml4_ptr, char **args, int argc)
{

	klog(LL_DEBUG, "Creating a new process\n");

	// Allocate some space to save the process information	
	struct process_info *new_process_info = (struct process_info *)kmalloc(sizeof(struct process_info), kernel_pml4);

	struct process_list_node *new_process_list_node = (struct process_list_node *)kmalloc(sizeof(struct process_list_node), kernel_pml4);

	// Create a 4k stack space
	void *stack_ptr = kmalloc(4096*1, pml4_ptr);
	map_phys_address_4k((uint64_t *)stack_ptr, stack_ptr, pml4_ptr);

	// Needed to give new process access to it's arguments
	klog(LL_WARN, "Mapping args at %x\n", args[0]);
	map_phys_address_4k((uint64_t *)args[0], (uint64_t*)args[0], pml4_ptr);

	// Create a buffer for stdin
	void *stdin_ptr = kmalloc(4096, pml4_ptr);
	map_phys_address_4k((uint64_t *)stdin_ptr, stdin_ptr, pml4_ptr);
	map_phys_address_4k((uint64_t *)stdin_ptr, stdin_ptr, kernel_pml4);
	for (int i=0; i<4096; i++)
		*(char *)(stdin_ptr+i) = '\0';

	// Create a buffer for stdout
	void *stdout_ptr = kmalloc(4096, pml4_ptr);
	map_phys_address_4k((uint64_t *)stdout_ptr, stdout_ptr, pml4_ptr);		

	// Populate the process structure info
	klog(LL_DEBUG, "New process info: %x\n", (uint64_t)new_process_info);
	new_process_info->cwd		= 2;
	new_process_info->cs		= 0x23;
	new_process_info->ss		= 0x1b;
	new_process_info->rsp		= (uint64_t)stack_ptr+0x1000-0x100;
	new_process_info->rip		= (uint64_t)entry_ptr;
	new_process_info->pml_ptr 	= (uint64_t)pml4_ptr;
	new_process_info->kernel_pml= (uint64_t)kernel_pml4;
	new_process_info->priority 	= 0x80;
	new_process_info->flags		= 0x202;
	new_process_info->state 	= STATE_RUNNING;
	new_process_info->pid 		= pid_counter++;
	new_process_info->rbp		= new_process_info->rsp;

	// Pre-populate registers with dummy data
	new_process_info->rax			= 0x1;
	new_process_info->rbx			= 0x2;
	new_process_info->rcx			= 0x3;
	new_process_info->rdx			= 0x4;
	new_process_info->rdi			= (uint64_t)args;
	new_process_info->rsi			= (uint64_t)argc;
	new_process_info->r8			= 0x7;
	new_process_info->r9			= 0x8;
	new_process_info->r10			= 0x9;
	new_process_info->r11			= 0xa;
	new_process_info->r12			= 0xb;
	new_process_info->r13			= 0xc;
	new_process_info->r14			= 0xd;
	new_process_info->r15			= 0xe;

	if (curr_proc_info != 0)
		new_process_info->parent = curr_proc_info;
	else
		new_process_info->parent = new_process_info;
	

	new_process_info->stdout_buffer	= curr_proc_info->stdin_buffer;
	new_process_info->stdout_buffer_pos = 0;
	new_process_info->stdin_buffer = stdin_ptr;	
	new_process_info->stdin_buffer_pos = 0;

	new_process_list_node->next = first_process;
	new_process_list_node->info = new_process_info;
	first_process = new_process_list_node;

	return new_process_info->pid;

	
}

/*
	Temporary helper function to display the process list
*/
void debug_processes(void)
{
	klog(LL_INFO, "Proccesses:\n");
	klog(LL_DEBUG, "  Curr process: %d\n", curr_proc_info->pid);
	struct process_list_node *curr = first_process;
	klog(LL_DEBUG, "  Curr Ptr: %x\n", (uint64_t)curr);
	while (curr != 0) {
		klog(LL_DEBUG, "PID:             %d\n", curr->info->pid);
		klog(LL_DEBUG, "  RSP:           %x\n", curr->info->rip);
		klog(LL_DEBUG, "  RSP:           %x\n", curr->info->rsp);
		klog(LL_DEBUG, "  PML4 Address:  %x\n", curr->info->pml_ptr);
		klog(LL_DEBUG, "  State:         %x\n", curr->info->state);
		klog(LL_DEBUG, "  Priority:      %d\n", curr->info->priority);
		curr = curr->next;
	}
	
}



uint64_t load_elf(void *elf_addr, uint64_t elf_size, char **args, int argc)
{
	// int argc = 0;
	// char *x = strtok(str, ' ');
	// while (*x != '\0') {
    // 	x = strtok(NULL, ' ');
	// 	argc++;
	// }
	// klog(LL_INFO, "Number of args: %d\n", argc);


	klog(LL_INFO, "Loading ELF\n");

	void *new_pml4 = create_new_pml4();
	klog(LL_DEBUG, "New PML4 entry %x\n", (uint64_t)new_pml4);

	// Copy the ELF data to somewhere - shouildn't be needed if loading direftly from disk
	void *elf_phys_addr = kmalloc(elf_size, get_pml4_addr());
	memcpy(elf_addr, elf_phys_addr, elf_size);
	// void *elf_phys_addr = elf_addr;

	struct elf_header *header = elf_phys_addr;
	klog(LL_DEBUG, "ELF Entry Point:         %x\n", header->e_entry);

	klog(LL_DEBUG, "-- Section Header Info -------\n");
	for (int i=0; i<header->e_shnum; i++) {
		struct elf_section_header *sheader = elf_phys_addr + header->e_shoff + (i * header->e_shentsize);
		uint64_t flags = sheader->s_flags;
		char executable = '-';
		char writable = '-';
		if (flags & 0b1)
			writable = 'W';
		if (flags & 0b100)
			executable = 'X';
		switch (sheader->s_type) {
			case 0:
				klog(LL_DEBUG, "  %d [%c%c] NULL \n", i, writable, executable);
				break;
			case 1:
				klog(LL_DEBUG, "  %d [%c%c] Program Data\n", i, writable, executable);
				klog(LL_DEBUG, "         Address: 0x%x (%d bytes)\n", sheader->s_addr, sheader->s_size);
				klog(LL_DEBUG, "         Offset:  0x%x\n", sheader->s_offset);
				// Map section to specified virtual address
				if (sheader->s_addr != 0) {
					for (int i=0; i<=(sheader->s_size-1)/4096; i++) 
						map_phys_address_4k((uint64_t *)sheader->s_addr+(512*i), elf_phys_addr+sheader->s_offset+(4096*i), new_pml4);
				}
				break;
			case 2:
				klog(LL_DEBUG, "  %d [%c%c] Symbol Table\n", i, writable, executable);
				break;
			case 3:
				klog(LL_DEBUG, "  %d [%c%c] String table\n", i, writable, executable);
				break;
			case 7:
				klog(LL_DEBUG, "  %d [%c%c] Notes\n", i, writable, executable);
				break;
			default:
				klog(LL_DEBUG, "  %d [%c%c] Not interesting (0x%x)\n", i, writable, executable, sheader->s_type);
				break;
		}
		
	}	

	map_phys_address_4k((uint64_t *)args, (uint64_t *)args, new_pml4);

	uint64_t pid = new_process((void *)header->e_entry, new_pml4, args, argc);
	klog(LL_DEBUG, "Loaded ELF executable.  Returning %d...\n", pid);
	return pid;

}

/*
	Load a page table, and jump into usermode
	NOTE: Don't put any code before the assembly - it is dependant on
	the registers.  I should fix this.
	Anyway push SS:RSP, Flags and CS:RIP on the stack and iretq to enter usermode
	using that stack/entry point
	TODO: Use proper segment registers
*/
void jump_usermode(void *code_entry_point, void *stack_ptr, void *pml4_ptr)
{
	asm volatile(
		// Set memory map
		"sti;"

        "mov rax, %0;"
        "mov cr3, rax;"
	
		// Set data segment registers
    	"mov ax, 0x18|3;"
    	"mov ds, ax;"
    	"mov es, ax;"
    	"mov fs, ax;"
    	"mov gs, ax;"

		// Set stack ptr
    	"push 0x18|3;"
    	"push rsi;"

		// Set flags
    	"pushfq;"

		// Set instruction pointer	
    	"push 0x20|3;"
    	"push rdi;"


		// Apply changes (NOTE: ireq set CS and DS registers, retq doesn't!)
    	"iretq;"
        ::"r"(pml4_ptr) :
	);
}

/*
	Create new PML4 table so every process gets its own memory map
	Copy the kernel mapping in, but keep the readt clear
*/
void *create_new_pml4(void)
{
	klog(LL_DEBUG, "create_new_pml4()\n");

    // Get the first and last PML4 entry currently used, and copy them both
    // into a newly allocated page table
    uint64_t *pml4 = get_pml4_addr();
    uint64_t kernel_high = pml4[511];

    uint64_t *new_pml4 = allocate_pt(0, 0);
	klog(LL_DEBUG, "Created new PML4 table at: %x_%x\n", (uint64_t)new_pml4>>32, (uint64_t)new_pml4&0xffffffff);
    new_pml4[511] = kernel_high;

	// Need to map these low addresses for IDE register access, LAPIC, etc.
	map_phys_address_4k((uint64_t *)0x0, (uint64_t *)0x0, new_pml4);
	map_phys_address_4k((uint64_t *)0xfee00000, (uint64_t *)0xfee00000, new_pml4);
	map_phys_address_4k((uint64_t *)0x20f000, (uint64_t *)0x20f000, new_pml4);

	// Map the page table to itself!
	map_phys_address_4k((uint64_t *)new_pml4, (uint64_t *)new_pml4, new_pml4);

	return (void *)new_pml4;
}


void *get_current_proc(void) {
	return (void *)curr_proc_info;
}

void exec(uint32_t inode, char **args, uint64_t *pid)
{

	// Count the nujmber of arguments and pass it down
	int argc = 0;
	while (strcmp(args[argc], "")!=0)
		argc++;
	klog(LL_DEBUG, "Number of args %d\n", argc);

    klog(LL_DEBUG, "Getting ready to execute program with inode: %d\n", inode); 
	klog(LL_DEBUG, "Command line arguments: %s\n", args);
    struct inode_table *it = (struct inode_table *)(inode_table_data+(128*(inode-1)));

	void *exec_location = kmalloc(it->i_size, kernel_pml4);
	klog(LL_DEBUG, "Allocated some memory to load executable at %x\n", exec_location);
	for (int i=0; i<it->i_blocks/8; i++) {
		klog(LL_DEBUG, "Reading block %d\n", i);
		read_block(exec_location+(4096*i), it->i_block[i]);
	}
	uint64_t res = load_elf(exec_location, it->i_size, args, argc);

	klog(LL_DEBUG, "Exec complete.  Returning %d...\n", res);
	*pid = res;
}

/*
	Given a PID, copy that processes info to a buffer
	Returns:
		1 on success
		0 on error (PID not found)
*/
int get_proc_info(struct process_info *dest, uint64_t pid)
{
	struct process_list_node *proc = first_process;
	while (proc != 0) {
		if (proc->info->pid == pid) {
			dest->cwd = proc->info->cwd;
			// klog(LL_DEBUG, "Returning info for proc %x\n", pid);
			return 1;
		}
		proc = proc->next;
	}
	klog(LL_ERROR, "Cant find info for proc %x\n", pid);
	return 0;
}

/*
	Given a PID, return a poointer to the data
	Returns:
		Pointer to the process structure
*/
struct process_info *get_proc_ptr(uint64_t pid)
{
	struct process_list_node *proc = first_process;
	while (proc != 0) {
		if (proc->info->pid == pid) {
			return proc->info;
		}
		proc = proc->next;
	}
	klog(LL_ERROR, "Cant find info for proc %x\n", pid);
	return 0;
}

int kill(uint64_t pid)
{
	klog(LL_INFO, "Killing pid %d\n", pid);
	struct process_list_node *proc = first_process;

	// Dont kill the only running process
	if (first_process->next == 0) {
		klog(LL_ERROR, "Can't kill only running process\n");
		return 0;
	}

	// If the pid is the first in the list, just point to the next one
	if (first_process->info->pid == pid) {
		first_process = first_process->next;
		klog(LL_WARN, "Killed pid %d\n", pid);
		return 1;
	}

	while (proc->next != 0) {
		if (proc->next->info->pid == pid) {
			klog(LL_WARN, "Killed pid %d\n", pid);
			proc->next = proc->next->next;
			return 1;
		}
		proc = proc->next;
	}
	return 0;
}

void debug_ps(void)
{
	klog(LL_INFO, "Debug PS()\n");
	struct process_list_node *proc = first_process;
	while (proc != 0) {
		klog(LL_DEBUG, "PID: %d\n", proc->info->pid);
		klog(LL_DEBUG, "CWD: %d\n", proc->info->cwd);
		klog(LL_DEBUG, "Parent PID: %d\n", proc->info->parent->pid);
		proc = proc->next;
	}
}
