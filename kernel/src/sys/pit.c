#include <common/common.h>
#include <common/logging.h>
#include <drivers/serial/serial.h>

#include "pit.h"

/* Simple pause function */
void pit_wait(uint32_t ms) {

	/* 1193 ticks per ms */
	uint64_t total_count = 0x4A9UL * ms;

	do {
		uint16_t count = (total_count > 0xFFFFU) ? 0xFFFFU : (uint16_t) total_count;
		
		outb(PORT_CMD, CMD_ACC_LOHI);
		outb(PORT_CH0, (uint8_t) (count & 0xFF));
		outb(PORT_CH0, (uint8_t) (count >> 8));

		do {
			__asm__ volatile ( "pause" : : );
			outb(PORT_CMD, CMD_READBACK | CMD_RB_CH0 | CMD_RB_NO_COUNT_LATCH);
		} while((inb(PORT_CH0) & STATUS_OUTPUT) == 0);
		
		total_count -= count;
	} while((total_count & ~0xFFFFU) != 0);
}


// static uint8_t pic_control[2] = {0x20, 0xa0};
static uint8_t pic_data[2] = {0x21, 0xa1};

void init_pit(void) {
	klog(LL_INFO, "Initialising PIT\n");
	uint32_t frequency = 60;
	uint32_t divisor = 1193180 / frequency;
	// Send the command byte.
	outb(0x43, 0x36);
	// Divisor has to be sent byte-wise, so split here into upper/lower bytes.
	uint8_t l = (uint8_t)(divisor & 0xFF);
	uint8_t h = (uint8_t)( (divisor >> 8) & 0xFF );
	// Send the frequency divisor.
	outb(0x40, l);
	outb(0x40, h);

	//pic_enable(0);
 	//pic_enable(1);
	//pic_enable(2);
}

void pic_enable(uint8_t irq) {
  uint8_t mask;
  if (irq < 8) {
    mask = inb(pic_data[0]);
    mask = mask & ~(1 << irq);
    outb(pic_data[0], mask);
  } else {
    irq -= 8;
    mask = inb(pic_data[1]);
    mask = mask & ~(1 << irq);
    outb(pic_data[1], mask);
//    pic_enable(2);
  }
}
