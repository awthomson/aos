/*
	Local APIC

	Controls individual CPU interrupt configuration

	LVT:  Local Vector Table
	ISR:  In-Service Register
	IRR:  Interrupt Request Register	
	PPR:  Processor Priority Register
	SPIv: Spurious Interrupt Vector
	ICR:  Interrupt Control Register

	Links:
		https://wiki.osdev.org/APIC

*/

#include <common/logging.h>
#include <common/stdint.h>
#include <sys/pit.h>
#include <sys/interrupts.h>

#include "lapic.h"


void init_lapic(void)
{
    klog(LL_INFO, "Initialising LAPIC\n");

    // Enable the LAPIC via the spurious interrupt register
    lapic_write(APIC_REG_SPURIOUS, lapic_read(APIC_REG_SPURIOUS) | 0x100);

	// Calibrate the timer by using the PIT
	lapic_write(APIC_REG_TIMER_DIVIDE, 0x3);
    lapic_write(APIC_REG_TIMER_INITIAL, 0xFFFFFFFF);
	pit_wait(10);
	lapic_write(APIC_REG_TIMER_LVT, APIC_DISABLE);
	uint32_t ticks = 0xFFFFFFFF - lapic_read(APIC_REG_TIMER_CURRENT);

	// Set the time to fire at a regular frequency
	lapic_write(APIC_REG_TIMER_LVT, IRQ_TIMER | APIC_TIMER_PERIODIC);
	lapic_write(APIC_REG_TIMER_INITIAL, ticks*5);
	
	klog(LL_DEBUG, "%d ticks\n", ticks);
	
	// This crashes if enabled too soon
    // asm volatile ("sti");
    asm volatile ("cli");

}

uint64_t get_ticks(void)
{
	return 0xffffffff-lapic_read(APIC_REG_TIMER_CURRENT);	
}


