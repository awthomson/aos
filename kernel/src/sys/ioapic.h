#ifndef __ioapic_h__
#define __ioapic_h__

void ioapic_set_irq(uint8_t irq, uint64_t apic_id, uint8_t vector) ;
void write_ioapic_register(const uint8_t offset, const uint32_t val) ;
uint32_t read_ioapic_register(const uint8_t offset);
void init_ioapic(void);

#endif
