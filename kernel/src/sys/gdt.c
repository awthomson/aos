#include "gdt.h"

#include <common/logging.h>

struct gdt_data {
	struct gdt_entry null;			
	struct gdt_entry r0_code;		// 0x08
	struct gdt_entry r0_data;		// 0x10
	struct gdt_entry r3_data;		// 0x18
	struct gdt_entry r3_code;		// 0x20
	struct gdt_tss_entry tss;		// 0x28
	struct gdt_entry null2;			
} __attribute__((packed));

struct descriptor {
    uint16_t size;
    uint64_t offset;
} __attribute__((packed));

struct gdt_data gdt;
struct tss_structure tss;

uint8_t kernel_stack[4096]={'.'};

void gdt_set_gate(struct gdt_entry *gdt_ptr, uint64_t base, uint64_t limit, uint8_t access, uint8_t gran)
{
    gdt_ptr->base_low   = (uint16_t)(base & 0xFFFF);
    gdt_ptr->base_mid   = (uint8_t)((base >> 16) & 0xFF);
    gdt_ptr->base_high  = (uint8_t)((base >> 24) & 0xFF);
    gdt_ptr->limit_low  = (uint16_t)(limit & 0xFFFF);
    gdt_ptr->flags     	= (uint8_t)((limit >> 16) & 0x0F);
    gdt_ptr->flags     	|= (gran & 0xF0);
    gdt_ptr->access    	= access;
}

void gdt_set_tss_gate(struct gdt_tss_entry *tss_ptr, struct tss_structure *tss, uint64_t limit, uint8_t access, uint8_t gran)
{
	uint64_t base = (uint64_t)tss;
    tss_ptr->base_low   = (uint16_t)(base & 0xFFFF);
    tss_ptr->base_mid   = (uint8_t)((base >> 16) & 0xFF);
    tss_ptr->base_high  = (uint8_t)((base >> 24) & 0xFF);
    tss_ptr->limit_low  = (uint16_t)(limit & 0xFFFF);
 	tss_ptr->flags     	= (uint8_t)((limit >> 16) & 0x0F);
    tss_ptr->flags     	|= (gran & 0xF0);
//    tss_ptr->flags     	= 0x40;			// TODO
    tss_ptr->access    	= access;
//    tss_ptr->access    	= 0x89;			// TODO
  	tss_ptr->base_xhigh = ((base >> 32) & 0xFFFFFFFF);
  	tss_ptr->reserved 	= 0;

}

void init_gdt(void) {

	klog(LL_INFO, "Initilising GDT\n");

    // Null descriptor
    gdt_set_gate(&gdt.null, 0, 0, 0, 0);
    gdt_set_gate(&gdt.null2, 0, 0, 0, 0);

	// Kernel segments
    gdt_set_gate(&gdt.r0_code, 0, 0, GDT_RING0_CODE, GDT_GRANULARITY);
    gdt_set_gate(&gdt.r0_data, 0, 0, GDT_RING0_DATA, GDT_GRANULARITY);

    // Applications segments
    gdt_set_gate(&gdt.r3_code, 0, 0, GDT_RING3_CODE, GDT_GRANULARITY);
    gdt_set_gate(&gdt.r3_data, 0, 0, GDT_RING3_DATA, GDT_GRANULARITY);

	// TSS
    // gdt_set_tss_gate(&gdt.tss, &tss, sizeof(tss), 0b10001110, GDT_GRANULARITY);
	// Bit 7:   Present				1
	// Bit 5-6: Access Level 		00
	// Bit 4: Type (1=CS/DS, 0=TSS)	0
	// Bit 3: Exe (1=CS, 0=Data)	0
	// Bit 2: Direction/Confirming  0
	// Bit 1: R/W					1
	// Bit 0: Accessed				0
    //gdt_set_tss_gate(&gdt.tss, &tss, sizeof(tss), 0b10001110, GDT_GRANULARITY);
	tss.rsp0 = (uint64_t)&kernel_stack+sizeof(kernel_stack);
	tss.rsp1 = 0x000000;
	tss.rsp2 = 0x000000;
	tss.ist1 = 0x000000;
	tss.ist2 = 0x000000;
	tss.ist3 = 0x000000;
	tss.ist4 = 0x000000;
	tss.ist5 = 0x000000;
	tss.ist6 = 0x000000;
	tss.ist7 = 0x000000;

    // gdt_set_tss_gate(&gdt.tss, &tss, sizeof(tss)-1, 0b10001110, GDT_GRANULARITY);
    gdt_set_tss_gate(&gdt.tss, &tss, sizeof(tss)-1, 0b10001001, GDT_GRANULARITY);

	struct descriptor descriptor;
	descriptor.size = sizeof(gdt)-1;
	descriptor.offset = (uint64_t )&gdt;
	klog(LL_DEBUG, "GDT Location:      0x%x_%x\n", descriptor.offset>>32, descriptor.offset);
	klog(LL_DEBUG, "GDT Size:          %d\n", descriptor.size);
	klog(LL_DEBUG, "Kerrnel Stack:     %x\n", (uint64_t)&kernel_stack);

	// Load GDT
	__asm__ volatile (
		"cli;"
		"lgdtq [%0]" : : "r"(&descriptor)
	);

	gdt_write(0x8, 0x10, 0x28);


}
