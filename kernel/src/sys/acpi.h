#ifndef __acpi_h__
#define __acpi_h__

// Should be "packed" - throws error though
typedef struct{
  char signature[4];
  uint32_t length;
  uint8_t revision;
  uint8_t checksum;
  char OEMID[6];
  char OEMTableID[8];
  uint32_t OEMRevision;
  uint32_t creatorID;
  uint32_t creatorRevision;
} ACPI_XSDT_Header;

typedef struct {
    char signature[4];    // 'HPET' in case of HPET table
    uint32_t length;
    uint8_t revision;
    uint8_t checksum;
    char oemid[6];
    uint64_t oem_tableid;
    uint32_t oem_revision;
    uint32_t creator_id;
    uint32_t creator_revision;
} ACPI_HPET_Header;

void init_acpi(void);


#endif
