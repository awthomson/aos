#include <common/common.h>

void parse_mmap();
void *malloc(uint64_t size);
void *kmalloc(uint64_t size, void *pml4);
void memset(uint8_t *ptr, uint8_t x, uint64_t size);
void init_mem();
void load_pd();
uint64_t *get_pml4_addr();
//void map_phys_address_2m(uint64_t *phys, uint64_t *virt, void *pml4_ptr);
void map_phys_address_4k(uint64_t *phys, uint64_t *virt, void *pml4_ptr);
uint64_t *allocate_pt(uint64_t val, uint64_t inc);
void memcpy(void *src, void *dest, uint64_t size);
void load_kernel_pml4(void);
void load_pml4(uint64_t pml4);
void get_mem(uint64_t *res);
void mark_page_used(int page);
void mark_page_free(int page);
int is_page_used(int page);
int find_next_free_block(int num_bytes);
void free(void *addr);

void *kernel_pml4;
