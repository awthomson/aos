/*
	IDT + Various Interrupt stuff

	PIC (Programmable Interrupt Controller) - this is a chip (8259A) that takes hardware interrupts and sends them to the approprate system interrupts.
		This is replaced by the APIC on modern systems.
	IRR - Interrupt Requests waiting for aknowledgment
	ISR - Interrupt Requests that have been been aknowledged and are waiting for an EOI

	0x20: Master PIC
	0xA0: Slave PIC

	Links:
		https://wiki.osdev.org/Interrupt_Descriptor_Table
		https://wiki.osdev.org/8259_PIC
		
*/

#include <common/logging.h>
#include <common/common.h>
#include <sys/ioapic.h>
#include <sys/lapic.h>
#include <sys/exceptions.h>
#include <sys/scheduler.h>
#include <sys/memory.h>
#include <drivers/keyboard.h>
#include <drivers/mouse.h>
#include "interrupts.h"

static struct idt_entry_t idt_table[256];
static void set_idt_entry(uint8_t e, uint8_t flags, void *base);

void init_idt(void)
{

    klog(LL_INFO, "Initilising Interrupts...\n");

	// Set all of them to "unhandled" by default
	for (int i=0; i<255; i++)
		set_idt_entry( i, IDT_FLAG_KERN | IDT_FLAG_INT, &isr_unhandled);

	// Exceptions
	set_idt_entry(IRQ_UD,		 IDT_FLAG_KERN | IDT_FLAG_INT, &isr_invalid_opcode);
	set_idt_entry(IRQ_GPF,		 IDT_FLAG_KERN | IDT_FLAG_INT, &isr_gpf);
	set_idt_entry(IRQ_PAGEFAULT, IDT_FLAG_KERN | IDT_FLAG_INT, &isr_pagefault);
	
	// Interrupts
	set_idt_entry(IRQ_MOUSE, 	 IDT_FLAG_USER | IDT_FLAG_INT, &isr_mouse);
	set_idt_entry(IRQ_KEYPRESS,  IDT_FLAG_USER | IDT_FLAG_INT, &isr_keyboard);
	set_idt_entry(IRQ_TIMER, 	 IDT_FLAG_USER | IDT_FLAG_INT, &irq_preempt);

    struct idt_ptr_t idt_ptr = {
        sizeof(idt_table) - 1, 
		(uint64_t)idt_table
    };

    asm volatile( "lidt [%0]" : : "p"(&idt_ptr) );

	// Disable PIC
	outb(0xa1, 0xff);
	outb(0x21, 0xff);

}


/*
	FLAGS
		Bit 7: 		Present (1)
		Bits 6, 5: 	DPL
		Bit 4: 		Storage Segment (0)
		Bit 0-3:	Type (1110=interrupt, 1111=trap)
		1 11 0 1110 = 
*/
static void set_idt_entry(uint8_t e, uint8_t flags, void *base)
{
	uint64_t b = (uint64_t)base;
	idt_table[e].base_1 = (uint16_t)b;
	idt_table[e].sel	= 0x8;							// CS in GDT
	idt_table[e].zero	= 0;
	idt_table[e].flags	= flags | 0b10000000;			// Always set present bit
	idt_table[e].base_2	= (uint16_t)(b>>16);
	idt_table[e].base_3 = (uint32_t)(b>>32);
	idt_table[e].zero2	= 0;
}

__attribute__((interrupt)) void isr_mouse(void *frame) {
	read_mouse();
	send_eoi(IRQ_MOUSE);
}

__attribute__((interrupt)) void isr_keyboard(void *frame) {
	read_keypress();
	send_eoi(IRQ_KEYPRESS);
}

/*
	This must be called at the end of the interrupt
	to tell the CPU that that request has finished processing
*/
void send_eoi(int irq)
{
    uint32_t reg = APIC_REG_ISR_BASE + 0x10 * (irq / 32);
    if (lapic_read(reg) & (1 << (irq % 32))) {
    	lapic_write(0xB0, 0);
	}
}



