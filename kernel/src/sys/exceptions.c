#include <common/logging.h>
#include <common/stdint.h>
#include <sys/interrupts.h>
#include <drivers/serial/serial.h>

#include "exceptions.h"

__attribute__((interrupt)) void isr_unhandled(struct exception_frame *f) {
	com1_printf("Unhandled Exception\n");
	while(1);
    klog(LL_FATAL, "Unhandled exception\n");
    klog(LL_DEBUG, "Flags:  %x\n", f->flags);
    klog(LL_DEBUG, "RIP:    %x%x\n", (uint64_t)(f->rip)>>32, (uint64_t)f->rip);
    while(1);
}

__attribute__((interrupt)) void isr_invalid_opcode(struct exception_frame *f) {
    com1_printf("Kernal aborting abnormally (Invalid Opcode)\n");
    com1_printf("    IP: %x_%x\n", (uint64_t)(f->rip)>>32, f->rip);
    com1_printf(" Flags: %x\n", f->flags);
	display_regs();
    while(1);
}

__attribute__((interrupt)) void isr_gpf(struct exception_frame *f) {
    com1_printf("Kernal aborting abnornally (GPF)\n");
    com1_printf("    IP: %x_%x\n", (uint64_t)(f->rip)>>32, f->rip);
    com1_printf(" Flags: %x\n", f->flags);
	display_regs();
	while(1);
}

void display_regs(void)
{
    uint64_t cr2, cr3;
    asm volatile(
        "mov rax, cr2;"
        "mov %0, rax;"
        "mov rax, cr3;"
        "mov %1, rax;"
    :"=m"(cr2),"=m"(cr3)::);
    com1_printf("   CR2: 0x%x\n", cr2);
    com1_printf("   CR3: 0x%x\n", cr3);
}

__attribute__((interrupt)) void isr_pagefault(struct exception_frame *f) {
	com1_printf("\nKernal aborting abnornally (Page Fault)\n");
    com1_printf("    IP: %x_%x\n", (uint64_t)(f->rip)>>32, f->rip);
    com1_printf(" Flags: %x\n", f->flags);
	display_regs();
	switch(f->flags) {
		case 0:
			com1_printf("Supervisory process tried to read a non-present page entry\n");
			break;
		case 1:
			com1_printf("Supervisory process tried to read a page and caused a protection fault\n");
			break;
		case 2:
			com1_printf("Supervisory process tried to write to a non-present page entry\n");
			break;
		case 3:
			com1_printf("Supervisory process tried to write a page and caused a protection fault\n");
			break;
		case 4:
			com1_printf("User process tried to read a non-present page entry\n");
			break;
		case 5:
			com1_printf("User process tried to read a page and caused a protection fault\n");
			break;
		case 6:
			com1_printf("User process tried to write to a non-present page entry\n");
			break;
		case 7:
			com1_printf("User process tried to write a page and caused a protection fault\n");
			break;
		default:
			com1_printf("???\n");
			break;
			
	}
    while(1);
}

