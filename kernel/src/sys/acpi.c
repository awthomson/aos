#include <common/bootboot.h>
#include <common/stdint.h>
#include <common/logging.h>

#include "acpi.h"

// https://wiki.osdev.org/XSDT

extern BOOTBOOT bootboot;

int doChecksum(ACPI_XSDT_Header *tableHeader);

void init_acpi(void)
{
	klog(LL_INFO, "Initilising ACPI\n");
	ACPI_XSDT_Header *acpi_hdr = (ACPI_XSDT_Header *)bootboot.arch.x86_64.acpi_ptr;

	klog(LL_DEBUG, "XSDT location:     0x%x\n", bootboot.arch.x86_64.acpi_ptr);
	int cs = doChecksum(acpi_hdr);
	if (cs != 0) {
		klog(LL_FATAL, "Invalid checksum for XSDT header table\n");
		while(1);
	}

	int num = acpi_hdr->length - sizeof(ACPI_XSDT_Header);
	num = num / 8;

	uint32_t *ptr = (uint32_t *)acpi_hdr+(sizeof(ACPI_XSDT_Header)/4);
	for (int i=0; i<num; i++) {
		uint64_t *entry = (uint64_t *)&ptr[i*2];
		uint64_t *entry_ptr = (uint64_t *)entry[0];
		uint32_t entry_type = entry_ptr[0];
	
		// klog("[DEBUG] Entry %d pointer: %x\n", i, entry_ptr);
		if (entry_type == 0x50434146 ) {
			// FACP
			klog(LL_DEBUG, " * Entry %d type:   0x%x (FACP), ptr: 0x%x\n", i, entry_type, entry_ptr);
		} else if (entry_type ==  0x43495041 ) {
			// APIC
			klog(LL_DEBUG, " * Entry %d type:   0x%x (APIC), ptr: 0x%x\n", i, entry_type, entry_ptr);
		} else if (entry_type ==  0x54455048 ) {
			// HPET
			klog(LL_DEBUG, " * Entry %d type:   0x%x (HPET), ptr: 0x%x\n", i, entry_type, entry_ptr);
			// init_hpet((ACPI_HPET_Header *)entry_ptr);
		} else if (entry_type ==  0x54524742 ) {
			// BGRT
			klog(LL_DEBUG, " * Entry %d type:   0x%x (BGRT), ptr: 0x%x\n", i, entry_type, entry_ptr);
		} else {
			// Not one of those others
			klog(LL_DEBUG, "  * Entry %d type:   0x%x (unknown), ptr: 0x%x\n", i, entry_type, entry_ptr);
		}
	}

}


int doChecksum(ACPI_XSDT_Header *tableHeader)
{
    unsigned char sum = 0;
 
    for (int i = 0; i < tableHeader->length; i++)
    {
        sum += ((char *) tableHeader)[i];
    }
 
    return sum;
}
