#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

#include <sys/ipc.h>

#define STATE_INIT  0
#define STATE_RUNNING 1
#define STATE_WAITING 2


/*
	Information stored about the process
*/
struct process_info {
    uint64_t rip;					// 0x00
    uint64_t cs;					// 0x08
    uint64_t flags;					// 0x10
    uint64_t rsp;					// 0x18
    uint64_t ss;					// 0x20
	
	uint64_t rbp;					// 0x28
    uint64_t rax;					// 0x30
    uint64_t rbx;					// 0x38
    uint64_t rcx;					// 0x40
    uint64_t rdx;					// 0x48
    uint64_t rdi;					// 0x50
    uint64_t rsi;					// 0x58
    uint64_t r8;					// 0x60
    uint64_t r9;					// 0x68
    uint64_t r10;					// 0x70
    uint64_t r11;					// 0x78
    uint64_t r12;					// 0x80
    uint64_t r13;					// 0x88
    uint64_t r14;					// 0x90
    uint64_t r15;					// 0x98

    uint64_t pml_ptr;				// 0xa0
    uint64_t pid;					// 0xa8
    uint8_t state;					// 0xb0
    uint8_t priority;				// 0xb1
	uint64_t kernel_pml;			// 0xb2 - TODO: Check why is this needed
	uint64_t wait_until;			// 0xba

	uint64_t cwd;					// 0xbe

    struct process_info* parent;

    char *stdin_buffer;
    uint64_t stdin_buffer_pos;
    char *stdout_buffer;
    uint64_t stdout_buffer_pos;
	

}__attribute__(( packed ));

/*
	Linked list storing all the processes
*/
struct process_list_node {
    struct process_info *info;
    struct process_list_node *next;
};

struct process_info *curr_proc_info;
struct process_list_node *first_process;
struct process_list_node *current_process;

void debug_processes(void);
void init_scheduler(void);
uint64_t load_elf(void *elf_addr, uint64_t elf_size, char **args, int argc);
void *create_new_pml4(void);
void jump_usermode(void *code_entry_point, void *stack_ptr, void *pml4_ptr);
uint64_t new_process(void *entry_ptr, void *pml4_ptr, char **args, int argc);
void switch_next_process(void);
void *get_current_proc(void);
void exec(uint32_t inode, char **args, uint64_t *pid);
void debug_ps(void);
int kill(uint64_t pid);
int get_proc_info(struct process_info *dest, uint64_t pid);
struct process_info *get_proc_ptr(uint64_t pid);

#endif
