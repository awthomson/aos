#ifndef __INTERRUPTS_H__
#define __INTERRUPTS_H__

#include <common/stdint.h>
#include <sys/scheduler.h>

// Exceptions
#define IRQ_UD			0x06
#define IRQ_GPF			0x0d
#define IRQ_PAGEFAULT	0x0e

// Interrupts
// #define IRQ_KEYPRESS	32
#define IRQ_KEYPRESS	1
#define IRQ_MOUSE       0x82
#define IRQ_TIMER		0x69
#define IRQ_SYSREQ		0x80
#define IRQ_DISK		0x81

#define IDT_FLAG_KERN	0b00000000
#define IDT_FLAG_USER	0b01100000
#define IDT_FLAG_INT	0b00001110


/*
#define SYSCALL_PRINT_NUM       0x0
#define SYSCALL_PRINT_CHAR      0x1
#define SYSCALL_WAIT            0x2
#define SYSCALL_CHECK_KEYPRESS  0x3
#define SYSCALL_READ_SECTOR     0x4
#define SYSCALL_DRAW_CHAR		0x5
*/


/* Defines an IDT entry */
struct idt_entry_t {
    uint16_t base_1;
    uint16_t sel;
    uint8_t zero;
    uint8_t flags;
    uint16_t base_2;
    uint32_t base_3;
    uint32_t zero2;
} __attribute__((packed));

struct idt_ptr_t {
    uint16_t limit;
    uint64_t base;
} __attribute__((packed));

struct isr_stack_frame {
	uint64_t rip;		
	uint64_t cs;		
	uint64_t flags;		
	uint64_t rsp;		
	uint32_t ss;		
} __attribute__((packed));

void init_idt(void);
void send_eoi(int irq);

__attribute__((naked,noreturn)) void irq_preempt(void *frame);
__attribute__((interrupt)) void isr_keyboard(void *frame);
__attribute__((interrupt)) void isr_mouse(void *frame);
void timer_irq(void *frame);

#endif

