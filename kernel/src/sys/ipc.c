#include <sys/ipc.h>
#include <sys/memory.h>
#include <sys/scheduler.h>
#include <common/logging.h>

uint64_t msg_id;

void init_ipc(void)
{
    klog(LL_INFO, "Initialising IPC\n");
    msg_id = 1;
	for (int i=0; i<100; i++)
		ipc_msg[i].id = 0;	
}

/*
	Returns:
		ID of message (or 0 if no memssage)
*/
uint64_t ipc_check(uint64_t *type, uint64_t *from_pid)
{
	for (int i=0; i<100; i++) {
        if ((ipc_msg[i].dest_pid == current_process->info->pid) && (ipc_msg[i].id != 0)) {
            *type = ipc_msg[i].type;
            *from_pid = ipc_msg[i].src_pid;
            return ipc_msg[i].id;
        }
	}
	return 0;
}

uint64_t ipc_get_64(uint64_t id)
{
	for (int i=0; i<100; i++) {
		if (ipc_msg[i].id == id) {
			ipc_msg[i].dest_pid = 0;
			ipc_msg[i].src_pid = 0;
			ipc_msg[i].id = 0;
			return ipc_msg[i].data;
		}
	}
	return 0;
}

void ipc_send_64(uint64_t dest_proc, uint64_t val, uint64_t type)
{

	for (int i=0; i<100; i++) {

		if (ipc_msg[i].id == 0) {
			ipc_msg[i].id = msg_id++;
			ipc_msg[i].data = val;
			ipc_msg[i].type = type;
			ipc_msg[i].dest_pid = dest_proc;
			ipc_msg[i].src_pid = current_process->info->pid;
			return;
		}

	}

	klog(LL_ERROR, "No room in IPC queue\n", 0);
	while(1);

}

