/*
	HPET

	Use the high-precision timer on modern CPUs.  This replaces the legacy PIT timer, which
	we use once here to calibrate the HPET timer (by counting how many ticks are in a second).

	Links:
		https://wiki.osdev.org/HPET
		https://wiki.osdev.org/HPET#HPET_operating_modes

*/

#include <common/logging.h>
#include <common/stdint.h>
#include <common/common.h>
#include <sys/pit.h>

#include "hpet.h"

void init_hpet(void)
{
	klog(LL_INFO, "Initialising HPET\n");

	// This doesn't seem to have any effect		
	//	disable_pic();

	// Set bit 8 in 0xfee00000 - why????
	uint32_t val = phy_mem_read32(0xfee00000);
	val |= (1<<8);
	phy_mem_write32(0xfee000f0, val);

	// Get the clock period from the general capabilities register
    // uint64_t *p = (uint64_t*) HPET_ADDR + GENERAL_CAPABILITIES;
    // uint64_t info = *p;
    // uint32_t period = info >> 32;

	// Enable the HPET timer (start timing)
	hpet_enable();

	/* Count how many ticks in 1s */
	uint64_t old_time = hpet_get_time();
    pit_wait(500);
	uint64_t new_time = hpet_get_time();
	ticks_per_sec = (new_time - old_time)*2;
    klog(LL_DEBUG, "%d ticks per second\n", ticks_per_sec);

	// Configure timer #1
    //uint64_t *t1_conf = (uint64_t *)(HPET_ADDR + TIMER_1_CONFIG);
	// Periodic IRQ = 2
    //*t1_conf = (3<< 9) | (1 << 2) | (1 << 3) | (1 << 6);

	// Get information about timer #1
	/*
	uint64_t t1_info = *t1_conf;
	klog(LL_DEBUG, "T1 info:            %g\n", t1_info);
	klog(LL_DEBUG, "T1_INT_ROUTE_CAP:   %d\n", get_range(t1_info, 63, 32));
	klog(LL_DEBUG, "T1_FSB_INT_DEL_CAP: %d\n", is_bit_set(t1_info, 15));
	klog(LL_DEBUG, "T1_FSB_EN_CNF:      %d\n", is_bit_set(t1_info, 14));
	klog(LL_DEBUG, "T1_FSB_EN_CNF:      %d\n", is_bit_set(t1_info, 14));
	klog(LL_DEBUG, "T1_INT_ROUTE_CNF:   %d\n", get_range(t1_info, 13, 9));
	klog(LL_DEBUG, "T1_32MODE_CNF:      %d\n", is_bit_set(t1_info, 8));
	klog(LL_DEBUG, "T1_VAL_SET_CNF:     %d\n", is_bit_set(t1_info, 6));
	klog(LL_DEBUG, "T1_SIZE_CAP:        %d\n", is_bit_set(t1_info, 5));
	klog(LL_DEBUG, "T1_PER_INT_CAP:     %d\n", is_bit_set(t1_info, 4));
	klog(LL_DEBUG, "T1_TYPE_CNF:        %d\n", is_bit_set(t1_info, 3));
	klog(LL_DEBUG, "T1_INT_ENB_CNF:     %d\n", is_bit_set(t1_info, 2));
	klog(LL_DEBUG, "T1_INT_TYPE_CNF:    %d\n", is_bit_set(t1_info, 1));
	*/

	// Set the comparitor to how often you want to trigger the IRQ
	// Setting this to the number of ticks in 1s will trigger every second
//    uint64_t *t1_comp = (uint64_t *)(HPET_ADDR + TIMER_1_COMPARATOR);
//	*t1_comp = hpet_get_time();

/*
write_register_64(timer_comparator(n), read_register(main_counter) + time);
write_register_64(timer_comparator(n), time);
*/

}

/*
void disable_pic(void) {

    klog(LL_DEBUG, "Disabling PIC\n");

    // Set ICW1 
    outb(0x20, 0x11);
    outb(0xa0, 0x11);

    /. Set ICW2 (IRQ base offsets) 
    outb(0x21, 0xe0);
    outb(0xa1, 0xe8);

    // Set ICW3 
    outb(0x21, 4);
    outb(0xa1, 2);

    // Set ICW4 
    outb(0x21, 1);
    outb(0xa1, 1);

    // Set OCW1 (interrupt masks) 
    outb(0x21, 0xff);
    outb(0xa1, 0xff);
}
*/


uint32_t phy_mem_read32(uint64_t addr) {
	uint32_t *p = (uint32_t *)addr;
	return *p;
}

void phy_mem_write32(uint64_t addr, uint32_t val) {
	uint32_t *p = (uint32_t *)addr;
	*p = val;
}

int is_bit_set(uint64_t value, uint8_t bit_num) {
	value = value & (1 << bit_num);
	return value >> bit_num;
}

uint64_t get_range(uint64_t value, uint8_t highest, uint8_t lowest) {
	value = value >> lowest;
	for (int i=(highest-lowest)+1; i<64; i++) {
		value &= ~(1UL << i);
	}
	return value;
}


void hpet_set_time(int timer, uint64_t time)
{
	// HPET timer must be disabled for this
	hpet_disable();

    uint64_t *t = (uint64_t*)(HPET_ADDR + MAIN_COUNTER);
	*t = 0;
	
}

uint64_t hpet_get_time(void)
{
    uint64_t *t = (uint64_t*)(HPET_ADDR + MAIN_COUNTER);
    uint64_t time = *t;
	return time;
	
}

uint64_t get_uptime_ns(void)
{
    uint64_t *t = (uint64_t*)(HPET_ADDR + MAIN_COUNTER);
    uint64_t time = *t;
	return (time * 1000000)/ticks_per_sec;
		
}

void hpet_enable(void)
{
    uint64_t *g_conf = (uint64_t *)(HPET_ADDR + GENERAL_CONFIG);
    *g_conf = 1;
}

void hpet_disable(void)
{
    uint64_t *g_conf = (uint64_t *)HPET_ADDR + GENERAL_CONFIG;
    *g_conf = 0;
}
