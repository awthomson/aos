bits 64
global gdt_write
; rdi = 1st argument (CS)
; si  = 2nd argument (DS, ES, FS, GS, SS)
; dx  = 3rd argument (TSS - unused for now)
gdt_write:
	mov ax, 0
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    mov ax, si
    ;mov ds, ax
    ;mov es, ax
    ;mov fs, ax
    ;mov gs, ax
    mov ss, ax
    push qword rdi
    lea rax, [rel .next]
    push rax
    o64 retf
.next:
    ltr dx
    ret

