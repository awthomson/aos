bits 64

global irq_preempt

extern current_process;
extern first_process;
extern curr_proc_info
extern next_proc_info
extern kernel_pml4
extern send_eoi
extern get_uptime_ns

num_task_switches:	dw	0

irq_preempt:

	; Save this for a bit later
	push	rax

	; Reload kernel memory map
	mov		rax,[kernel_pml4]
	mov		cr3, rax

	; Store current values in 
	mov		rax,[curr_proc_info]
	mov qword	[rax+0x28], rbp
	mov qword	[rax+0x38], rbx
	mov qword	[rax+0x40], rcx
	mov qword	[rax+0x48], rdx
	mov qword	[rax+0x50], rdi
	mov qword	[rax+0x58], rsi
	mov qword	[rax+0x60], r8
	mov qword	[rax+0x68], r9
	mov qword	[rax+0x70], r10
	mov qword	[rax+0x78], r11
	mov qword	[rax+0x80], r12
	mov qword	[rax+0x88], r13
	mov qword	[rax+0x90], r14
	mov qword	[rax+0x98], r15
	

	; Save RAX
	pop	rax
	mov		rbx,[curr_proc_info]
	mov		[rbx+0x30], rax
	
	mov			rax, [curr_proc_info]
	mov			rbx,[rsp]
	mov qword	[rax+0x00], rbx		; RIP	
	mov			rbx,[rsp+8]
	mov qword	[rax+0x08], rbx		; CS
	mov			rbx,[rsp+16]
	mov qword	[rax+0x10], rbx		; Flags
	mov			rbx,[rsp+24]
	mov qword	[rax+0x18], rbx		; RSP	
	mov			rbx,[rsp+32]
	mov qword	[rax+0x20], rbx		; SS


next_proc:
	; Go to the next process in the list
	mov		rax,[current_process]
	mov		rbx, [rax+0x8]
	cmp		rbx, 0					; If at the end of the list, start again
	jne		.n
	mov		rbx,[first_process]
.n:	mov		[current_process], rbx

	; Point curr_proc_info at the next info
	mov		rax,[current_process]
	mov		rbx, [rax]
	mov		[curr_proc_info], rbx

	; Check if the new thread is sleeping
	mov		rax, [curr_proc_info]
	mov		al, [rax+0xb0]
	cmp		al, 0x02
	jne		done_switch
	
	; Check if we're finished waiting
	mov		rax, [curr_proc_info]
	mov		rbx, [rax+0xba]
	call	get_uptime_ns
	cmp		rax, rbx				; Haven't finished waiting - go to the next in the list
	jl		next_proc

	; Change to  state back to "running"
	mov		rax, [curr_proc_info]
	mov byte	[rax+0xb0], 0x01

done_switch:	

	; CS, RIP, Flags, SS, RSP is stored on the stack
	mov		rax,[curr_proc_info]
	mov		rbx,[rax]				
	mov		[rsp], rbx				; RIP
	mov		rbx,[rax+0x08]			
	mov		[rsp+0x08], rbx			; CS
	mov		rbx,[rax+0x10]			
	mov		[rsp+0x10], rbx			; Flags
	mov		rbx,[rax+0x18]			
	mov		[rsp+0x18], rbx			; RSP
	mov		rbx,[rax+0x20]			
	mov		[rsp+0x20], rbx			; SS


	mov		rax,[num_task_switches]
	inc		rax
	mov		[num_task_switches], rax

	; Visual indicator that this is triggering
;	mov		rax, 0xfffffffffc000000
;	mov		cx, 0
;	mov		edx, [num_task_switches]
;	rol		edx, 3
;.o: mov	dword	[rax], edx
;	add		rax, 4
;	add		cx, 1
;	cmp		cx, 1280
;	jle		.o


	; End end-of-interrupt
	mov		rdi, 0x69
	call	send_eoi 

	; Restore the values of the new process
	mov			rax, [curr_proc_info]
	;mov qword	rbp, [rax+0x30]
	;push		rbp					; This is actually going to be our RAX
	mov qword	rbp, [rax+0x28]
	mov qword	rbx, [rax+0x38]
	mov qword	rcx, [rax+0x40]
	mov qword	rdx, [rax+0x48]
	mov qword	rdi, [rax+0x50]
	mov qword	rsi, [rax+0x58]
	mov qword	r8 , [rax+0x60]
	mov qword	r9 , [rax+0x68]
	mov qword	r10, [rax+0x70]
	mov qword	r11, [rax+0x78]
	mov qword	r12, [rax+0x80]
	mov qword	r13, [rax+0x88]
	mov qword	r14, [rax+0x90]
	mov qword	r15, [rax+0x98]

	; Load the new memory map
	mov		rax, [curr_proc_info]
	mov		rax, [rax+0xa0]			; PML4 pointer
	mov		cr3, rax

	; Not sure how RAX is getting magically restored when returning to user-space
	;pop		rax

	iretq
