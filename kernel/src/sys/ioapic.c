/*
	I/O APIC

	Provides multi-processor interrupt management.  

	Links:
		https://wiki.osdev.org/APIC

*/

#include <common/logging.h>
#include <common/stdint.h>
#include <sys/interrupts.h>

#include "ioapic.h"

#define IOAPIC_BASE 0xfec00000

void init_ioapic(void) {

	klog(LL_INFO, "Initialising I/O APIC\n");

	ioapic_set_irq(1,    0, IRQ_KEYPRESS);
    ioapic_set_irq(12, 0, IRQ_MOUSE);
    // ioapic_set_irq(0x12, 0, IRQ_MOUSE);

}

void write_ioapic_register(const uint8_t offset, const uint32_t val) 
{
    *(volatile uint32_t*)(IOAPIC_BASE) = offset;
    *(volatile uint32_t*)(IOAPIC_BASE + 0x10) = val; 
}
 
uint32_t read_ioapic_register(const uint8_t offset)
{
    *(volatile uint32_t*)(IOAPIC_BASE) = offset;
    return *(volatile uint32_t*)(IOAPIC_BASE + 0x10);
}

void ioapic_set_irq(uint8_t irq, uint64_t apic_id, uint8_t vector)
{

    const uint32_t low_index = 0x10 + irq*2;
    const uint32_t high_index = 0x10 + irq*2 + 1;

    uint32_t high = read_ioapic_register(high_index);

    // set APIC ID
    high &= ~0xff000000;
    high |= apic_id << 24;
    write_ioapic_register(high_index, high);

    uint32_t low = read_ioapic_register(low_index);

    // unmask the IRQ
    low &= ~(1<<16);

    // set to physical delivery mode
    low &= ~(1<<11);

    // set to fixed delivery mode
    low &= ~0x700;

    // set delivery vector
    low &= ~0xff;
    low |= vector;

    write_ioapic_register(low_index, low);
}

