#ifndef __ui_h__
#define __ui_h__

#include "../common/stdint.h"
#include "../drivers/graphics/graphics.h"

void ui_draw_window(uint16_t x, uint16_t y, uint16_t w, uint16_t h);

#endif
