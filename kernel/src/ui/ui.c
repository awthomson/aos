#include "ui.h"

void ui_draw_window(uint16_t x, uint16_t y, uint16_t w, uint16_t h ) {
	gfx_draw_rect(x, y, w, h, 0xFFFFFF);
	gfx_draw_filled_rect(x+1, y+1, w-2, 30, 0x0808080);
	gfx_draw_filled_rect_alpha(x+1, y+30, w-2, h-32, 0x0, 0.5f);
}
