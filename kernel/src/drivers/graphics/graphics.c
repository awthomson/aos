#include <common/bootboot.h>
#include <common/logging.h>
#include "font.h"
#include "graphics.h"

extern BOOTBOOT bootboot;
extern uint8_t fb;

uint32_t *wfb;

void gfx_set_working_fb(uint32_t *addr)
{
	wfb = addr;
}

void gfx_draw_img(uint32_t *src, uint16_t pos_x, uint16_t pos_y, uint16_t width, uint16_t height)
{
	uint32_t *lfb= (uint32_t *)0xfffffffffc000000;
    uint32_t *dest = lfb + (pos_x) + (pos_y*SCREEN_WIDTH);
    for (int y=0; y<height; y++) {
        for (int x=0; x<width; x++) {
            *dest++ = *src++;
        }
        dest += (SCREEN_WIDTH-width);
    }
}


void gfx_draw_area(uint16_t pos_x, uint16_t pos_y, uint16_t width, uint16_t height)
{
	uint32_t *lfb= (uint32_t *)0xfffffffffc000000;
	uint32_t *dest = lfb + (pos_x) + (pos_y*SCREEN_WIDTH);
	uint32_t *src  = wfb + (pos_x) + (pos_y*SCREEN_WIDTH);
	for (int y=0; y<height; y++) {
		for (int x=0; x<width; x++) {
			*dest = *src;
			dest++;
			src++;
		}
		dest += (SCREEN_WIDTH-width);
		src  += (SCREEN_WIDTH-width);
	}
}

void gfx_scroll_up(uint32_t lines)
{
	uint32_t *dest = (uint32_t *)0xfffffffffc000000;
	uint32_t *src= (uint32_t *)0xfffffffffc000000+(lines*1280);
	int i = 0;
	for (i=0; i<1280*1024; i++) {
		*dest++ = *src++;
	}
}

void gfx_clear_screen(uint32_t colour) {

    int w=bootboot.fb_width, h=bootboot.fb_height;

	uint32_t *lfb = (uint32_t *)0xfffffffffc000000;
	for (int i=0; i<w*h; i++) {
		lfb[0] = colour;
		lfb++;
	}

}

void gfx_draw_pixel(uint16_t x, uint16_t y, uint32_t colour) {
    int w=bootboot.fb_width;
	uint32_t *lfb = (uint32_t *)0xfffffffffc000000;
	lfb[x+(y*w)] = colour;
}

void gfx_draw_char(uint16_t x, uint16_t y, uint32_t colour, uint8_t chr) {
	if (chr < 32)
		return;
    int font_height = 24;
    uint16_t font_offset = (chr-' ')*font_height*2;
    for (int fy=0; fy<font_height; fy++) {
        for (int fx=0; fx<16; fx++) {
            uint16_t font_line = (uint16_t)font_data[font_offset+(fy*2)];
            font_line += font_data[font_offset+(fy*2)+1]<<8;
            if (font_line & (1<<fx)) {
                gfx_draw_pixel(x+fx, y+fy, colour);
            }
        }
    }
}

void gfx_draw_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint32_t colour) {
	uint32_t *lfb = (uint32_t *)0xfffffffffc000000;
    int fbw=bootboot.fb_width;
	for (uint16_t dy=y; dy<y+h; dy++) {
		lfb[x+(dy*fbw)] = colour;
		lfb[x+w+(dy*fbw)] = colour;
	}
	for (uint16_t dx=x; dx<x+w; dx++) {
		lfb[dx+(y*fbw)] = colour;
		lfb[dx+((y+h)*fbw)] = colour;
	}
}

void gfx_draw_filled_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint32_t colour) {
	uint32_t *lfb = (uint32_t *)0xfffffffffc000000;
    int fbw=bootboot.fb_width;
	for (uint16_t dy=y; dy<y+h; dy++) {
		for (uint16_t dx=x; dx<x+w; dx++) {
			lfb[dx+(dy*fbw)] = colour;
		}
	}
}

void gfx_draw_filled_rect_alpha(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint32_t colour, float alpha) {
	float t = 1.0f - alpha;
    uint8_t *lfb = (uint8_t *)0xfffffffffc000000;
	uint8_t dest_r = (colour & 0x00ff0000) >> 16;
	uint8_t dest_g = (colour & 0x0000ff00) >> 8;
	uint8_t dest_b = (colour & 0x000000ff) >> 0;
    int fbw=bootboot.fb_width;
    for (uint16_t dy=y; dy<y+h; dy++) {
        for (uint16_t dx=x; dx<x+w; dx++) {
			uint32_t off = (dx+(dy*fbw))*4; 
			uint8_t bg_r   = lfb[off];
			uint8_t bg_g   = lfb[off+1];
			uint8_t bg_b   = lfb[off+2];
			int		tmp_r  = bg_r+((dest_r - bg_r) * t);
			int		tmp_g  = bg_g+((dest_g - bg_g) * t);
			int		tmp_b  = bg_b+((dest_b - bg_b) * t);
            lfb[off]   = (uint8_t)tmp_r;
            lfb[off+1] = (uint8_t)tmp_g;
            lfb[off+2] = (uint8_t)tmp_b;
        }
    }
}


void gfx_draw_string(uint16_t x, uint16_t y, uint32_t colour, char *s) {
	uint16_t origx = x;
	uint8_t c = s[0];
	int ptr = 0;
	while (c != 0) {
		if (c == '\n') {
			y += 24;
			x = origx;
		} else {
			gfx_draw_char(x, y, colour, c);
			x+=16;
		}
		c = s[++ptr];	
	}
}

void gfx_draw_buffer(uint32_t *buff)
{
	uint32_t *lfb = (uint32_t *)0xfffffffffc000000;
	for (int i=0; i<SCREEN_WIDTH*SCREEN_HEIGHT; i++)
		*lfb++ = *buff++;
}
