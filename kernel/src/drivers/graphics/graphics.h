#ifndef __graphics_h__
#define __graphics_h__

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 800

#include <common/stdint.h>

void gfx_set_working_fb(uint32_t *addr);
void gfx_clear_screen(uint32_t colour);
void gfx_draw_pixel(uint16_t x, uint16_t y, uint32_t colour);
void gfx_draw_char(uint16_t x, uint16_t y, uint32_t colour, uint8_t chr);
void gfx_draw_string(uint16_t x, uint16_t y, uint32_t colour, char *s);
void gfx_draw_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint32_t colour);
void gfx_draw_filled_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint32_t colour);
void gfx_draw_filled_rect_alpha(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint32_t colour, float a);
void gfx_scroll_up(uint32_t lines);
void gfx_draw_buffer(uint32_t *buff);
void gfx_draw_area(uint16_t pos_x, uint16_t pos_y, uint16_t width, uint16_t height);
void gfx_draw_img(uint32_t *src, uint16_t pos_x, uint16_t pos_y, uint16_t width, uint16_t height);


#endif
