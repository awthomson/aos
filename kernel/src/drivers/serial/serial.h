#include <common/stdint.h>
#include <common/stdarg.h>

unsigned char inb(unsigned int port);
int is_transmit_empty();
void outb(uint16_t port, uint8_t val);

void com1_putchar(char a);
void com1_printf(const char *fmt, ...);
void com1_kprint_str(char *c);
