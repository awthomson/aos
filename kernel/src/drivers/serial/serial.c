#include <common/common.h>
#include <common/stdarg.h>
#include "serial.h"

int is_transmit_empty() {
   return inb(0x3F8 + 5) & 0x20;
}

void com1_putchar(char a) {
   while (is_transmit_empty() == 0);
   outb(0x3F8,a);
}

void com1_printf (const char *fmt, ...)
{
    const char *p;
    int i;
    char *s;
    va_list argp;
    char buff[20];

    va_start(argp, fmt);

    for(p = fmt; *p != '\0'; p++) {
        if(*p != '%') {
			if (*p == '\n') {
            	com1_putchar('\r');
            	com1_putchar('\n');
			} else {
            	com1_putchar(*p);
			}
            continue;
        }

        switch(*++p) {
        case 'c':
            i = va_arg(argp, int);
            com1_putchar(i);
            break;
        case 'd':
            i = va_arg(argp, unsigned int);
            itoa_32(buff, 'd', i);
            com1_kprint_str(buff);
            break;
        case 's':
            s = va_arg(argp, char *);
            com1_kprint_str(s);
            break;
        case 'x':
            i = va_arg(argp, unsigned int);
            itoa_32(buff, 'x', i);
            com1_kprint_str(buff);
            break;
        case 'g':
            i = va_arg(argp, unsigned long);
            itoa_64(buff, 'x', i);
            com1_kprint_str(buff);
            break;
        case '%':
            com1_putchar('%');
            break;
        }
    }

    va_end(argp);
}

void com1_kprint_str(char *c)
{
    int x=0;
    char byte = c[x];
    while (byte != 0) {
		if (byte == '\n') {
			com1_putchar('\r');
			com1_putchar('\n');
		} else {
        	com1_putchar(byte);
		}
        byte = c[++x];
    }

}
