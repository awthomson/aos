#include <common/common.h>
#include <common/logging.h>
//#include <drivers/serial/serial.h>
#include <sys/pit.h>
//#include <sys/memory.h>
#include "ide.h"

uint8_t ide_buf[2048] = {0};
// unsigned static char ide_irq_invoked = 0;
// unsigned static char atapi_packet[12] = {0xA8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

struct IDEChannelRegisters {
	uint16_t base;			// I/O Base.
	uint16_t ctrl;  		// Control Base
	uint16_t bmide; 		// Bus Master IDE
	uint8_t  nIEN;  		// nIEN (No Interrupt);
} channels[2];

struct ide_device {
	uint8_t  reserved;		// 0 (Empty) or 1 (This Drive really exists).
	uint8_t  channel;		// 0 (Primary Channel) or 1 (Secondary Channel).
	uint8_t  drive;			// 0 (Master Drive) or 1 (Slave Drive).
	uint16_t type;        	// 0: ATA, 1:ATAPI.
	uint16_t signature;   	// Drive Signature
	uint16_t capabilities;	// Features.
	uint32_t commandSets; 	// Command Sets Supported.
	uint32_t size;        	// Size in Sectors.
	uint8_t  model[41];   	// Model in string.
} ide_devices[4];

/*
	Read a sector into memory
	TODO: Probably don't hard-code 0XE0
*/
void ide_read_sectors(uint8_t *dest, uint32_t lba, uint32_t num)
{

	int i;
	for (i=0; i<num; i++) {

		// Select drive
		outb(CHAN_P_BASE + ATA_REG_HDDEVSEL, 0xE0);

	    outb(CHAN_P_BASE + ATA_REG_SECCOUNT0, 1);
	    outb(CHAN_P_BASE + ATA_REG_LBA0, ((lba+i) & 0x000000ff) >>  0);
	    outb(CHAN_P_BASE + ATA_REG_LBA1, ((lba+i) & 0x0000ff00) >>  8);
	    outb(CHAN_P_BASE + ATA_REG_LBA2, ((lba+i) & 0x00ff0000) >> 16);
	    outb(CHAN_P_BASE + ATA_REG_COMMAND, ATA_CMD_READ_PIO);

		// Check for errors
		int x = ide_polling(0, 1);
		if (x != 0) {
			klog(LL_DEBUG, "P: %x\n", x);
			x = inb(channels[0].base + ATA_REG_ERROR);
			klog(LL_DEBUG, "E: %x\n", x);
			ide_print_error(0, x);
		}

   		asm volatile (
			"mov rdi, rbx;"
			"mov rdx, 0x1f0;"
			"mov rcx, 256;"
			"rep insw;"
	        ::"b"(dest):
	    );
		dest += 512;
	}

}

uint8_t ide_read(uint8_t channel, uint8_t reg)
{
	uint8_t result;
	if (reg > 0x07 && reg < 0x0C)
		ide_write(channel, ATA_REG_CONTROL, 0x80 | channels[channel].nIEN);
   if (reg < 0x08)
      result = inb(channels[channel].base + reg - 0x00);
   else if (reg < 0x0C)
      result = inb(channels[channel].base  + reg - 0x06);
   else if (reg < 0x0E)
      result = inb(channels[channel].ctrl  + reg - 0x0A);
   else if (reg < 0x16)
      result = inb(channels[channel].bmide + reg - 0x0E);
   if (reg > 0x07 && reg < 0x0C)
      ide_write(channel, ATA_REG_CONTROL, channels[channel].nIEN);
   return result;
}

void ide_write(uint8_t channel, uint8_t reg, uint8_t data) {
   if (reg > 0x07 && reg < 0x0C)
      ide_write(channel, ATA_REG_CONTROL, 0x80 | channels[channel].nIEN);
   if (reg < 0x08)
      outb(channels[channel].base  + reg - 0x00, data);
   else if (reg < 0x0C)
      outb(channels[channel].base  + reg - 0x06, data);
   else if (reg < 0x0E)
      outb(channels[channel].ctrl  + reg - 0x0A, data);
   else if (reg < 0x16)
      outb(channels[channel].bmide + reg - 0x0E, data);
   if (reg > 0x07 && reg < 0x0C)
      ide_write(channel, ATA_REG_CONTROL, channels[channel].nIEN);
}

/*
	Read information into buffer
*/
void ide_read_buffer(uint8_t channel, uint8_t reg, uint8_t *buffer)
{
    // Read all the IDE 0 info
    // uint16_t ide0_info[quads*2];
    for (int i=0; i<2048/2; i++) {
        uint16_t x = inw(channels[channel].base + reg);
		buffer[i*2] = x & 0x00ff;
		buffer[i*2+1] = (x>>8) & 0x00ff;
    }
}

uint8_t ide_polling(uint8_t channel, uint32_t advanced_check)
{
 
	// Delay 400 nanosecond for BSY to be set
	for(int i = 0; i < 4; i++)
		// Wastes 100ns
		ide_read(channel, ATA_REG_ALTSTATUS);
 
	// Wait for BSY to be cleared
	while (ide_read(channel, ATA_REG_STATUS) & ATA_SR_BSY);
 
	if (advanced_check) {
		uint8_t state = ide_read(channel, ATA_REG_STATUS); 	// Read Status Register.
 
		if (state & ATA_SR_ERR) {
			klog(LL_ERROR, "Drive error\n");
			return 2; 		// Error.
		}
 
		if (state & ATA_SR_DF) {
			klog(LL_ERROR, "Drive fault\n");
			return 1; 		// Device Fault.
		}
 
		// Check DRQ
		// BSY = 0; DF = 0; ERR = 0 so we should check for DRQ now.
		if ((state & ATA_SR_DRQ) == 0) {
			klog(LL_ERROR, "DRQ not set\n");
			return 3; 		// DRQ not set
		}
 
   }
 
   return 0; 
 
}

uint8_t ide_print_error(uint32_t drive, uint8_t err)
{
   if (err == 0)
      return err;
 
   klog(LL_DEBUG, "IDE:\n");
   if (err == 1) {klog(LL_DEBUG, "- Device Fault\n     "); err = 19;}
   else if (err == 2) {
      uint8_t st = ide_read(ide_devices[drive].channel, ATA_REG_ERROR);
      if (st & ATA_ER_AMNF)		{klog(LL_DEBUG, "- No Address Mark Found\n     ");   err = 7;}
      if (st & ATA_ER_TK0NF)   	{klog(LL_DEBUG, "- No Media or Media Error\n     ");   err = 3;}
      if (st & ATA_ER_ABRT)   	{klog(LL_DEBUG, "- Command Aborted\n     ");      err = 20;}
      if (st & ATA_ER_MCR)   	{klog(LL_DEBUG, "- No Media or Media Error\n     ");   err = 3;}
      if (st & ATA_ER_IDNF)   	{klog(LL_DEBUG, "- ID mark not Found\n     ");      err = 21;}
      if (st & ATA_ER_MC)   	{klog(LL_DEBUG, "- No Media or Media Error\n     ");   err = 3;}
      if (st & ATA_ER_UNC)   	{klog(LL_DEBUG, "- Uncorrectable Data Error\n     ");   err = 22;}
      if (st & ATA_ER_BBK)   	{klog(LL_DEBUG, "- Bad Sectors\n     ");       err = 13;}
   } else  if (err == 3)		{klog(LL_DEBUG, "- Reads Nothing\n     "); err = 23;}
     else  if (err == 4)		{klog(LL_DEBUG, "- Write Protected\n     "); err = 8;}
   klog(LL_DEBUG, "- [%s %s] %s\n",
      (const char *[]){"Primary", "Secondary"}[ide_devices[drive].channel], // Use the channel as an index into the array
      (const char *[]){"Master", "Slave"}[ide_devices[drive].drive], // Same as above, using the drive
      ide_devices[drive].model);
 
   return err;
}

void init_ide(void)
{
	klog(LL_INFO, "Initialising IDE\n");
	init_ide2(0x1F0, 0x3F6, 0x170, 0x376, 0x000);
}

void init_ide2(uint32_t BAR0, uint32_t BAR1, uint32_t BAR2, uint32_t BAR3, uint32_t BAR4)
{
	// ide_initialize(0x1F0, 0x3F6, 0x170, 0x376, 0x000);
	/*
	uint32_t BAR0=0x1f0;
	uint32_t BAR1=0x3f6;
	uint32_t BAR2=0x170;
	uint32_t BAR3=0x376;
	uint32_t BAR4=0x000;
	*/

	int count = 0;
 
	// Detect I/O Ports which interface IDE Controller:
	channels[ATA_PRIMARY  ].base  = (BAR0 & 0xFFFFFFFC) + 0x1F0 * (!BAR0);
	channels[ATA_PRIMARY  ].ctrl  = (BAR1 & 0xFFFFFFFC) + 0x3F6 * (!BAR1);
	channels[ATA_PRIMARY  ].bmide = (BAR4 & 0xFFFFFFFC) + 0; // Bus Master IDE
	channels[ATA_SECONDARY].base  = (BAR2 & 0xFFFFFFFC) + 0x170 * (!BAR2);
	channels[ATA_SECONDARY].ctrl  = (BAR3 & 0xFFFFFFFC) + 0x376 * (!BAR3);
	channels[ATA_SECONDARY].bmide = (BAR4 & 0xFFFFFFFC) + 8; // Bus Master IDE

	// Disable IRQs:
	ide_write(ATA_PRIMARY  , ATA_REG_CONTROL, 2);
	ide_write(ATA_SECONDARY, ATA_REG_CONTROL, 2);

   // Detect ATA-ATAPI Devices:
   for (int i = 0; i < 2; i++)
      for (int j = 0; j < 2; j++) {
 
         uint8_t err = 0, type = IDE_ATA, status;
         ide_devices[count].reserved = 0; // Assuming that no drive here.
 
         // Select Drive:
         ide_write(i, ATA_REG_HDDEVSEL, 0xA0 | (j << 4));
		
         pit_wait(1);
 
         // Send ATA Identify Command:
         ide_write(i, ATA_REG_COMMAND, ATA_CMD_IDENTIFY);
         pit_wait(1); 
 
         // Polling:
         if (ide_read(i, ATA_REG_STATUS) == 0) continue; // If Status = 0, No Device.
 
         while(1) {
            status = ide_read(i, ATA_REG_STATUS);
            if ((status & ATA_SR_ERR)) {err = 1; break;} // If Err, Device is not ATA.
            if (!(status & ATA_SR_BSY) && (status & ATA_SR_DRQ)) break; // Everything is right.
         }
 
         // (IV) Probe for ATAPI Devices:
 
         if (err != 0) {
            uint8_t cl = ide_read(i, ATA_REG_LBA1);
            uint8_t ch = ide_read(i, ATA_REG_LBA2);
 
            if (cl == 0x14 && ch ==0xEB)
               type = IDE_ATAPI;
            else if (cl == 0x69 && ch == 0x96)
               type = IDE_ATAPI;
            else
               continue; // Unknown Type (may not be a device).
 
            ide_write(i, ATA_REG_COMMAND, ATA_CMD_IDENTIFY_PACKET);
            pit_wait(1);
         }
 
         // (V) Read Identification Space of the Device:
         ide_read_buffer(i, ATA_REG_DATA, (uint8_t *)ide_buf);
 
         // (VI) Read Device Parameters:
         ide_devices[count].reserved     = 1;
         ide_devices[count].type         = type;
         ide_devices[count].channel      = i;
         ide_devices[count].drive        = j;
         ide_devices[count].signature    = *((uint16_t *)(ide_buf + ATA_IDENT_DEVICETYPE));
         ide_devices[count].capabilities = *((uint16_t *)(ide_buf + ATA_IDENT_CAPABILITIES));
         ide_devices[count].commandSets  = *((uint32_t *)(ide_buf + ATA_IDENT_COMMANDSETS));
 
         // (VII) Get Size:
         if (ide_devices[count].commandSets & (1 << 26))
            // Device uses 48-Bit Addressing:
            ide_devices[count].size   = *((uint32_t *)(ide_buf + ATA_IDENT_MAX_LBA_EXT));
         else
            // Device uses CHS or 28-bit Addressing:
            ide_devices[count].size   = *((uint32_t *)(ide_buf + ATA_IDENT_MAX_LBA));
 
         // (VIII) String indicates model of device (like Western Digital HDD and SONY DVD-RW...):
         for(int k = 0; k < 40; k += 2) {
            ide_devices[count].model[k] = ide_buf[ATA_IDENT_MODEL + k + 1];
            ide_devices[count].model[k + 1] = ide_buf[ATA_IDENT_MODEL + k];
			}
         ide_devices[count].model[40] = 0; // Terminate String.       
 
         count++;
      }
 
	// Print Summary:
	for (int i = 0; i < 4; i++) {
		if (ide_devices[i].reserved == 1) {
			klog(LL_DEBUG, "Found %s Drive %d KB - %s\n", (const char *[]){"ATA", "ATAPI"}[ide_devices[i].type], ide_devices[i].size / 2);
			// klog(LL_DEBUG, "Channel: %d   Drive: %d\n", ide_devices[i].channel, ide_devices[i].drive);
			// klog(LL_DEBUG, "Signature: %d\n", ide_devices[i].signature);
			klog(LL_DEBUG, "Model: %s\n", ide_devices[i].model);
			// klog(LL_DEBUG, "Info: %x\n", &ide_buf);
		}
	}
		
}

