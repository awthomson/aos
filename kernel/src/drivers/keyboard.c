#include <common/stdint.h>
#include <common/common.h>
#include <common/logging.h>

#include "keyboard.h"

uint8_t scan_codes[] = {
	'?', 'E', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', KEY_BS, 'T',		// E = ESC, B = Backspace, T = Tab
	'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', 10, KEY_LCTRL, 				// 10 = Enter, L = Left. Ctrl
	'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '`', KEY_LSHIFT, '\\',
	'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/',
	'R', 'b', 'L', ' ', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',		// R = Right Shift, L = Left. Alt
	'?', '?', 'u', '?', '?', 'l', '?', 'r', '?', '?', 'd', '?', '?', '?', '?', '?',
	'?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',
	'?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?'
};

uint8_t shift_scan_codes[] = {
	'?', 'E', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', 'B', 'T',		// E = ESC, B = Backspace, T = Tab
	'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', 10, 'L', 				// 10 = Enter, L = Left. Ctrl
	'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ';', '\'', '`', KEY_LSHIFT, '\\',				// L = L. Shift
	'Z', 'X', 'C', 'V', 'B', 'N', 'M', ',', '.', '/'
};

static uint8_t key_buffer[256];
static uint8_t key_buffer_pos = 0;


uint8_t read_keypress(void)
{
	static int is_shift_down = 0;
	uint8_t sc = inb(0x60);
	// klog(LL_INFO, "Raw scancode: %x\n", sc);

	if (sc == 0x2a)
		is_shift_down = 1;
	if (sc == 0xaa)
		is_shift_down = 0;

	// If a button is released we're not particularly interested
	if (sc & 128)
		return 0;

	uint8_t parsed = scan_codes[sc];
	if (is_shift_down == 1)
		parsed = shift_scan_codes[sc];

	key_buffer[key_buffer_pos++] = parsed;
	if (key_buffer_pos > 255) {
		klog(LL_ERROR, "Keyboard buffer full!\n", parsed);
		return 0;
	}
	// klog(LL_INFO, "Interpreted: %c\n", parsed);
	return parsed;
}

uint8_t read_key_buffer(void)
{
	// Going to return this one
	uint8_t tmp = key_buffer[0];

	// Move the rest of the buffer down
	for (int i=0; i<255; i++)
		key_buffer[i] = key_buffer[i+1];
	key_buffer[255] = '\0';

	if (key_buffer_pos>0)
		key_buffer_pos--;

	return tmp;

}