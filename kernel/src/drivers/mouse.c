#include <common/stdint.h>
#include <common/common.h>
#include <common/logging.h>

#include "mouse.h"

static char mouse_byte[2];
static uint8_t mouse_cycle;
static char mouse_x;
static char mouse_y;
static uint16_t mouse_pos_x;
static uint16_t mouse_pos_y;
static uint8_t  mouse_buttons;

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 800

uint16_t get_mouse_x()
{
	return mouse_pos_x;
}

uint16_t get_mouse_y()
{
	return mouse_pos_y;
}

uint8_t get_mouse_buttons(void)
{
	return mouse_buttons;
}

void mouse_wait(uint8_t a_type) //unsigned char
{
  uint32_t _time_out=100000; //unsigned int
  if (a_type==0) {
    while(_time_out--) //Data
      if((inb(0x64) & 1)==1)
        return;
    return;
  } else {
    while(_time_out--) //Signal
      if((inb(0x64) & 2)==0)
        return;
    return;
  }
}

void mouse_write_port(uint8_t a_write) //unsigned char
{
  //Wait to be able to send a command
  mouse_wait(1);
  //Tell the mouse we are sending a command
  outb(0x64, 0xD4);
  //Wait for the final part
  mouse_wait(1);
  //Finally write
  outb(0x60, a_write);
}

uint8_t mouse_read_port()
{
	//Get's response from mouse
	mouse_wait(0);
	return inb(0x60);
}

void init_mouse(void)
{
	uint8_t _status;  //unsigned char

	//Enable the auxiliary mouse device
	mouse_wait(1);
	outb(0x64, 0xA8);

	//Enable the interrupts
	mouse_wait(1);
	outb(0x64, 0x20);
	mouse_wait(0);
	_status=(inb(0x60) | 2);
	mouse_wait(1);
	outb(0x64, 0x60);
	mouse_wait(1);
	outb(0x60, _status);

	//Tell the mouse to use default settings
	mouse_write_port(0xF6);
	mouse_read_port();  //Acknowledge

	//Enable the mouse
	mouse_write_port(0xF4);
	mouse_read_port();  //Acknowledge

	mouse_pos_x = 600;
	mouse_pos_y = 400;
}

uint64_t read_mouse(void)
{

  switch(mouse_cycle)
  {
    case 0:
      mouse_byte[0]=inb(0x60);
      mouse_cycle++;
      break;
    case 1:
      mouse_byte[1]=inb(0x60);
      mouse_cycle++;
      break;
    case 2:
      mouse_byte[2] = inb(0x60);
      mouse_x = mouse_byte[1];
      mouse_y = mouse_byte[2];
	  mouse_buttons = mouse_byte[0] & 0b1;
      mouse_cycle=0;
	  if ((mouse_y>0) & (mouse_pos_y < mouse_y)) {
		mouse_pos_y = 0;
		mouse_y = 0;
		}
	  if ((mouse_x<0) & (mouse_pos_x < -mouse_x)) {
		mouse_pos_x = 0;
		mouse_x = 0;
		}
	  if ((mouse_y<0) & (mouse_pos_y > (SCREEN_HEIGHT+mouse_y)-20)) {
		mouse_pos_y = SCREEN_HEIGHT-20;
		mouse_y = 0;
		}
	  if ((mouse_x>0) & (mouse_pos_x > SCREEN_WIDTH-mouse_x)) {
		mouse_pos_x = SCREEN_WIDTH;
		mouse_x = 0;
		}
	  mouse_pos_x += mouse_x;
	  mouse_pos_y -= mouse_y;
      break;
  }
  return 0;

}

