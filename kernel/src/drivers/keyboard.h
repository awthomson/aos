#ifndef __keyboard_h__
#define __keyboard_h__

#define KEY_LSHIFT 0x02
#define KEY_BS     0x03
#define KEY_LCTRL  0x04

uint8_t read_keypress(void);
uint8_t read_key_buffer(void);

#endif
