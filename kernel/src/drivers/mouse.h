#ifndef __mouse_h__
#define __mouse_h__

uint64_t read_mouse(void);
void init_mouse();
uint8_t mouse_read_port();
void mouse_write_port(uint8_t a_write);
void mouse_wait(uint8_t a_type);
uint16_t get_mouse_x();
uint16_t get_mouse_y();
uint8_t get_mouse_buttons(void);

#endif
