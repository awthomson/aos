#include <common/logging.h>
#include <common/common.h>
#include <drivers/ide/ide.h>
#include <sys/memory.h>
#include "ext2.h"



struct superblock {
  uint32_t s_inodes_count;
  uint32_t s_blocks_count;
  uint32_t s_r_blocks_count;
  uint32_t s_free_blocks_count;
  uint32_t s_free_inodes_count;
  uint32_t s_first_data_block;
  uint32_t s_log_block_size;
  uint32_t s_log_frag_size;
  uint32_t s_blocks_per_group;
  uint32_t s_frags_per_group;
  uint32_t s_inodes_per_group;
  uint32_t s_mtime;
  uint32_t s_wtime;
  uint16_t s_mnt_count;
  uint16_t s_max_mnt_count;
  uint16_t s_magic;
  uint16_t s_state;
  uint16_t s_errors;
  uint16_t s_minor_rev_level;
  uint32_t s_lastcheck;
  uint32_t s_checkinterval;
  uint32_t s_creator_os;
  uint32_t s_rev_level;
  uint16_t s_def_resuid;
  uint16_t s_def_resgid;

  uint32_t s_first_ino;
  uint16_t s_inode_size;
  uint16_t s_block_group_nr;
  uint32_t s_feature_compat;
  uint32_t s_feature_incompat;
  uint32_t s_feature_ro_compat;
  uint8_t  s_uuid[16];
  uint8_t s_volume_name[16];
  uint8_t s_last_mounted[64];
  uint32_t s_algo_bitmap;

}__attribute__((packed));

struct bgdt {
  uint32_t bg_block_bitmap;
  uint32_t bg_inode_bitmap;
  uint32_t bg_inode_table;
  uint16_t bg_free_blocks_count;
  uint16_t bg_free_inodes_count;
  uint16_t bg_used_dirs_count;
  uint16_t bg_pad;
  uint8_t  bg_reserved[12];
}__attribute__((packed));

struct dir {
  uint32_t inode;
  uint16_t rec_len;
  uint8_t name_len;
  uint8_t file_type;
  uint8_t name[256];
}__attribute__((packed));


void init_ext2(void)
{
	klog(LL_INFO, "Initializing EXT2\n");	

  klog(LL_INFO, "Allocating memory for BGDT\n");
	bgdt_data = kmalloc(4096, kernel_pml4);	
	// int i=0;
	// for (i=0; i<4096; i++)
	// 	bgdt_data[i] = 'a';

  klog(LL_INFO, "Allocating memory for inode table\n");
	inode_table_data = kmalloc(4096, kernel_pml4);	
	// for (i=0; i<4096; i++)
	// 	inode_table_data[i] = 'b';

	dir_data = kmalloc(4096, kernel_pml4);	
	// for (i=0; i<4096; i++)
	// 	dir_data[i] = 'c';

  // TODO: Don't assume everything is in the first blockgroup
	read_block(bgdt_data, 1);
	struct bgdt *bg = (struct bgdt*)bgdt_data;
	klog(LL_DEBUG, "inode table: %d\n", bg->bg_inode_table);

	read_block(inode_table_data, bg->bg_inode_table);

}

/*
	Read in directory information - specified by inode
*/
void *read_dir(uint32_t inode)
{
	klog(LL_INFO, "Reading directory with inode: %d\n", inode);
	struct inode_table *it = (struct inode_table *)(inode_table_data+(128*(inode-1)));
	klog(LL_DEBUG,"inodetable: %x\n", (uint64_t)it);					
	klog(LL_DEBUG,"Blocks: %d\n", it->i_blocks/8);					// Actually number of 512 byte sectors not blocks
	klog(LL_DEBUG,"Dir block 0: %d\n", it->i_block[0]);
	klog(LL_DEBUG,"Dir block 1: %d\n", it->i_block[1]);
	read_block(dir_data, it->i_block[0]);
  	klog(LL_DEBUG, "read_dir() - dir_data: %x\n", dir_data);
	return dir_data;
	
}

int read_file(void *ptr, uint32_t inode, uint64_t pos, uint64_t size)
{
  struct inode_table *it = (struct inode_table *)(inode_table_data+(128*(inode-1)));
  uint32_t num_blocks = it->i_blocks/8;								// i_blocks is actually # of 512 byte blocks
  klog(LL_INFO, "ext2.c: read_file() - Number of blocks to read: %d\n", num_blocks);
  uint8_t *tmp = malloc(4096);

  /*
    File is smaller than 11*4k blocks ~45k so we dont need any indirect addressing
  */
  if (num_blocks<12) {
    for (int i=0; i<num_blocks; i++) {
      uint32_t block0 = it->i_block[i];
      read_block(tmp, block0);
      memcpy(tmp, ptr, 4096);
      ptr += 4096;	
    }
    return 0;
  }

  /*
    File is big enough to need indirect addressing.  This uses entry 12 to pooint to another
    list of block addresses
    */
  // uint32_t blocklist[1000];
  uint32_t *blocklist = malloc(4096);
  for (int i=0; i<12; i++) {
      read_block(tmp, it->i_block[i]);
      memcpy(tmp, ptr, 4096);
      ptr += 4096;	
  }

  read_block(tmp, it->i_block[12]);
  memcpy(tmp, blocklist, 4096);

  for (int i=0; i<num_blocks-13; i++) {
      read_block(tmp, blocklist[i]);
      memcpy(tmp, ptr, 4096);
      ptr += 4096;
  }

  klog(LL_DEBUG, "Finished reading medium sized file\n", 0);
  return 0;

}

void read_block(uint8_t *buff, uint32_t block_num)
{
	// 8 (512 byte) sectors to a block (4096)
	ide_read_sectors(buff, block_num*8, 8);
}

/*
	Given a string represnting the filename/dir, find the associated inode
*/
int get_inode(const char *str, uint32_t cwd)
{

  if (strlen(str)==0) {
    klog(LL_WARN, "get_inode() - filename not specificed\n");
    return ENOENT;
  }

  klog(LL_INFO, "ext2.c - get_inode(): Looking for inode given name \"%s\"\n", str);

  char *name = malloc(256*sizeof(char));
  char *token = malloc(256*sizeof(char));
  char *next_token = malloc(256*sizeof(char));

  uint32_t curr_inode = 0;

  token = strtok((char*)str, '/');
  next_token = strtok(0, '/');
  // Find where we start based on whether string starts with a /
  if (strcmp(token, "")==0) {
    strcpy(token, next_token);
    next_token = strtok(0, '/');
    cwd = 2;
  } else {
    klog(LL_DEBUG, "Starting in cwd (%d)\n", cwd);
  }

  struct inode_table *it = (struct inode_table *)(inode_table_data+(128*(cwd-1))); 
  read_block(dir_data, it->i_block[0]);
  struct dir *d = (struct dir *)dir_data;
  int offset = 0;

  while (*token != '\0') {

    klog(LL_DEBUG, "Token:      %s\n", token);
    klog(LL_DEBUG, "Next Token: %s\n", next_token);

    // Scan through the directory entries looking for a match on 'token'
    while (offset < 4096) {
      strncpy(name, (const char *)d->name, d->name_len);
	    // klog(LL_DEBUG, "Checking %s for a match\n", name);
      
      if (strcmp(token, (const char *)name)==0)  {
		    // Match on current entry and there's more in the path
        klog(LL_INFO, "Match on this: %s\n", name);
        curr_inode = d->inode;
        klog(LL_DEBUG, "Loading inode %d\n", d->inode);
        klog(LL_DEBUG, "inode type: %d\n", d->file_type);

        // If this is the last thing in the list, then return the inode
        if (next_token == 0)
          return d->inode;

        if (d->file_type == 2) {
          // Token is a directory, so follow link to the next one
          it = (struct inode_table *)(inode_table_data+(128*(curr_inode-1)));
          klog(LL_DEBUG, "Block: %d\n", it->i_block[0]);
          read_block(dir_data, it->i_block[0]);
          d = (struct dir *)dir_data;
          offset = 0;
        } else if (d->file_type != 2) {
          klog(LL_WARN, "Something wonky here - not a directory entry\n");
        }

        break;
      }

      // Go to the next directory entry
      offset += d->rec_len;
      if (offset < 4096)
        d = (void *)d+d->rec_len;
      else {
        klog(LL_DEBUG, "No match found in this dir - returning ENOENT\n");
        return ENOENT;
      }
      
    }

    strcpy(token, next_token);
    next_token = strtok(0, '/');

  }

  klog(LL_INFO, "Done scanning.  Returning %d...\n", curr_inode);
	return curr_inode;


}

struct inode_table *get_inode_info(uint32_t inode) {
  struct inode_table *it = (struct inode_table *)(inode_table_data+(128*(inode-1))); 
  read_block(dir_data, it->i_block[0]); 
  return it;
}
