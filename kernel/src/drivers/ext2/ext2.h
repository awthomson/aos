#ifndef __EXT2_H__
#define __EXT2_H__

#define ENOERR  0
#define ENOENT  -1
#define ENOTDIR -2

#include <common/stdint.h>

struct inode_table {
  uint16_t i_mode;
  uint16_t i_uid;
  uint32_t i_size;
  uint32_t i_atime;
  uint32_t i_ctime;
  uint32_t i_mtime;
  uint32_t i_dtime;
  uint16_t i_gid;
  uint16_t i_links_count;
  uint32_t i_blocks;
  uint32_t i_flags;
  uint32_t i_osd1;
  uint32_t i_block[15];
  uint32_t i_generation;
  uint32_t i_file_acl;
  uint32_t i_dir_acl;
  uint32_t i_faddr;
  uint8_t  i_osd2[12];
}__attribute__((packed));

struct FILE {
    uint64_t fpos;
    uint32_t inode;
    uint8_t mode;
    uint64_t fsize;
};

uint8_t *bgdt_data;
uint8_t *inode_table_data;
uint8_t *dir_data;

void debug_ext2(void);
void init_ext2(void);
void read_block(uint8_t *buff, uint32_t block_num);
void *read_dir(uint32_t inode);
void getcwd(uint32_t inode);
int get_inode(const char *x, uint32_t cwd);
struct inode_table *get_inode_info(uint32_t inode);
int read_file(void *ptr, uint32_t inode, uint64_t pos, uint64_t size);  

#endif
