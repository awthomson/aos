#include <common/logging.h>

#include <drivers/graphics/graphics.h>
#include <drivers/ide/ide.h>
#include <drivers/ext2/ext2.h>
#include <drivers/keyboard.h>
#include <drivers/mouse.h>

#include <sys/memory.h>
#include <sys/pit.h>
#include <sys/interrupts.h>
#include <sys/gdt.h>
#include <sys/acpi.h>
#include <sys/lapic.h>
#include <sys/hpet.h>
#include <sys/ioapic.h>
#include <sys/syscall.h>
#include <sys/scheduler.h>
#include <sys/ipc.h>

/******************************************
 * Entry point, called by BOOTBOOT Loader *
 ******************************************/
void _start()
{

	klog(LL_INFO, "**********************************************************\n");
	klog(LL_INFO, "AOS \n");
	klog(LL_INFO, "**********************************************************\n");

	init_gdt();
	init_idt();
	init_mem();
	init_syscall();
	init_ide();
	init_mouse();	
	init_ipc();
	init_ext2();
	init_ioapic();
	init_lapic();
	init_acpi();
	// setup_pic();
	init_hpet();

	// Doesn't get past this - this starts the usermode processes
	init_scheduler();

	while(1);
	

// 	Not sure if these are actually needed now.  Maybe for keybaord interrupts?

}

