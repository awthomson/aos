#!/bin/bash

rm ../aos.iso
mkdir sys 2> /dev/null
cp ../kernel/src/aos-kernel.x86_64.elf sys/core
./mkbootimg mkbootimg.json ../aos.iso
