#ifndef __WINDOWS_H__
#define __WINDOWS_H__

#include <alibc.h>

#define LABEL       0x01
#define BUTTON      0x02
#define CANVAS		0x03

// UI component definitions - these must match those in ui.h
// TODO - maybe fix that so they're in one place

typedef struct component UIComponent;
typedef struct window UIWindow;
typedef struct s_UICanvas UICanvas;
typedef struct s_UILabel UILabel;

struct s_UILabel {
	char text[256];
};

struct s_UICanvas {
	uint64_t width;
	uint64_t height;
	uint32_t *buffer;
};

struct component {
	uint16_t min_width;
	uint16_t min_height;
	uint16_t max_width;
	uint16_t max_height;
	uint16_t x;						// Last calculated x position within window
	uint16_t y;						// Last calculated y position within window
	uint16_t width;
	uint16_t height;
	uint8_t type;					// Type of component (Label, Button, Canvas, etc.)
	uint8_t layout;					// Layout of child components - vertical 90) or horizontal (1)
	void *element;					// Pointer to specific element structure (button details, label details, etc.)
	struct window *owner;
	struct component *parent;
	struct component *children; 
	struct component *next;
};

struct window {
	int pos_x;
	int pos_y;
	int width;
	int height;
	uint16_t z;
	uint8_t  has_focus;
	uint64_t wid;
	uint64_t pid;
	uint8_t needs_redraw;
	char title[256];
	struct component *first_component;
	struct window *next;
};

UIWindow *first_window;

void draw_windows(void);
uint64_t num_windows(void);
void draw_window(UIWindow *win);
void normalize_z(void);
struct window *get_focused_window(void);
int num_components(struct component *first_component);
void draw_components(UIComponent *comp);
void calc_sizes(UIWindow *win);
void focus_window(UIWindow *win);
int overlaps(UIWindow *w1, UIWindow *w2);

#endif
