#include <alibc.h>
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <stdlib.h>
#include <time.h>

#include "compost.h"
#include "windows.h"
#include "mouse.h"
#include "buttons.h"
#include "primatives.h"
#include "libbmp.h"

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 900

#define COL_BACKGROUND 0x404060;

struct dirty_region {
	uint16_t x;
	uint16_t y;
	uint16_t w;
	uint16_t h;
};

uint16_t num_regions;
struct dirty_region region[100];

uint64_t id_counter;

uint32_t *gfx_buff;

void *background_image;

void load_bg(void)
{
	background_image = malloc(sizeof(uint32_t)*1280*901);
	load_bmp("background.bmp", background_image);
}


void _start(char **args, int argc)
{

	first_window   = 0;
	num_regions    = 0;
	id_counter     = 1;

	// Allocate some area which will be our graphics working space
	// TODO: Not sure we need the x4 but it seems to fix page fault issues when draggikng windows around
	gfx_buff = malloc(SCREEN_WIDTH*SCREEN_HEIGHT*sizeof(uint32_t)*2);
	syscall(SYSCALL_SET_WFB, (uint64_t)gfx_buff, 0, 0, 0);

	srand(get_uptime_ns());

	load_bg();
	draw_bg();
	draw_dirty_regions();

	init_mouse();
	show_mouse();
\
	// Load and execute an example GUI program
	uint64_t res;
	char *fname = malloc(50);
	strncpy(fname, "guitest", 7);
	syscall(SYSCALL_EXEC, (uint64_t)fname, (uint64_t)0, (uint64_t)&res, 0);
	// strncpy(fname, "bouncy", 6);
	// syscall(SYSCALL_EXEC, (uint64_t)fname, (uint64_t)0, (uint64_t)&res, 0);
	free(fname);

	// Main render loop
	while(1) {	
		update_mouse();
		check_events();
		draw_dirty_regions();

		wait(1);

	}
	kill_me();
}

void add_dirty_region(uint16_t x, uint16_t y, uint16_t w, uint16_t h)
{

	if (num_regions == 100) {
		klog(LL_WARN, "Maximum redraw regions reached - expect graphics corruption\n", 0);
		return;
	}

	if (x < 0)
		x = 0;
	if (y < 0)
		y = 0;
	if (x+w > SCREEN_WIDTH)
		w = SCREEN_WIDTH-x;
	if (y+h > SCREEN_HEIGHT)
		h = SCREEN_HEIGHT-y;

	int exists = 0;
	if (num_regions > 0) {
		for (int i = 0; i<num_regions; i++) {
			if ((region[i].x == x) && (region[i].y == y) && (region[i].w == w) && (region[i].h == h)) {
				exists = 1;
			} 
		}
	}

	if (exists == 0) {
		region[num_regions].x = x;
		region[num_regions].y = y;
		region[num_regions].w = w;
		region[num_regions].h = h;
		num_regions++;
	}

}


void draw_dirty_regions(void)
{
	if (num_regions == 0)
		return;

	for (int i=0; i<num_regions; i++)
		syscall(SYSCALL_DRAW_REGION, region[i].x, region[i].y, region[i].w, region[i].h);
	
	num_regions = 0;
	return;
}

/*
	Check incoming events via. IPC (new windows ,etc.)
*/
void check_events(void)
{

	// See is there's any messages on the IPC queue
	uint64_t type = 0;
	uint64_t from_pid = 0;
	uint64_t id = ipc_check(&type, &from_pid);

	// Process all events on the queue
	while (id != 0) {

		if (type == IPC_REDRAW_WINDOW) {
			// klog(LL_INFO, "compost.c: IPC_REDRAW_WINDOW\n", 0);
			UIWindow *win = (UIWindow *)ipc_get_64(id);
			draw_window(win);
		}

		if (type == IPC_NEW_WINDOW)  {
			klog(LL_INFO, "compost.c: IPC_NEW_WINDOW\n", 0);
			UIWindow *win = (UIWindow *)ipc_get_64(id);
			win->next = first_window;
			first_window = win;	
		}

		id = ipc_check(&type, &from_pid);
	}
}

void redraw_scene(void)
{
	klog(LL_DEBUG, "Redrawing whole scene\n", 0);
	draw_bg();
	draw_taskbar();
	draw_windows();
	add_dirty_region(0, 0, SCREEN_WIDTH, SCREEN_WIDTH);
}

void draw_taskbar(void)
{
	gfx_draw_filled_rect(0, SCREEN_HEIGHT-30, SCREEN_WIDTH, 30, 0x808080);
	add_dirty_region(0, SCREEN_HEIGHT-30, SCREEN_WIDTH, 30);
}


void draw_bg(void)
{
	// Set background colour
	uint32_t *src = background_image;
	for (int i=0; i<SCREEN_WIDTH*SCREEN_HEIGHT; i++) {
		gfx_buff[i] = *src++;	
	}
	add_dirty_region(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
}

