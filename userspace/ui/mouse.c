#include <syscall.h>
#include <stdlib.h>

#include "mouse.h"
#include "compost.h"
#include "windows.h"
#include "buttons.h"
#include "primatives.h"

uint8_t mouse_buttons = 0;
uint8_t mouse_buttons_old = 0;
uint16_t mouse_x = 600;
uint16_t mouse_y = 400;
uint16_t mouse_x_old = 601;
uint16_t mouse_y_old = 401;

uint32_t *pointer;

#define W 0x00ffffff
#define B 0x00000000
#define c 0xff000000

uint32_t pointer_img[] = {
	B,B,c,c,c,c,c,c,c,c,c,c,
	B,W,B,B,c,c,c,c,c,c,c,c,
	B,W,W,W,B,B,c,c,c,c,c,c,
	B,W,W,W,W,W,B,B,c,c,c,c,
	B,W,W,W,W,W,W,W,B,B,c,c,
	B,W,W,W,W,W,W,W,W,W,B,B,
	B,W,W,W,W,W,W,W,W,W,B,c,
	B,W,W,W,W,W,W,W,B,B,c,c,
	B,W,W,W,W,W,W,B,c,c,c,c,
	B,W,W,W,B,W,W,W,B,c,c,c,
	B,W,B,B,c,B,W,W,W,B,c,c,
	B,B,c,c,c,c,B,W,W,W,B,c,
	c,c,c,c,c,c,c,B,W,B,c,c,
	c,c,c,c,c,c,c,c,B,c,c,c,
	c,c,c,c,c,c,c,c,c,c,c,c

};

uint32_t pnt_backup[9*20];

uint64_t *mouse_data;

int grabbed;				// Is the user dragging a window around?

void update_mouse()
{
	syscall(SYSCALL_READ_MOUSE, (uint64_t)mouse_data, 0, 0, 0);
	mouse_x = ((*mouse_data)>>16) & 0b1111111111111111;
	mouse_y = (*mouse_data) & 0b1111111111111111;
	mouse_buttons = ((*mouse_data)>>32) & 0b11111111;

	int mouse_moved = 1;
	if ((mouse_x == mouse_x_old) && (mouse_y == mouse_y_old))
		mouse_moved = 0;
	
	UIWindow *f = get_focused_window();

	if (mouse_moved == 1) {
		hide_mouse();
	}

	// CHeck if user was dragging a window and then released button
	if (((mouse_buttons & 0b1) == 0) && (mouse_buttons != mouse_buttons_old)) {\
		if (grabbed) {
			klog(LL_DEBUG, "Dropped window %x\n", (uint64_t)f);
			grabbed = 0;
			f->pos_x = mouse_x;
			f->pos_y = mouse_y;
			redraw_scene();
		}
	}

	// Check if a user is dragging a window around
	if ( (mouse_buttons & 0b1) && (grabbed == 1) && (mouse_moved == 1)) {
		// gfx_draw_filled_rect(mouse_x, mouse_y, f->width, f->height, 0x000000);
		// f->pos_x = mouse_x;
		// f->pos_y = mouse_y;
		// add_dirty_region(mouse_x, mouse_y, f->width, f->height);
	}

	// Check if user has clicked the mouse button
	if ((mouse_buttons & 0b1) && (mouse_buttons != mouse_buttons_old)) {
		
		// Check if user has clicked the title bar (to "grab" it)
		// TODO: Let the user grab the titlebar of non-focused windows
		if ((mouse_x > f->pos_x) && (mouse_x < f->pos_x+f->width)
			&& (mouse_y > f->pos_y-27) && (mouse_y < f->pos_y)) {
			klog(LL_DEBUG, "Titlebar clicked!\n", 0);
			grabbed = 1;
		}

		uint64_t max_z = 0;
		uint64_t max_z_id = 0;
		UIWindow *curr = first_window;
		UIWindow *selected_win = 0;

		// Find the window clicked with the greated z-depth (i.e. the one on top)
		while (curr != 0) {
			if ((mouse_x>curr->pos_x) &
				(mouse_x<curr->pos_x+curr->width) &
				(mouse_y>curr->pos_y-20) &
				(mouse_y<curr->pos_y+curr->height)) {
					if (curr->z > max_z) {
						max_z = curr->z;
						// max_z_id = curr->wid;
						selected_win = curr;
					}
			}
			curr = curr->next;
		}	

		// A window was clicked on
		if (selected_win != 0) {

			// Clicked window already has focus
			if (selected_win == f) {
				klog(LL_DEBUG, "Window already focused\n", 0);
				check_component_clicked(selected_win, mouse_x-selected_win->pos_x, mouse_y-selected_win->pos_y);
			} else {
				focus_window(selected_win);
				normalize_z();
				draw_window(f);		// Draw the previously focussed window
				draw_window(selected_win);		// Update the newly focussed window
			}
		}


	}

	mouse_buttons_old = mouse_buttons;

	mouse_x_old = mouse_x;
	mouse_y_old = mouse_y;

	if (mouse_moved == 1) {
		show_mouse();
	}
	
}

// Iterate through the components in a window to see what element was clicked
// This needs calc_sizes to have been run first to populate the coords first
struct component *check_component_clicked(struct window *w, uint16_t x, uint16_t y)
{
	struct component *cc = w->first_component;
	while (cc != 0) {
		uint16_t cx1 = w->pos_x + cc->x;
		uint16_t cy1 = w->pos_y + cc->y;
		uint16_t cx2 = w->pos_x + cc->x + cc->width;
		uint16_t cy2 = w->pos_y + cc->y + cc->height;
		// This component has been clicked
		if ((mouse_x>cx1) && (mouse_x<cx2) && (mouse_y>cy1) && (mouse_y<cy2)) {
			if (cc->type == BUTTON) {
				uint64_t handler_addr = (uint64_t)((UIButton *)cc->element)->fn_on_click;
				UIButton *btn = (UIButton *)cc->element;
				ipc_send_64(w->pid, handler_addr, IPC_LBTN_CLICK);
				return cc;
			}
		}
		cc = cc->next;
	}
	return 0;
}

void show_mouse()
{
	syscall(SYSCALL_SHOW_MOUSE, (uint64_t)pointer, mouse_x, mouse_y, 0);
}

void hide_mouse()
{
	syscall(SYSCALL_HIDE_MOUSE, mouse_x_old, mouse_y_old, 0, 0);
}

void init_mouse(void)
{
    pointer = malloc(sizeof(uint32_t)*9*12);
	mouse_data = malloc(sizeof(uint64_t));
	for (int i=0; i<12*15; i++)
		pointer[i] = pointer_img[i];
	mouse_buttons = 0;
	mouse_buttons_old = 0;
	grabbed = 0;
}