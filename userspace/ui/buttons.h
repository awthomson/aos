#ifndef __BUTTON_H__
#define __BUTTON_H__

struct s_UIButton {
	uint8_t state;
	char text[256];
	void *fn_on_click;
};

typedef struct s_UIButton UIButton;

void draw_button(struct component *comp);

#endif