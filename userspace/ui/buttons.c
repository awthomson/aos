#include "primatives.h"
#include "windows.h"

#include "buttons.h"

#define COL_BTN         0xaaaaaa
#define COL_BTN_TEXT    0x000000


void draw_button(struct component *comp)
{
    UIButton *button = comp->element;
    uint16_t x = comp->x + comp->owner->pos_x;      // Absolute X position on the screen
    uint16_t y = comp->y + comp->owner->pos_y;      // Absolute Y position on the screen
    uint16_t w = comp->width;
    uint16_t h = comp->height;
    gfx_draw_filled_rect(x, y, w, h, COL_BTN);
    gfx_draw_rect(x, y, w, 1, 0xffffff);
    gfx_draw_rect(x, y, 1, h, 0xffffff);
    gfx_draw_rect(x+w-1, y, 1, h, 0x444444);
    gfx_draw_rect(x, y+h-1, w, 1, 0x444444);

    gfx_draw_string(button->text, x+6, y+6, 0x0);
}