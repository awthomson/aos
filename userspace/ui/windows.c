#include "compost.h"
#include "windows.h"
#include "primatives.h"
#include "buttons.h"

#define COL_DISABLED 0x808080
#define COL_ACTIVE   0xffffff

extern uint32_t *gfx_buff;

void calc_component_sizes(UIComponent *c, int context_x, int context_y, int context_width, int context_height)
{

	if (c == 0) {
		klog(LL_WARN, "calc_component_size(): Null component passed\n", 0);
		return;
	}

	int layout;
	int x = context_x;
	int y = context_y;
	int h = context_height;
	int w = context_width;
	int nat_height, nat_width;

	if (c->parent != 0) {
		layout = c->parent->layout;
		// klog(LL_DEBUG, "Parent component has layout = %d\n", layout);
	} else {
		layout = 0;
		// klog(LL_DEBUG, "No parent so layout = %d\n", layout);
	}

	if (layout == 0) {
		// Vertical containers
		nat_height = h / num_components(c);
		nat_width  = w;
	} else {
		// Horizontal containers
		nat_height = h;
		nat_width  = w / num_components(c);
	}

	while (c != 0) {

		c->width = nat_width;
		c->height = nat_height;
		c->x = x;
		c->y = y;
		// klog(LL_DEBUG, "This: %x\n", (uint64_t)c);
		// klog(LL_DEBUG, "Prnt: %x\n", (uint64_t)c->parent);
		// klog(LL_DEBUG, "L: %d\n", layout);
		// klog(LL_DEBUG, "X: %d\n", x);
		// klog(LL_DEBUG, "Y: %d\n", y);
		// klog(LL_DEBUG, "W: %d\n", c->width);
		// klog(LL_DEBUG, "H: %d\n", c->height);

		if (c->children != 0)
			calc_component_sizes(c->children, x, y, nat_width, nat_height);

		if (layout == 0)
			y += c->height;
		if (layout == 1)
			x += c->width;

		c = c->next;
	}
}

void focus_window(UIWindow *win)
{
	klog(LL_DEBUG, "Focussing on window %x\n", (uint64_t)win);
	UIWindow *c = first_window;
	while (c != 0) {
		if (c == win)
			c->has_focus = 1;
		else
			c->has_focus = 0;
		c = c->next;
	}
}

void calc_sizes(UIWindow *win)
{
	// klog(LL_DEBUG, "Calculating sizes for window\n", 0);
	calc_component_sizes(win->first_component, 0, 0, win->width, win->height);
}

int num_components(struct component *first_component)
{
	struct component *c = first_component;
	int count = 0;
	while (c != 0) {
		count++;
		c = c->next;
	}
	return count;
}

void draw_windows(void)
{
	normalize_z();
	UIWindow *curr;
	for (int i=1; i<=num_windows(); i++) {
		// klog(LL_DEBUG, "Drawing window %d...\n", i);
		curr = first_window;
		while (curr != 0) {
			if (curr->z == i) {
				draw_window(curr);
			}
			curr = curr->next;
		}
	}
}

/*
	Redraw the window, and any other window with a higher z-value
	TODO: We can improve performance by only redrawing windows with
	a higher z-value and that overlaps this window
*/
void draw_window(UIWindow *win)
{
	// klog(LL_INFO, "windows.c: draw_window(%x)\n", (uint64_t)win);
	// klog(LL_DEBUG, "Focussed: %d\n", win->has_focus);

	calc_sizes(win);
	normalize_z();

	if (win == 0)
		return;

	// if (win->needs_redraw == 0)
	// 	return;

	int x = win->pos_x;
	int y = win->pos_y;
	int w = win->width;
	int h = win->height;
	uint64_t colour = COL_DISABLED;
	if (win->has_focus == 1)
		colour = COL_ACTIVE;
	
	 gfx_draw_filled_rect(x, y, w, h, 0);				// Inner
	// Border
	gfx_draw_rect(x-1, y-1, w+2, h+2, colour);			// Border
	gfx_draw_rect(x+10, y-20, 20, 10, 0xff0000);		// Border - close icon
	// Title
	gfx_draw_filled_rect(x-1, y-27, w+2, 27, colour);	// Title bar
	gfx_draw_string(win->title, x+22, y-25, 0x000000);	// Title text

	draw_components(win->first_component);

	add_dirty_region(x-1, y-27, w+3, h+30);

	// If there's any windows with a higher Z draw that too
	// TODO: This can be improved by only redrawing that windows that
	// overlap
	UIWindow *c = first_window;
	while (c != 0) {
		if (c->z > win->z) {
			if (overlaps(c, win)==1)
				draw_window(c);
		}
		c = c->next;
	}

	// win->needs_redraw = 1;

}

// Determine whether two windows overlap - this is useful for speeding up
// screen refreshes
int overlaps(UIWindow *w1, UIWindow *w2)
{
	int x1_s = w1->pos_x;
	int y1_s = w1->pos_y;
	int x1_e = w1->pos_x + w1->width;
	int y1_e = w1->pos_y + w1->height;

	int x2_s = w2->pos_x;
	int y2_s = w2->pos_y;
	int x2_e = w2->pos_x + w2->width;
	int y2_e = w2->pos_y + w2->height;

	if ((x2_s >= x1_s) && (x2_s <= x1_e) && (y2_s >= y1_s) && (y2_s <= y1_e) )
		return 1;
	if ((x2_e >= x1_s) && (x2_e <= x1_e) && (y2_e >= y1_s) && (y2_e <= y1_e) )
		return 1;
	if ((x2_e >= x1_s) && (x2_e <= x1_e) && (y2_s >= y1_s) && (y2_s <= y1_e) )
		return 1;
	if ((x2_s >= x1_s) && (x2_s <= x1_e) && (y2_e >= y1_s) && (y2_e <= y1_e) )
		return 1;

	return 0;
}

void draw_components(UIComponent *comp)
{
	if (comp == 0) {
		klog(LL_WARN, "Trying to draw a null components\n", 0);
		return;
	}

	int win_x = comp->owner->pos_x;
	int win_y = comp->owner->pos_y;

	while (comp != 0) {

		if (comp->children != 0) {
			draw_components(comp->children);
		}

		if (comp->type == LABEL) {
			// klog(LL_DEBUG, "Drawing label\n", 0);
			UILabel *label = comp->element;
			gfx_draw_string(label->text, comp->x+win_x, comp->y+win_y, 0xc0c0c0);
		} else if (comp->type == BUTTON) {
			// klog(LL_DEBUG, "Drawing button\n", 0);
			draw_button(comp);
		} else if (comp->type == CANVAS) {
			klog(LL_DEBUG, "Drawing canvas\n", 0);
			UICanvas *canvas = comp->element;
			int draw_height = min(comp->height, canvas->height);
			int draw_width = min(comp->width, canvas->width);
			uint32_t *src = canvas->buffer;
			uint32_t *dest = gfx_buff;
			klog(LL_DEBUG, "Draw width:  %d\n", draw_width);
			klog(LL_DEBUG, "Draw height: %d\n", draw_height);
			for (uint16_t y = 0; y<draw_height; y++) {
				for (uint16_t x = 0; x<draw_width; x++) {
					uint16_t destx = x+win_x+comp->x;
					uint16_t desty = y+win_y+comp->y;
					// dest[destx+desty*SCREEN_WIDTH] = 0x800000;
					dest[destx+desty*SCREEN_WIDTH] = src[x+(y*canvas->width)];
				}
			}
		}
		comp = comp->next;
	}
}


uint64_t num_windows(void)
{
	uint64_t num = 0;
	struct window *curr = first_window;
	while (curr != 0) {
		num++;
		curr = curr->next;
	}
	return num;
}

// Set z depth for each window, with the focused window at the largest z
void normalize_z(void)
{
	struct window *curr = first_window;
	uint64_t win_cnt = 0;
	while (curr != 0) {
		if (curr->has_focus == 0) {
			win_cnt++;
			curr->z = win_cnt;
		} else {
			curr->z = num_windows();	
		}
		curr = curr->next;
	}	
}

struct window *get_focused_window(void)
{
	struct window *curr = first_window;
	while (curr != 0) {
		if (curr->has_focus == 1)
			return curr;
		curr = curr->next;
	}	
	return 0;
}
