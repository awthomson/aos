#ifndef __LIBBMP_H__
#define __LIBBMP_H__

void load_bmp(const char *fname, void *buffer);

#endif