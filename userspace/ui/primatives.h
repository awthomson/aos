#ifndef __PRIMATIVES_H__
#define __PRIMATIVES_H__

#include <alibc.h>
#include "windows.h"

// #define SCREEN_WIDTH 1280
// #define SCREEN_HEIGHT 800

void gfx_draw_rect(int x, int y, int w, int h, uint32_t colour);
void gfx_draw_filled_rect(int x, int y, int w, int h, uint32_t colour);
void draw_area(uint32_t *buffer, int pos_x, int pos_y, int width, int height);
void store_area(uint32_t *dest_buffer, int pos_x, int pos_y, int width, int height);
void gfx_draw_char(char chr, int x, int y, uint32_t colour);
void gfx_draw_pixel(int x, int y, uint32_t colour);
void gfx_draw_string(const char *str, int x, int y, uint32_t colour);
void gfx_draw_line(int x1, int y1, int x2, int y2, uint32_t colour);
void gfx_canvas_draw_rect(uint64_t canvas, int x, int y, int w, int h, uint32_t colour);
void gfx_draw_canvas(UICanvas *canvas);
void gfx_canvas_draw_filled_rect(uint64_t canvas, int x, int y, int w, int h, uint32_t colour);

#endif