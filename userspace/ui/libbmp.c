#include <stdio.h>
#include <stdlib.h>
#include <alibc.h>

#include "libbmp.h"

struct bmp_header {
    char header_field[2];
    uint32_t filesize;
    uint32_t reserved;
    uint32_t data_offset;
}__attribute__((__packed__));

struct bmp_info_header {
    uint32_t header_size;
    uint32_t width;
    uint32_t height;
    uint16_t colour_planes;
    uint16_t bpp;
    uint32_t compression_method;
    uint32_t image_size;
    uint32_t horizontal_resolution;
    uint32_t vertical_resolution;
    uint32_t colours_in_palette;
    uint32_t num_important_colours;
}__attribute__((__packed__));

void load_bmp(const char *fname, void *buffer)
{
    klog(LL_INFO, "libbmp.c: load_bmp(0x%x)\n", (uint64_t)buffer);
    FILE *fd = fopen(fname, "r");
    char *file_data = malloc(fd->fsize*sizeof(char));
    fread(file_data, fd->fsize, 1, fd);

    struct bmp_header *header = (struct bmp_header *)file_data;
    struct bmp_info_header *info_header = (struct bmp_info_header *)(file_data+14);

    int width = info_header->width;
    int height = info_header->height;

    uint8_t *src = file_data+header->data_offset;
    uint32_t *dest = buffer;

    dest = buffer+(width*height);

    for (int y=height; y>1; y--) {
        dest = buffer+((width*y)*4);
        for (int x=0; x<width; x++) {
            uint8_t r = *src++;
            uint8_t g = *src++;
            uint8_t b = *src++;
            *dest =     (b<<16) + (g<<8) + r;    
            dest++;    
        }
    }

}
