#include "windows.h"
#include "primatives.h"

#include "Liberation-Sans.h"
#include "Liberation-Sans_meta.h"

// gfx_buff is used as the desktop workspace
extern uint32_t *gfx_buff;

void gfx_draw_line(int x1, int y1, int x2, int y2, uint32_t colour)
{
    int sdx, sdy, dxabs, dyabs;

    int dx = x2 - x1;
    int dy = y2 - y1;

    if (dx<0) {
        dxabs = -dx;
        sdx = -1;
    }  else {
        dxabs = dx;
        sdx = 1;
    }

    if (dy<0) {
        dyabs = -dy;
        sdy = -1;
    } else { 
        dyabs = dy;   
        sdy = 1;     
    }

    int x = dyabs>>1;
    int y = dxabs>>1;
    int px = x1;
    int py = y1;

    if (dxabs>=dyabs) {
        // Mostly horizontal line
        for(int i = 0; i < dxabs; i++) {
            y += dyabs;
            if (y >= dxabs) {
                y -= dxabs;
                py += sdy;
            }
            px += sdx;
            gfx_draw_pixel(px, py, colour);
        }
    } else {
        // Mostly vertical line
        for(int i = 0; i < dyabs; i++) {
            x += dxabs;
            if (x >= dyabs) {
                x -= dyabs;
                px += sdx;
            }
            py += sdy;
            gfx_draw_pixel(px, py, colour);
        }
    }
}    

void gfx_draw_string(const char *str, int x, int y, uint32_t colour)
{
	while (*str != '\0') {
		gfx_draw_char(*str, x, y, colour);
        if (*str >= ' ') {
            uint8_t w = *str-' ';
    		x+=font_width[w];
        }
		str++;
	}
}

void gfx_draw_char(char chr, int x, int y, uint32_t colour)
{
    if (chr < 32)
        return;

    int font_height = 24;
    uint16_t font_offset = (chr-' ')*font_height*2;
    for (int fy=0; fy<font_height; fy++) {
        for (int fx=0; fx<16; fx++) {
            uint16_t font_line = (uint16_t)font_data[font_offset+(fy*2)];
            font_line += font_data[font_offset+(fy*2)+1]<<8;
            if (font_line & (1<<fx)) {
                gfx_draw_pixel(x+fx, y+fy, colour);
            }
        }
    }
}

void gfx_draw_pixel(int x, int y, uint32_t colour)
{
    if (x<0) x = 0;
    if (y<0) y = 0;
    if (x>SCREEN_WIDTH) x = SCREEN_WIDTH;
    if (y>SCREEN_HEIGHT) y = SCREEN_HEIGHT;
    gfx_buff[x+(y*SCREEN_WIDTH)] = colour;
}


void gfx_draw_rect(int x, int y, int w, int h, uint32_t colour)
{
    // TODO: Don't clamp to sides of screen, clip it instead
    if (x<0) x = 0;
    if (y<0) y = 0;
    if (x+w>SCREEN_WIDTH) w = SCREEN_WIDTH-x;
    if (y+h>SCREEN_HEIGHT) h = SCREEN_HEIGHT-y;
    int fbw=SCREEN_WIDTH;
    for (uint16_t dy=y; dy<y+h; dy++) {
        gfx_buff[x+(dy*fbw)] = colour;
        gfx_buff[x+w+(dy*fbw)] = colour;
    }
    for (uint16_t dx=x; dx<x+w; dx++) {
        gfx_buff[dx+(y*fbw)] = colour;
        gfx_buff[dx+((y+h)*fbw)] = colour;
    }
}

void gfx_canvas_draw_rect(uint64_t canvas, int x, int y, int w, int h, uint32_t colour)
{
    UICanvas *c = ((struct component *)canvas)->element;
    uint32_t *buff = c->buffer;
    uint64_t canw = c->width;
    uint64_t canh = c->height;
    for (uint16_t dy=y; dy<y+h; dy++) {
        buff[x+(dy*canw)] = colour;
        buff[x+w+(dy*canw)] = colour;
    }
    for (uint16_t dx=x; dx<x+w; dx++) {
        buff[dx+(y*canw)] = colour;
        buff[dx+((y+h)*canw)] = colour;
    }
}

void gfx_canvas_draw_filled_rect(uint64_t canvas, int x, int y, int w, int h, uint32_t colour)
{
    struct component *c = (struct component *)canvas;
    UICanvas *e = ((struct component *)canvas)->element;
    uint32_t *buff = e->buffer;
    uint64_t canw = e->width;
    uint64_t canh = e->height;
    for (uint16_t dy=y; dy<y+h; dy++) {
        for (uint16_t dx=x; dx<x+w; dx++) {
            buff[dx+(dy*canw)] = colour;
        }
    }
    c->owner->needs_redraw = 1;
}

void gfx_draw_filled_rect(int x, int y, int w, int h, uint32_t colour)
{
    if (x<0) x = 0;
    if (y<0) y = 0;
    if (x+w>SCREEN_WIDTH) w = SCREEN_WIDTH-x;
    if (y+h>SCREEN_HEIGHT) h = SCREEN_HEIGHT-y;

    int fbw=SCREEN_WIDTH;
    for (uint16_t dy=y; dy<y+h; dy++) {
        for (uint16_t dx=x; dx<x+w; dx++) {
            gfx_buff[dx+(dy*fbw)] = colour;
        }
    }
}

void draw_area(uint32_t *buffer, int pos_x, int pos_y, int width, int height)
{
	uint32_t *offset = gfx_buff + (pos_x) + (pos_y*SCREEN_WIDTH);
	for (int y=0; y<15; y++) {
		for (int x=0; x<12; x++) {
			*offset = *buffer;
			// if (*buffer != c)
			// 	*offset = *buffer;
			buffer++;
			offset++;
		}
		offset += (SCREEN_WIDTH-12);
	}
}

void store_area(uint32_t *dest_buffer, int pos_x, int pos_y, int width, int height)
{
	uint32_t *src = gfx_buff + (pos_x) + (pos_y*SCREEN_WIDTH);
	for (int y=0; y<15; y++) {
		for (int x=0; x<12; x++) {
			*dest_buffer++ = *src++;
		}
		src += (SCREEN_WIDTH-12);
	}	
}
