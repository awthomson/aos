#ifndef __MOUSE_H__
#define __MOUSE_H__

#include <alibc.h>
#include "windows.h"

void show_mouse();
void hide_mouse();
void update_mouse();
void init_mouse();\

// TODO: This should probably be part of windows.c or compost or somewhere more appropriate?
struct component *check_component_clicked(struct window *w, uint16_t x, uint16_t y);

#endif