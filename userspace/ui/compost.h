#ifndef __COMPOST_H__
#define __COMPOST_H__

#include <alibc.h>

void draw_bg();
void draw_taskbar(void);
void draw_dirty_regions(void);
void add_dirty_region(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
void check_events(void);
void redraw_scene(void);

#endif
