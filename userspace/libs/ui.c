#include "ui.h"
#include "alibc.h"
#include "stdlib.h"
#include "string.h"

#define LABEL       0x01
#define BUTTON      0x02
#define CANVAS      0x03

void ui_event_loop(void)
{
    while(1) {
        uint64_t size, type, from;
        uint64_t msg_id = ipc_check(&type, &from);
        if ((type == IPC_LBTN_CLICK) &&  (msg_id != 0)) {
            uint64_t handler_addr = ipc_get_64(msg_id);
            uint64_t addr = handler_addr;
            void (*f)() = (void(*)())handler_addr;
            (*f)();
        } 
        wait(1);
    }
}

void ui_redraw_window(UIWindow *window)
{
    ipc_send_64(0, (uint64_t)window, IPC_REDRAW_WINDOW);
}


void debug_components(UIComponent *c) {
    while (c!=0) {
        klog(LL_INFO, "Component:    %x ---------------------------\n", (uint64_t)c);
        klog(LL_DEBUG, "X: %d\n", c->x);
        klog(LL_DEBUG, "Y: %d\n", c->y);
        klog(LL_DEBUG, "W: %d\n", c->width);
        klog(LL_DEBUG, "H: %d\n", c->height);
        if (c->type == 1) {
            klog(LL_DEBUG, "Element (Label):   %x\n", (uint64_t)c->element);
            UIButton *b = (UIButton *)c->element;
            klog(LL_DEBUG, "Label text: %s\n", (uint64_t)b->text);
        } else if (c->type == 2) 
            klog(LL_DEBUG, "Element (Button):  %x\n", (uint64_t)c->element);
        else if (c->type == 3) 
            klog(LL_DEBUG, "Element (Canvas):  %x\n", (uint64_t)c->element);
        else 
            klog(LL_DEBUG, "Element (Unknown):  %x\n", (uint64_t)c->element);
        klog(LL_DEBUG, "Children:     %x\n", (uint64_t)c->children);
        klog(LL_DEBUG, "Next:         %x\n", (uint64_t)c->next);
        if (c->children != 0) {
            debug_components(c->children);
        }
        c = c->next;    
    }
}

void ui_debug_window(UIWindow *window)
{
    // ipc_send_64(0, (uint64_t)window, IPC_DEBUG_WIN);
    klog(LL_DEBUG, "Debug - got request for debug of window %x\n", (uint64_t)window);
    klog(LL_DEBUG, "Width:   %d\n", window->width);
    klog(LL_DEBUG, "Height:  %d\n", window->height);
    UIComponent *c = window->first_component;
    debug_components(c);
}

UIComponent *ui_container_new(UIWindow *owner, UIComponent *parent, int layout)
{
    klog(LL_DEBUG, "ui.c: ui_container_new(0x%x)\n", (uint64_t)owner);
    UIComponent *this_c = malloc(sizeof(UIComponent));
    share_mem(this_c, sizeof(UIComponent), 2);  
    this_c->layout = layout;
    this_c->children = 0;
    this_c->element = 0;
    this_c->owner = owner;
    this_c->parent = parent;

    // If no parent is specifed, put it in the window root list
    if (parent == 0) {
        klog(LL_DEBUG, "Adding to window root components\n", 0);
        this_c->next = owner->first_component;
        owner->first_component = this_c;
    } else {
        klog(LL_DEBUG, "Adding as a child components\n", 0);
        parent->children =  this_c;
        this_c->next = 0;
    }

    return this_c;
}

void ui_canvas_draw_filled_rect(UICanvas *canvas, uint64_t x, uint64_t y, uint64_t width, uint64_t height, uint32_t colour)
{
    uint32_t *buff = canvas->buffer;
    uint64_t canw = canvas->width;
    uint64_t canh = canvas->height;
    for (int dy=y; dy<y+height; dy++) {
        for (int dx=x; dx<x+width; dx++) {
            buff[dx+(dy*canw)] = colour;
        }
    }
}

UIWindow *ui_window_new(uint16_t width, uint16_t height, const char *title)
{
	klog(LL_DEBUG, "ui.c: ui_window_new()\n", 0);
    UIWindow *this_win = malloc(sizeof(UIWindow));
    share_mem(this_win, sizeof(UIWindow), 2);
	this_win->pos_x  = (rand()%(SCREEN_WIDTH-100))+10;
	this_win->pos_y  = (rand()%(SCREEN_HEIGHT-100))+10;
	this_win->width  = width;
	this_win->height = height;
    this_win->pid = get_pid();
    this_win->has_focus = 0;
    this_win->first_component = 0;
    strncpy(this_win->title, title, strlen(title));
	ipc_send_64(0, (uint64_t)this_win, IPC_NEW_WINDOW);
    return this_win;
}


UIButton *ui_button_new(UIWindow *owner, UIComponent *parent, const char *button_text, void (*on_click)(void))
{
	klog(LL_DEBUG, "ui_button_new()\n", 0);

    UIComponent *component = malloc(sizeof(UIComponent));
    share_mem(component, sizeof(UIComponent), 2);

    UIButton *button = malloc(sizeof(UIButton));
    share_mem(button, sizeof(UIButton), 2);

    strncpy(button->text, button_text, strlen(button_text));

    component->type = BUTTON;
    component->owner = owner;
    component->element = button;
    component->children = 0;
    component->parent = parent;
    button->fn_on_click = on_click;

    // If no parent is specifed, put it in the window root list
    if (parent == 0) {
        klog(LL_DEBUG, "Adding to window root components\n", 0);
        component->next = owner->first_component;
        owner->first_component = component;
    } else {
        klog(LL_DEBUG, "Adding as a child components\n", 0);
        component->next = parent->children;
        parent->children =  component;
    }

    return button;
}


UILabel *ui_label_new(UIWindow *owner, UIComponent *parent, const char *label_text)
{
	klog(LL_DEBUG, "ui_label_new()\n", 0);

    // Create the button conatiner
    UIComponent *component = malloc(sizeof(UIComponent));
    share_mem(component, sizeof(UIComponent), 2);
    component->type = LABEL;
    component->owner = owner;
    component->parent = parent;

    // Create the button
    UILabel *label = malloc(sizeof(UILabel));
    share_mem(label, sizeof(UILabel), 2);
    strncpy(label->text, label_text, strlen(label_text));

    component->element = label;
    component->children = 0;

    // If no parent is specifed, put it in the window root list
    if (parent == 0) {
        klog(LL_DEBUG, "Adding label to window root components\n", 0);
        component->next = owner->first_component;
        owner->first_component = component;
    } else {
        klog(LL_DEBUG, "Adding label as a child components\n", 0);
        if (parent->children == 0) {
            parent->children =  component;
            component->next = 0;
        } else {
            component->next = parent->children;
            parent->children =  component;
        }
    }

    return label;
}

UICanvas *ui_canvas_new(UIWindow *owner, UIComponent *parent, uint16_t width, uint16_t height)
{
	klog(LL_DEBUG, "ui_canvas_new()\n", 0);

    // Create the button conatiner
    UIComponent *component = malloc(sizeof(UIComponent));
    share_mem(component, sizeof(UIComponent), 2);

    UICanvas *canvas = malloc(sizeof(UICanvas));
    share_mem(canvas, sizeof(UICanvas), 2);

    uint32_t *canvas_buffer = malloc(sizeof(uint32_t)*width*height*4);
    for (int i=0; i<width*height; i++)
        canvas_buffer[i] = 0xffffff;
    klog(LL_INFO, "Canvas buffer addr %x\n", (uint64_t)canvas_buffer);

    share_mem(canvas_buffer, sizeof(uint32_t)*width*height*4, 2);

    component->type = CANVAS;
    component->owner = owner;
    component->element = canvas;
    component->width = width;
    component->height = height;
    component->children = 0;
    component->parent = parent;

    canvas->buffer = canvas_buffer;
    canvas->width = width;
    canvas->height = height;

    // If no parent is specifed, put it in the window root list
    if (parent == 0) {
        klog(LL_DEBUG, "Adding canvas to window root components\n", 0);
        component->next = owner->first_component;
        owner->first_component = component;
    } else {
        klog(LL_DEBUG, "Adding canvas as a child components\n", 0);
        if (parent->children == 0) {
            parent->children =  component;
            component->next = 0;
        } else {
            component->next = parent->children;
            parent->children =  component;
        }
    }

    // klog(LL_DEBUG, "Painting canvas\n", 0);
	// ui_canvas_draw_filled_rect(canvas, 0, 0, width, height, 0x008000);

    return canvas;
    
}