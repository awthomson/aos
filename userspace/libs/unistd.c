#include <unistd.h>
#include <syscall.h>
#include <string.h>
#include <alibc.h>

int exec(const char *str)
{
	int x;
	syscall(SYSCALL_EXEC, (uint64_t)str, (uint64_t)&x, 0, 0);
	return x;
}

void readdir(struct dirent *de)
{	
	syscall(SYSCALL_READ_DIR, (uint64_t)de, 0, 0, 0);
}

/*
	Set working directory via. string
*/
int setcwd(const char *str)
{
	int res;
	syscall(SYSCALL_SET_CWD, (uint64_t)str, (uint64_t)&res, 0, 0);
	return res;
}

int getcwd(void)
{
	int res;
	syscall(SYSCALL_GET_CWD, (uint64_t)&res, 0, 0, 0);
	return res;
}
