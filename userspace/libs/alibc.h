#ifndef __ALIBC_H__
#define __ALIBC_H__

#define NULL 0
#define LL_WARN     0
#define LL_INFO     1
#define LL_DEBUG    2
#define LL_ERROR    3
#define LL_FATAL    4

#define IPC_REDRAW_SCREEN           0x001
#define IPC_CANVAS_RECT             0x002
#define IPC_CANVAS_FILLED_RECT      0x003
#define IPC_NEW_WINDOW              0x100
// #define IPC_NEW_COMPONENT           0x101
// #define IPC_NEW_ELEMENT             0x104
// #define IPC_NEW_LABEL               0x105
// #define IPC_NEW_BUTTON              0x106
// #define IPC_NEW_CANVAS              0x107
#define IPC_REDRAW_WINDOW           0x108
#define IPC_OK                      0x200
// #define IPC_DEBUG_WIN               0x300
#define IPC_LBTN_CLICK              0x400

typedef short int           int16_t;
typedef unsigned char       uint8_t;
typedef unsigned short int  uint16_t;
typedef unsigned int        uint32_t;
typedef unsigned long int   uint64_t;

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 900



struct process_info {
    uint64_t rip;					// 0x00
    uint64_t cs;					// 0x08
    uint64_t flags;					// 0x10
    uint64_t rsp;					// 0x18
    uint64_t ss;					// 0x20
	
	uint64_t rbp;					// 0x28
    uint64_t rax;					// 0x30
    uint64_t rbx;					// 0x38
    uint64_t rcx;					// 0x40
    uint64_t rdx;					// 0x48
    uint64_t rdi;					// 0x50
    uint64_t rsi;					// 0x58
    uint64_t r8;					// 0x60
    uint64_t r9;					// 0x68
    uint64_t r10;					// 0x70
    uint64_t r11;					// 0x78
    uint64_t r12;					// 0x80
    uint64_t r13;					// 0x88
    uint64_t r14;					// 0x90
    uint64_t r15;					// 0x98

    uint64_t pml_ptr;				// 0xa0
    uint64_t pid;					// 0xa8
    uint8_t state;					// 0xb0
    uint8_t priority;				// 0xb1
	uint64_t kernel_pml;			// 0xb2
	uint64_t wait_until;			// 0xba

	uint64_t cwd;					// 0xbe

    char *stdin_buffer;
    uint64_t stdin_buffer_pos;
    char *stdout_buffer;
    uint64_t stdout_buffer_pos;

}__attribute__(( packed ));

void share_mem(void *addr, uint64_t len, uint64_t pid);
void memcpy(const char *src, char *dest, uint64_t size);
void scroll_up(uint32_t lines);
uint8_t check_keypress(void);
uint8_t wait_keypress(uint8_t c);
void draw_char(uint8_t c, int x, int y, uint64_t colour) ;
void wait(uint64_t ms);
uint64_t get_uptime_ns(void);
void get_mem(void);
void debug_ps(void);
int atoi(const char *str);
int kill(uint64_t pid);
int proc_info(struct process_info *buff, int pid);
int kill_me(void);
void memset(char *buff, char c, uint64_t size);
void klog(int log_level, const char *str, uint64_t arg);
uint64_t min(uint64_t a, uint64_t b);
uint64_t get_pid(void);

void *read_disk_sectors(uint64_t start_sector, uint64_t num_sectors);

uint64_t ipc_check(uint64_t *type, uint64_t *from_pid);
uint64_t ipc_get_64(uint64_t id);
void ipc_send_64(uint64_t dest_pid, uint64_t val, uint64_t type);

#endif
