/*
#ifndef __UNISTD_H__
#define __UNISTD_H__
*/

#include <stdint.h>

struct dirent {
  uint32_t inode;
  uint16_t rec_len;
  uint8_t name_len;
  uint8_t file_type;
  uint8_t name[256];
}__attribute__((packed));

// struct dirent *readdir(void);
int setcwd(const char *);
int getcwd(void);
int exec(const char *);
void readdir(struct dirent *de);
// #endif
