#include <syscall.h>

__attribute__((naked, noreturn)) uint64_t syscall(uint64_t syscall_func, uint64_t arg1, uint64_t arg2, uint64_t arg3, uint64_t arg4) {
	asm volatile(
        "mov r8, %0;"
        "mov r9, %1;"
        "mov r10, %2;"
        "mov rbx, %3;"
        "mov r12, %4;"
		"syscall;"
		"retq;"
		: :"r"(syscall_func), "r"(arg1), "r"(arg2), "r"(arg3), "r"(arg4) :
	);
}

