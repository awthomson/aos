#include <string.h>
#include <alibc.h>

char *strtok(char *s, const char delim)
{
    static char *pos = 0;

    if ((s == 0) && ((uint64_t)pos == -1))
        return 0;

    if (s == 0)
        s = pos;

    char *bu = s;
    while (*s != 0) {
        if (*s == delim) {
            *s = '\0';
            pos = s+1;
            return bu;
        }
        s++;
    }
    pos =(char *)-1;            // Indicate there's no more tokens after this one
    return bu;
}


int strlen(const char *str) 
{
	int l = 0;
	while (*str++ != 0)
		l++;
	return l;
}

char *strncpy(char *destination, const char* source, unsigned int num)
{
    if (destination == 0)
        return 0;
 
    char* ptr = destination;
 
    while (*source && num--)
    {
        *destination = *source;
        destination++;
        source++;
    }
 
    *destination = '\0';
 
    return ptr;
} 

int strncmp(const char *s1, const char *s2, unsigned long int max)
{
	unsigned long int i;
	for (i=0; i<max; i++) {
		if ((s1[i] == 0) && (s2[i] == 0))
			return 0;
		else if (s1[i] < s2[i])
			return 1;
		else if (s1[i] > s2[i])
			return -1;
	}
			
	return 0;
}

int strcmp(const char *s1, const char *s2)
{
    unsigned long int i;
    while(1) {
        if ((s1[i] == 0) && (s2[i] == 0))
            return 0;
        else if (s1[i] < s2[i])
            return 1;
        else if (s1[i] > s2[i])
            return -1;
		i++;
    }
}

