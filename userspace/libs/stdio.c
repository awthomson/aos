#include <alibc.h>
#include <syscall.h>
#include <stdlib.h>
#include <stdargs.h>
#include <string.h>
#include <stdargs.h>
#include <stdio.h>

int feof(FILE *f)
{
	int res = 0;
	if (f->fpos >= f->fsize)
		return 1;
	else
		return 0;
}

// TODO Read num * size at a time and return how many nums were successful
int fread(void *ptr, uint64_t size, uint64_t num, FILE *f)
{
	syscall(SYSCALL_FREAD, (uint64_t)ptr, (uint64_t)(size*num), (uint64_t)f, 0);	
	f->fpos += (size*num);
	if (f->fpos > f->fsize)
		f->fpos = f->fsize;
	return 0;
}

/*
	fopen

	Returns:
		Error code or 0
*/
FILE *fopen(const char *fname, const char *mode)
{	
	// Kernel doesn't have access to the const char buffer in userland, so malloc some
	// memory and copy to it before jumping to ring-0
	char *tmpname = malloc(strlen(fname));
	strncpy(tmpname, fname, strlen(fname));
	FILE *result = malloc(sizeof(FILE));
	syscall(SYSCALL_FOPEN, (uint64_t)tmpname, (uint64_t)mode, (uint64_t)result, 0);
	return result;
}

int printf(const char *str)
{
	// Kernel doesn't have access to the const char buffer in userland, so malloc some
	// memory and copy to it before jumping to ring-0
	char *tmpbuff = malloc(strlen(str));
	strncpy(tmpbuff, str, strlen(str));
	syscall(SYSCALL_WRITE_STDOUT, (uint64_t)tmpbuff, 0, 0, 0);
/*
	uint64_t x = strlen(str);
	int y = 1;
	syscall(SYSCALL_PRINT_NUM, (uint64_t)x, 0, 0, 0);
	char *tmpbuff = malloc(x);
	memcpy(str, tmpbuff, x);
	syscall(SYSCALL_PRINT_STR, (uint64_t)tmpbuff, 0, 0, 0);
	syscall(SYSCALL_WRITE_STDOUT, (uint64_t)tmpbuff, 0, 0, 0);
	//int z = y/(y-1);
/*
	syscall(SYSCALL_WRITE_STDOUT, (uint64_t)tmpbuff, 0, 0, 0);
*/
	return 0;
}

int sprintf(char *dest, const char *fmt, ...)
{

    const char *p;
	
	va_list argp;
    va_start(argp, fmt);
	char buff[20];

	char c;

   for(p = fmt; *p != '\0'; p++) {
        if(*p != '%') {
            *dest++ = *p;
            continue;
        }

		int i;
        switch(*++p) {
        case 'c':
            i = va_arg(argp, int);
            *dest++ = i;
            break;
        case 'd':
            i = va_arg(argp, unsigned int);
            itoa(i, buff, 10);
            strncpy(dest, buff, 10);
			dest += strlen(buff);
            break;
		}
	};
	*dest++ = '\0';
	*dest = '\0';

    va_end(argp);

}
