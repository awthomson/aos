#ifndef __TIME_H__
#define __TIME_H__

#include <alibc.h>

uint64_t get_time_ns(void);

#endif
