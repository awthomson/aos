#include <stdlib.h>
#include <syscall.h>
#include <alibc.h>

#define RAND_MAX 2147483647

unsigned int seed = 12345678;

void srand(uint64_t s)
{
    seed = s;
}

unsigned int rand()
{
    // static unsigned int prev = (unsigned int)get_uptime_ns();
    static int a = 27;                      // Multiplier
    static int c = 702070207;               // Increment
    unsigned int new = (a*seed + c) % RAND_MAX;
    seed = new;
    return new;
}

void *malloc(uint64_t size)
{
    uint64_t addr = 0;
    syscall(SYSCALL_MALLOC, size, (uint64_t)&addr, 0, 0);
    return (void *)addr;
}

void free(void *addr)
{
    syscall(SYSCALL_FREE, (uint64_t)addr, 0, 0, 0);
}

char *itoa(int num, char *buff, int base)
{

    char *p1 = buff;

    unsigned int ud = num;
    int divisor = 10;

    /*  If %d is specified and D is minus, put `-' in the head. */
    if ((base == 10) && (num < 0)) {
        *buff++ = '-';
        ud = -num;
    } else if (base == 16) {
         divisor = 16;
	}
	
    do {
        int remainder = ud % divisor;
        *buff++ = (remainder < 10) ? remainder + '0' : remainder + 'a' - 10;
    } while (ud /= divisor);

    /*  Reverse BUF. */
    char *p2 = buff - 1;
    while (p1 < p2) {
    char tmp = *p1;
        *p1 = *p2;
        *p2 = tmp;
        p1++;
        p2--;
    }


	return buff;

}

