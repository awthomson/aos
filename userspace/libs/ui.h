#ifndef __UI_H__
#define __UI_H__

#include "alibc.h"

// #define SCREEN_WIDTH 1280
// #define SCREEN_HEIGHT 800

// UI component definitions - these must match those in window.h
// TODO - maybe fix that so they're in one place

typedef struct s_UIWindow UIWindow;
typedef struct s_UIComponent UIComponent;
typedef struct s_UILabel UILabel;
typedef struct s_UIButton UIButton;
typedef struct s_UICanvas UICanvas;


struct s_UICanvas {
	uint64_t width;
	uint64_t height;
	uint32_t *buffer;
};

struct s_UIButton {
	uint8_t state;
	char text[256];
	void *fn_on_click;
};

struct s_UILabel {
	char text[256];
};

struct s_UIWindow {
	int pos_x;
	int pos_y;
	int width;
	int height;
	uint16_t z;
	uint8_t  has_focus;
	uint64_t wid;
	uint64_t pid;
	uint8_t needs_redraw;
	char title[256];
	UIComponent *first_component;
	UIWindow *next;
};

struct s_UIComponent {
	uint16_t min_width;
	uint16_t min_height;
	uint16_t max_width;
	uint16_t max_height;
	uint16_t x;						// Last calculated x position within window
	uint16_t y;						// Last calculated y position within window
	uint16_t width;					// Last calculated width
	uint16_t height;				// Last calculated height
	uint8_t type;					// Type of component (Label, Button, Canvas, etc.)
	uint8_t layout;	
	void *element;					// Pointer to specific element structure (button details, label details, etc.)
	struct s_UIWindow *owner;
	struct s_UIComponent *parent;
	struct s_UIComponent *children; 
	struct s_UIComponent *next;
};

void ui_event_loop(void);
void ui_redraw_window(UIWindow *window);
UIWindow *ui_window_new(uint16_t width, uint16_t height, const char *title);
UILabel *ui_label_new(UIWindow *owner, UIComponent *parent, const char *label_text);
UIButton *ui_button_new(UIWindow *owner, UIComponent *parent, const char *button_text, void (*on_click)(void));
UICanvas *ui_canvas_new(UIWindow *owner, UIComponent *parent, uint16_t width, uint16_t height);
void ui_debug_window(UIWindow *window);
void ui_canvas_draw_filled_rect(UICanvas *canvas, uint64_t x, uint64_t y, uint64_t width, uint64_t height, uint32_t colour);
UIComponent *ui_container_new(UIWindow *owner, UIComponent *parent, int layout);

#endif
