#ifndef __STDLIB_H__
#define __STDLIB_H__

#include "alibc.h"

char *itoa(int num, char *buff, int base);
void *malloc(uint64_t size);
void free(void *mem_addr);
unsigned int rand();
void srand(uint64_t s);


#endif
