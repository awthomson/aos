#include "syscall.h"
#include "alibc.h"
#include "stdlib.h"
#include "string.h"

uint64_t get_pid(void)
{
	uint64_t pid = 0;
	syscall(SYSCALL_GET_PID, (uint64_t)&pid, 0, 0, 0);
	return pid; 
}

void share_mem(void *addr, uint64_t len, uint64_t pid)
{
	syscall(SYSCALL_MMAP, (uint64_t)addr, len, pid, 0);
}

uint64_t min(uint64_t a, uint64_t b)
{
	if (a<b)
		return a;
	else
		return b;
}

void klog(int log_level, const char *str, uint64_t arg)
{
	char *tmpstr = malloc(strlen(str));
	strncpy(tmpstr, str, strlen(str));
	syscall(SYSCALL_KLOG, (uint64_t)log_level, (uint64_t)tmpstr, arg, 0);
	return;
}

void ipc_send_64(uint64_t dest_pid, uint64_t val, uint64_t type)
{
	syscall(SYSCALL_SEND_MSG_64, dest_pid, val, type, 0);
}

uint64_t ipc_get_64(uint64_t id)
{
	uint64_t x;
	syscall(SYSCALL_GET_MSG_64, (uint64_t)&x, id, 0, 0);
	return x;
}

uint64_t ipc_check(uint64_t *type, uint64_t *from_pid)
{
	uint64_t result;
	syscall(SYSCALL_CHECK_MSG, (uint64_t)&result, (uint64_t)type, (uint64_t)from_pid, 0);
	return result;
}

void memcpy(const char *src, char *dest, uint64_t size) 
{
	uint8_t *s = (uint8_t *)src;
	uint8_t *d = (uint8_t *)dest;
	for (uint64_t i=0; i<size; i++) {
		d[i] = s[i];
	}
}

void memset(char *buff, char c, uint64_t size)
{
	for (int i=0; i<size; i++)
		buff[i] = c;
}

int kill_me(void)
{
	syscall(SYSCALL_KILL_ME, 0, 0, 0, 0);
	while(1);
}

int proc_info(struct process_info *info_buff, int pid)
{
	return syscall(SYSCALL_PROC_INFO, (uint64_t)info_buff, pid, 0, 0);
}

int kill(uint64_t pid)
{
	return syscall(SYSCALL_KILL, pid, 0, 0, 0);
}

int atoi(const char *str)
{
    int res = 0;
 
    for (int i = 0; str[i] != '\0'; ++i)
        res = res * 10 + str[i] - '0';
 
    return res;
}

void debug_ps(void)
{
	syscall(SYSCALL_DEBUG_PS, 0, 0, 0, 0);
	return;
}

void wait(uint64_t ms) {
	syscall(SYSCALL_WAIT, ms, 0, 0, 0);
	asm volatile("int 0x69;");
	return;
}

uint8_t check_keypress(void)
{
	uint8_t x = syscall(SYSCALL_CHECK_KEYPRESS, 0, 0, 0, 0);
	return x;
}

void draw_char(uint8_t c, int x, int y, uint64_t colour)
{
	syscall(SYSCALL_DRAW_CHAR, c, x, y, colour);	
	// asm volatile("int 0x69;");
	return;
}

void scroll_up(uint32_t lines)
{
	syscall(SYSCALL_SCROLL_UP, lines, 0, 0, 0);
	return;
}

void *read_disk_sectors(uint64_t sector, uint64_t num_sectors)
{
	void *ptr = malloc(num_sectors*512);			// 512 bytes in a sector		
	int i;
	for (i=0; i<num_sectors; i++)
		syscall(SYSCALL_READ_SECTOR, (uint64_t)ptr+(i*512), sector+i, 0, 0);
	return ptr;	
}

uint64_t get_uptime_ns(void)
{
	return syscall(SYSCALL_GET_UPTIME_NS, 0, 0, 0, 0);
}

void get_mem(void)
{
	uint64_t x = 0;
	syscall(SYSCALL_GET_MEM, (uint64_t)&x, 0, 0, 0);
}
