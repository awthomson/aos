#ifndef __STRING_H__
#define __STRING_H__

char *strtok(char *s, const char delim);
int strlen(const char *str);
char *strncpy(char *destination, const char* source, unsigned int num);
int strcmp(const char *s1, const char *s2);
int strncmp(const char *s1, const char *s2, unsigned long int max);

#endif
