#ifndef __STDIO_H__
#define __STDIO_H__

#define ENOERR  0
#define ENOENT  -1
#define ENOTDIR -2

#include <alibc.h>

struct s_FILE {
    uint64_t fpos;
    uint32_t inode;
    uint8_t mode;
    uint64_t fsize;
};

typedef struct s_FILE FILE;

int fread(void *ptr, uint64_t size, uint64_t num, FILE *f);
FILE *fopen(const char *fname, const char *mode);
int sprintf(char *dest, const char *str, ...);
int printf(const char *str);
int feof(FILE *f);

#endif
