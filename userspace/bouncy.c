#include <alibc.h>
#include <syscall.h>
#include <stdlib.h>
#include <ui.h>
#include <stdio.h>

void _start(void)
{	
	klog(LL_INFO, "Starting Bouncy Bouncy\n", 0);
	UIWindow *this_win  = ui_window_new(300, 400, "Bouncy Bouncy");

	UICanvas *canvas = ui_canvas_new(this_win, 0, 300, 400);
	ui_canvas_draw_filled_rect(canvas, 0, 0, 300, 400, 0x0);

	// wait(100);

	// ui_redraw_window(this_win);

	int x = rand()%300;
	int y = rand()%300;
	int dx = -1;
	int dy = 1;
	uint32_t colour = 0x000080;
	int fc = 0;
	uint64_t start = 0;
	uint64_t elapsed = 0;
	start = get_uptime_ns();
	while(1) {
		ui_canvas_draw_filled_rect(canvas, x, y, 10, 10, colour);
		x += dx;
		y += dy;
		if (x<0) {
			x = 0;
			dx =- dx;
		}
		if (x>285) {
			x = 285;
			dx =- dx;
		}
		if (y<0) {
			y = 0;
			dy =- dy;
		}
		if (y>385) {
			y = 385;
			dy =- dy;
		}	
		// colour += 0x40;
		// ipc_send(0, 0, 0, IPC_REDRAW_SCREEN);
		ui_redraw_window(this_win);
		wait(1);
		fc++;
		if (fc%100 == 0) {
			elapsed = get_uptime_ns()-start;
			start = get_uptime_ns();
			klog(LL_INFO, "100 frames in %dms\n", elapsed/1000);	
			int fps = 100000/(elapsed/1000);
			klog(LL_DEBUG, "%d fps\n", fps);
		}
	}

	// Poll UI events
	ui_event_loop();

	kill_me();
}

