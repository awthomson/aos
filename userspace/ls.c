#include <alibc.h>
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <unistd.h>
#include <stdlib.h>

void _start(void)
{
    char *buff = malloc(256*sizeof(char));
    char *name = malloc(256*sizeof(char));
    struct dirent *de = malloc(sizeof(struct dirent));
	readdir(de);

    int i=0;
    for (i=0; i<256; i++) {
        buff[i]='\0';
        name[i]='\0';
    }
    int offset = 0;

    // while ((offset < 4096) && (de->rec_len != 0)) {
    while (offset < 4096) {
        strncpy(name, de->name, de->name_len);

        sprintf(buff, "%d ", de->inode);
		printf(buff);
		printf(name);
        sprintf(buff, "\n");
		printf(buff);
        offset += (uint64_t)de->rec_len;
		if (offset < 4096)
        	de = (void *)de+de->rec_len;
    }
	kill_me();
}

