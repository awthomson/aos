#include <alibc.h>
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <unistd.h>
#include <stdlib.h>

void _start(void)
{
    char *buff = malloc(256*sizeof(char));

	uint64_t cwd = getcwd();

    int i=0;
    for (i=0; i<256; i++) {
        buff[i]='\0';
    }
    sprintf(buff, "%d\n\0", cwd);
	printf(buff);
	kill_me();
}

