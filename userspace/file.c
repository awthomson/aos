#include <alibc.h>
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <stdlib.h>

void usage(void);


void _start(char **args, int argc)
{
	if (argc < 1) {
		usage();
		kill_me();
	}

	FILE *fd = fopen(args[0], "r");
	if (*(uint64_t *)fd == ENOENT) {
		printf("file: File not found\n");
		kill_me();
	}

	char *strbuff = malloc(256);
	sprintf(strbuff, "Size:  %d\n", fd->fsize);
	printf(strbuff);
	sprintf(strbuff, "INode: %d\n", fd->inode);
	printf(strbuff);
	sprintf(strbuff, "Mode:  %d\n", fd->mode);
	printf(strbuff);

	char *file_data = malloc(fd->fsize*sizeof(char));
	fread(file_data, fd->fsize, 1, fd);
	klog(LL_DEBUG, "Data addr: %x\n", (uint64_t)file_data);
	while(1);
	kill_me();

}

void usage(void)
{	
	printf("Usage: file <filename>\n");
}



