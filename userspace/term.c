#include <alibc.h>
#include <syscall.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define COL_BG		0x000000
#define COL_CURS 	0x60f050
#define COL_STDOUT	0xa0a0a0

#define KEY_LSHIFT 0x02
#define KEY_BS     0x03

void ls(void);
void cd(char *s);
int is_valid_char(char c);
void get_line(uint8_t *buff);
void display_sector(void);
char wait_key(char old);
void print_string(uint8_t *str, uint32_t colour);
void inc_cursor(void);
void mem(void);
int get_inode(const char *);
void new_line(void);
void load_exe(char *);
void read_stdin(char *buff);
void kill_proc(char *);
void ps_info(char *str);
void test(void);

uint16_t x = 10;
uint16_t y = 10;

char *tmpbuff;

struct dirent *de;

void _start(void) {

	char *tmpbuff = malloc(256);
	int res = 0;

	print_string("*** rawterm ***\n", 0x80ff80);
	while(1) {
		get_line(tmpbuff);
		if (strncmp(tmpbuff, "kill", 4)==0) {
			kill_proc(tmpbuff);
		} else if (strncmp(tmpbuff, "cd", 2)==0) {
			cd(tmpbuff);
		} else if (strncmp(tmpbuff, "mem", 3)==0) {
			get_mem();
		} else if (strncmp(tmpbuff, "psinfo", 6)==0) {
			ps_info(tmpbuff);
		} else if (strncmp(tmpbuff, "ps", 2)==0) {
			debug_ps();
		} else if (strncmp(tmpbuff, "debugipc", 8)==0) {
			syscall(SYSCALL_DEBUG_IPC, 0, 0, 0, 0);
		} else if (strncmp(tmpbuff, "uptime", 6)==0) {
			uint64_t ut = get_uptime_ns();
			sprintf(tmpbuff, "Uptime: %d ms\n", ut/1000);
			print_string(tmpbuff, 0x808080);
		} else {
			load_exe(tmpbuff);
		}
	}

}

void read_stdin(char *b)
{
	syscall(SYSCALL_READ_STDIN, (uint64_t)b, 0, 0, 0);	
	print_string((uint8_t *)b, COL_STDOUT);
}


void load_exe(char *str)
{
	uint64_t *res = malloc(20);
	// Find the first space character - everything after that is a argument
	char **a = malloc(4096);
	str = strtok(str, ' ');

	// Split up the remaining string into arguments
	int arg_cnt = 0;
	a[arg_cnt] = strtok(0, ' ');
	while (a[arg_cnt] != 0) {
		arg_cnt++;
		a[arg_cnt] = strtok(0, ' ');
	}

	syscall(SYSCALL_EXEC, (uint64_t)str, (uint64_t)a, (uint64_t)res, 0);

	if ((uint64_t)*res == ENOENT) {
        print_string("File not found.\n", COL_STDOUT);
		return;
	}

	if (strncmp(a[0], "bg", 2) == 0) {
		print_string("Background\n", COL_STDOUT);
		return;
	}

	struct process_info *tmp = malloc(sizeof(struct process_info));
	char *buff = malloc(4096*sizeof(char));
	int is_proc_running  = proc_info(tmp, *res);
	read_stdin(buff);
	while (is_proc_running != 0) {
		// Read stdin and wait until process has finished
		read_stdin(buff);
		wait(10);
		is_proc_running = proc_info(tmp, *res);
	}
	read_stdin(buff);
    return;
}

void cd(char *str)
{
	char *x = strtok(str, ' ');
	x = strtok(NULL, ' ');
	int res = setcwd(x);
	if (res == ENOENT) {
		print_string("Directory not found.\n", 0x808080);
	} else if (res == ENOTDIR) {
		print_string("Not a directory\n", 0x808080);
	} else {
		print_string("Ok\n", 0x808080);
	}
	return;
}

void ps_info(char *str)
{
	struct process_info *pi = malloc(sizeof(struct process_info));
    char *x = strtok(str, ' ');
    x = strtok(NULL, ' ');
    int i = atoi(x);
    proc_info(pi, i);
    return;
}

void kill_proc(char *str)
{
	char *x = strtok(str, ' ');
	x = strtok(NULL, ' ');
	int i = atoi(x);
	int z = kill(i);
	return;
}

void print_string(uint8_t *str, uint32_t colour) 
{
	int i = 0;
	while(str[i]!=0) {
		if (str[i]==10) {
			new_line();
		} else {
			draw_char(str[i], x, y, colour);
			inc_cursor();
		}
		i++;
	}
}

void new_line(void)
{
	syscall(SYSCALL_DRAW_CURS, x, y, COL_BG, 0);
	x = 10;
	y += 22;
	if (y > 760) {
		y -= 22;
		scroll_up(22);
	}
	syscall(SYSCALL_DRAW_CURS, x, y, COL_CURS, 0);
}

void inc_cursor(void)
{
	x += 15;
    if (x > 1200) {
		new_line();
	}
	syscall(SYSCALL_DRAW_CURS, x, y, COL_CURS, 0);
}

void dec_cursor(void)
{
	syscall(SYSCALL_DRAW_CURS, x, y, 0, 0);
	x -= 15;
	if (x < 0)
		x = 0;
	syscall(SYSCALL_DRAW_CURS, x, y, COL_CURS, 0);
}

void get_line(uint8_t *buff)
{	
	print_string("# ", 0xff8080);
	// Clear buffer before use
	for (int i=0; i<255; i++)
		buff[i] = 0;

	char t = 0;
	uint32_t buff_pos = 0;
    while (t != 10) {
    	t = check_keypress();
		if ((t == KEY_BS) && (buff_pos>0)) {
			// Backspace pressed
			buff[buff_pos--] = '\0';
			dec_cursor();
			continue;
		}
		if (t < 10)
			continue;
    	draw_char(t, x, y, 0xfffdff);
		if (t != 10) {
			buff[buff_pos++] = t;
    		inc_cursor();
		}
	}
	buff[buff_pos] = 0;	
	new_line();
	t = 0;
}

