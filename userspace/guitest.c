#include <alibc.h>
#include <syscall.h>
#include <stdlib.h>
#include <ui.h>

void button_clicked();
void ui_event_loop();

UICanvas *canvas;
UIWindow *this_win;

void _start(void)
{
	srand(get_uptime_ns());

	this_win   = ui_window_new(400, 300, "Test Title");

	// Top level containers
	canvas = ui_canvas_new(this_win, 0, 400, 200);
	// UIButton *btn = ui_button_new(this_win, 0, "Click to add a rectangle", button_clicked);
	// UIComponent *c = ui_container_new(this_win, 0, 1);
	// UILabel *lbl = ui_label_new(this_win, 0, "This is a test label");

	// // 2nd Level
	// UILabel *lbl2 = ui_label_new(this_win, c, "Label2");
	// UILabel *lbl3 = ui_label_new(this_win, c, "Label3");
	// UIButton *btn2 = ui_button_new(this_win, c, "Button2", button_clicked);


	ui_redraw_window(this_win);
	ui_event_loop();
	kill_me();
}

void button_clicked(void)
{
	klog(LL_INFO, "Clicked the button worked\n", 0);
	uint64_t x = rand()%350;
	uint64_t y = rand()%30;
	uint64_t w = 50;
	uint64_t h = 50;
	uint32_t colour = rand()%0xffffff;
	ui_canvas_draw_filled_rect(canvas, x, y, w, h, colour);
	ui_redraw_window(this_win);
}
