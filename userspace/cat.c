#include <alibc.h>
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <stdlib.h>

void usage(void);


void _start(char **args, int argc)
{
	if (argc < 1) {
		usage();
		kill_me();
	}

	FILE *fd = fopen(args[0], "r");
	if (*(uint64_t *)fd == ENOENT) {
		printf("cat: File not found\n");
		kill_me();
	}
	char *fbuff = malloc(4096);

	// Read in the file, 4096 bytes at a time	
	while (!feof(fd)) {
		memset(fbuff, '\0', 4096);
		fread(fbuff, 4096, 1, fd);
		printf(fbuff);
	}
	kill_me();

}

void usage(void)
{	
	printf("Usage: cat <filename>\n");
}



