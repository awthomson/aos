#!/bin/bash

FONTNAME="Liberation-Sans"
SIZE="16"

rm ${FONTNAME}.h 2> /dev/null

convert -resize ${SIZE}\! -font ${FONTNAME} -pointsize 60 label:'.' blank.xbm 

for X in {A..Z} {a..z} {0..9} "." "\"" ":" "?" "!" "<" ">" "[" "]" "(" ")" "{" "}" "," "~" "/" '@' "#" "$" "%" "^" '*' '+' '-' '=' '_'; do
	Y=$(echo "${X}" | tr -d "\n" | od -An -t uC)
	echo "Converting $Y ($X)"
	# convert -resize ${SIZE}\! -font ${FONTNAME} -pointsize 60 label:"${X}" ${Y}.xbm 
	convert -scale 50% -font ${FONTNAME} -pointsize 60 label:"${X}" ${Y}.xbm 
done

for X in {32..127}; do
	echo  "/* $X */" >> ${FONTNAME}.h
if [ -e ${X}.xbm ]; then
	cat ${X}.xbm | grep -v "#" | grep -v "static" >> ${FONTNAME}.h
	rm ${X}.xbm
else
	cat blank.xbm | grep -v "#" | grep -v "static" >> ${FONTNAME}.h
fi
done

sed -i 's/ };//g' ${FONTNAME}.h

sed -i '1 i\static uint8_t font_data[] = {' ${FONTNAME}.h
echo "};" >> ${FONTNAME}.h
cp ${FONTNAME}.h ../userspace/ui
