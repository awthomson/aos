#include <stdio.h>
#include <stdlib.h>

void show_usage() {
	printf("Usage: vacalc address\n");
	printf("  Shows page-table offsets given a virtual address\n");
	exit(0);
}

void main(int argc, char **argv) {

	if (argc != 2 )
		show_usage();

	unsigned long long int val = strtol(argv[1], 0, 16);

	unsigned long long int x = val;
	unsigned int unused = x >> 48 & (0b111111111111111);
	unsigned int pml4   = x >> 39 & (0b111111111);
	unsigned int pdp    = x >> 30 & (0b111111111);
	unsigned int pd     = x >> 21 & (0b111111111);
	unsigned int pt     = x >> 12 & (0b111111111);
	unsigned int off    = x >> 0  & (0b111111111);
	printf("Converting virtual address 0x%llx to page-table indexes\n\n", x);

	printf("Unused: 0x%x\n", unused);
	printf("PML4:   %d\n", pml4);
	printf("PDP:    %d\n", pdp);
	printf("PD:     %d\n", pd);
	printf("PT:     %d\n", pt);
	printf("Offset: 0x%x\n\n", off);

}
