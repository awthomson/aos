#include <stdio.h>
#include <stdlib.h>

void show_usage() {
	printf("Usage: pml_calc pml4 pdp pd pt\n");
	printf("  Converts page-table locations to virtual address\n");
	exit(0);
}

void main(int argc, char **argv) {

	if (argc != 5 )
		show_usage();

	unsigned long long int pml4 = atoi(argv[1]);
	unsigned long long int pdp  = atoi(argv[2]);
	unsigned long long int pd   = atoi(argv[3]);
	unsigned long long  int pt   = atoi(argv[4]);

    pml4   = pml4 << 39;
    pdp    = pdp  << 30;
    pd     = pd   << 21;
    pt     = pt   << 12;

	unsigned long long int addr = pml4 + pdp + pd + pt;

	printf("Address: 0x%.16llx\n", addr);

}
