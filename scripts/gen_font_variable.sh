#!/bin/bash

FONTNAME="Liberation-Sans"
SCALE="50%"

rm ${FONTNAME}.h 2> /dev/null
rm ${FONTNAME}_meta.h 2> /dev/null

convert -resize "16x24" -extent "16x24" -font ${FONTNAME} -pointsize 160 label:'.' blank.xbm 

for X in {A..Z} {a..z} {0..9} "." "\"" ":" "?" "!" "<" ">" "[" "]" "(" ")" "{" "}" "," "~" "/" '@' "#" "$" "%" "^" '*' '+' '-' '=' '_'; do
	Y=$(echo "${X}" | tr -d "\n" | od -An -t uC)
	echo "Converting $Y ($X)"
	# convert -scale ${SCALE} -font ${FONTNAME} -pointsize 60 label:"${X}" ${Y}.xbm 
	convert -resize "16x24" -extent "16x24" -font ${FONTNAME} -pointsize 160 label:"${X}" ${Y}.xbm 
	# convert -resize "16x24" -extent "16x24" -font ${FONTNAME} -pointsize 60 label:"${X}" ${Y}.bmp
done

for X in {32..127}; do
	echo  "/* $X */" >> ${FONTNAME}.h
if [ -e ${X}.xbm ]; then
	cat ${X}.xbm | grep -v "#" | grep -v "static" >> ${FONTNAME}.h
	SIZE=$(tr -cd ',' < ${X}.xbm | wc -c)
	echo "16,    // ${X}" >> ${FONTNAME}_meta.h
	rm ${X}.xbm
else
	cat blank.xbm | grep -v "#" | grep -v "static" >> ${FONTNAME}.h
fi
done

sed -i 's/ };//g' ${FONTNAME}.h

sed -i '1 i\static uint8_t font_data[] = {' ${FONTNAME}.h
sed -i '1 i\static uint8_t font_width[] = {' ${FONTNAME}_meta.h
echo "};" >> ${FONTNAME}.h
echo "};" >> ${FONTNAME}_meta.h

cp ${FONTNAME}.h ../userspace/ui
cp ${FONTNAME}_meta.h ../userspace/ui
