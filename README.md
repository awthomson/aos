aOS

This is a simple hobby kernel + OS with a focus on running modern hardware (UEFI, 64-bit, etc.).  This targeted "hardware" is whatever 64-bit QEmu supports.

This uses the great "BOOTBOOT" bootloader to do a lot of the setup

# Features
	* 64-bit
	* Pre-emptive multitasking
	* Simple memory allocator/deallocator
	* IPC
	* EXT2 support (read-only)
	* Keyboard + Mouse drivers
	* 32-bit colour graphics WXGA resolution (1280x800)
	* Working terminal
	* ELF support
	* Beginnings of a GUI

# Helper scripts
    + gen_font.sh		- Converts a TTF to c source
	+ va_calc			- Simpe programe to show PML4/PDT indexes from a virtual address
	+ pml4_calc			- Simpe programe to show virtual address from table indexes

# TODO (GUI)
	* Primatives
		* Circles
	* Windows
		* Nicer drag & drop
		* Resize
		* Decorations
	* Components
		* Alignment
		* Margins
	* Buttons
		* States (enabled, disabled, clicked, etc.)
		* Center align text
	* Mouse
		* Context aware pointers
	* Labels
		* Alignment options
	* Menus
	* Status bar
	* Textbox
	* Slider
	* Textarea
	* Treeview
	* Tables

# TODO (Smallish)
	* Function for exec
	* Alpha/masking mouse pointer
	* Terminate process on fault and continue
	* Better drag + drop windows
	* Support double-click title to maximize
	* Userland tools
		* cd
		* uptime
		* mkdir
		* ps
		* pwd
	* Better way to read directory data (without shared dir_data space)
	* Convert inode to path string

# TODO (Large)
	* SMP
	* Better way to end processes - without kill_me()
	* ext2 write support
	* File permissions
	* Multi-user
	* Installable image

# Tech Debt
	* Mouse data should be stored in a structure
	* Create a typedef for structures - In progress
	* Tidy up includes in userspace makefile

# Known bugs
	* can execute a directory
	* Functions randomly returning memory addresses instead of actual values (stack corruption?)
	* get_uptime_ns() is actually get_uptime_us()?
	* Should be able to drag out-of-focus windows
	* Only frees the first page when doing a free()
	* No handling OOM situation

