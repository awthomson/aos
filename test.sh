#!/bin/bash

set -e 

cd ../../initrd; ./build_img.sh

cd ..

qemu-system-x86_64 -cpu qemu64 \
  -drive if=pflash,format=raw,unit=0,file=/usr/share/OVMF/OVMF_CODE.fd,readonly=on \
  -drive if=pflash,format=raw,unit=1,file=/usr/share/OVMF/OVMF_VARS.fd \
  -drive file=ext2-disk.img,format=raw,index=0,media=disk \
  -soundhw pcspk \
  -net none \
  -cdrom aos.iso \
  -m 3G \
  -serial stdio \
  -no-reboot   \
  -smp 1 \
#  -d int \
#/  -boot menu=on
#  -drive file=diskB.img,format=raw,index=3,media=disk \

